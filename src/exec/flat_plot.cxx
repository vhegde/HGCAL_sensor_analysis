#include "flat_plot.h"

flat_plot::flat_plot() {
  onlySpecial = false;
  useLogVal = true;
  testInfo = false;
  SuccessfullyLoaded = true;
  isAverageValues = false;
  initializedFlat = false;
  isRawData = true;
  h_fill = nullptr;
  h_fill_2D = nullptr;
  verbose = 0;
  zoomMin = 0;
  zoomMax = 0;
  for (size_t i = 0; i < 3; i++) {
    valueLabels.emplace_back("");
  }
  totalMax = -MAX_DOUBLE;
  totalMin = MAX_DOUBLE;
  aspectRatioX = 1125;
  aspectRatioY = 750;
}
void flat_plot::fill_data(TH1F *h) {
  DEBUG_MSG("Executing fill_data(TH1F*)...");
  h_fill = h;
  if (h_fill->Integral() == 0) {
    SuccessfullyLoaded = false;
  }
  h_fill->SetTitle("");
  h_fill->SetXTitle("Pad number");
  h_fill->SetYTitle(Form("%s [%s]", valueLabels.at(1).c_str(), valueLabels.at(2).c_str()));
  h_fill->GetYaxis()->SetTitleOffset(1.);
  h_fill->GetXaxis()->SetTitleSize(0.05);
  h_fill->GetXaxis()->SetLabelSize(0.06);
  h_fill->GetYaxis()->SetTitleSize(0.05);
  h_fill->GetYaxis()->SetLabelSize(0.05);
  h_fill->SetLineColor(kBlack);
  h_fill->SetMarkerStyle(20);
  h_fill->SetMarkerSize(1);
  if (zoomMin != zoomMax) {
    h_fill->GetYaxis()->SetRangeUser(zoomMin, zoomMax);
  }
  numberNAN = remove_OF_bins(h_fill, MAX_ALLOWED_PAD_CONTENT);
  numberOF = remove_NAN_bins(h_fill);
  if (testInfo) {
    printf("flat_plot::fill_data_TH1F %s\n", unique_ID(h_fill).c_str());
  }
  DEBUG_MSG("fill_data(TH1F*) finished!");
}
void flat_plot::fill_data(std::vector<TH1F *> &h_vec) {
  h_fill_vec.clear();
  for (auto i : h_vec) {
    h_fill_vec.push_back(dynamic_cast<TH1F *>(i));
  }
}
void flat_plot::fill_data(TH2F *h) {
  DEBUG_MSG("Executing fill_data(TH2F*)...");
  h_fill_2D = h;
  if (h_fill_2D->Integral() == 0) {
    SuccessfullyLoaded = false;
  }
  h_fill_2D->SetTitle("");
  if (zoomMin != zoomMax) {
    h_fill_2D->GetZaxis()->SetRangeUser(zoomMin, zoomMax);
  }
  if (testInfo) {
    printf("flat_plot::fill_data_TH2F %s\n", unique_ID(h_fill_2D).c_str());
  }
  DEBUG_MSG("fill_data(TH2F*) finished!");
}
void flat_plot::draw_warnings() {
  double WARNING_TXT_SIZE = 0.07;
  int WARNING_COLOR = 2;
  double WARNING_XPOS = 0.2;
  if (numberOF != 0) {
    std::string warningMsg = "OF warning: " + int_to_string(numberOF) + " entries set to 0";
    draw_TPave(warningMsg, WARNING_XPOS, 0.7, WARNING_XPOS, 0.7, WARNING_TXT_SIZE, "NDC", WARNING_COLOR);
  }
  if (numberNAN != 0) {
    std::string warningMsg = "NAN warning: " + int_to_string(numberNAN) + " entries set to 0";
    draw_TPave(warningMsg, WARNING_XPOS, 0.4, WARNING_XPOS, 0.4, WARNING_TXT_SIZE, "NDC", WARNING_COLOR);
  }
}
bool flat_plot::has_negative_values() {
  for (int i = 0; i < h_fill->GetNbinsX(); ++i) {
    if (h_fill->GetBinContent(i + 1) <= 0) {
      return true;
    }
  }
  return false;
}
void flat_plot::init_flat() {
  if (initializedFlat) {
    return;
  }
  DEBUG_MSG("Executing init_flat...");
  if (has_negative_values()) {
    useLogVal = false;
  }

  if (!useLogVal) {
    h_fill->SetMaximum(h_fill->GetMaximum() + (h_fill->GetMaximum() - h_fill->GetMinimum()) * 0.2);
  } else {
    h_fill->SetMaximum(h_fill->GetMaximum() + (h_fill->GetMaximum() - h_fill->GetMinimum()) * 4);
  }
  h_categories[MAX_CELL_TYPE] = new TH1F(Form("h_categories[%d]", MAX_CELL_TYPE),
                                         Form("h_categories[%d]", MAX_CELL_TYPE), MAX_CELL_TYPE, 0, MAX_CELL_TYPE);
  h_categories[MAX_CELL_TYPE]->SetName("all");
  for (int i = 0; i < MAX_CELL_NUMBER; ++i) {
    std::vector<int> v_tmp;
    cellWithType.push_back(v_tmp);
  }
  std::vector<int> processed_specialCells;
  for (int i_bin = 0; i_bin < h_fill->GetNbinsX(); ++i_bin) {
    if (i_bin >= int(padTypes.size())) {
      continue;
    }
    int type = padTypes.at(i_bin);
    if (type == -1) {
      continue;
    }
    std::string typestring = padTypeStrings.at(i_bin);
    bool isSpecialCell = false;
    int special_type(-1);
    std::string special_typestring;
    for (unsigned int i = 0; i < specialCellNumbers.size(); ++i) {
      for (unsigned int j = 0; j < specialCellNumbers.at(i).size(); ++j) {
        if (i_bin + 1 == specialCellNumbers.at(i).at(j)) {
          isSpecialCell = true;
          if (specialCellNames.size() != specialCellCollection.size()) {
            special_typestring = specialCellCollection.at(i);
          } else {
            // special_typestring=specialCellNames.at(i)+" ("+specialCellCollection.at(i)+")";
            special_typestring = specialCellNames.at(i);
          }
          special_type = MAX_CELL_TYPE + i;
          break;
        }
      }
      if (isSpecialCell == true) {
        break;
      }
    }
    bool reduceBinIndex = false;
    if (isSpecialCell) {
      if (!is_in_vector(i_bin + 1, processed_specialCells)) {
        // printf("Bin %d is special type value %d, description %s\n",i_bin+1,special_type,special_typestring.c_str()
        // );
        if (!is_in_vector(special_type, special_types)) {
          special_types.push_back(special_type);
        }
        processed_specialCells.push_back(i_bin + 1);
        type = special_type;
        typestring = special_typestring;
        reduceBinIndex = true;
      }
    }
    // from here on type includes "normal" and "special" types!
    cellWithType.at(i_bin).push_back(type);

    // std::cout<<"Bin "<<i_bin+1<<" has type "<<type<<" "<<typestring<<" with content
    // "<<h_fill->GetBinContent(i_bin+1)<<std::endl;
    int pos = find(types.begin(), types.end(), type) - types.begin();
    if (!is_in_vector(type, types)) {
      types.push_back(type);
      h_categories[types.size() - 1] =
          new TH1F(Form("h_categories[%d]", int(types.size()) - 1), Form("h_categories[%d]", int(types.size()) - 1),
                   MAX_CELL_TYPE, 0, MAX_CELL_TYPE);
      for (int i_files = 0; i_files < N_VALID_FILES; ++i_files) {
        h_cat_sensors[types.size() - 1][i_files] = new TH1F(
            Form("h_cat_sensors[%d][%d]", int(types.size()) - 1, i_files),
            Form("h_cat_sensors[%d][%d]", int(types.size()) - 1, i_files), MAX_CELL_TYPE, 0, 200 * MAX_CELL_TYPE);
      }
      gr_categories_sensors[types.size() - 1] = new TGraphErrors(0);
      h_markers[types.size() - 1] = dynamic_cast<TH1F *>(h_fill->Clone(Form("h_markers[%d]", int(types.size()) - 1)));
      h_markers[types.size() - 1]->Reset();
      h_markers[types.size() - 1]->SetMarkerSize(0.8);
      int color = get_color_line_fill_style_no_black(pos)[0];
      if (type == -1) {
        color = 20;
      } else if (type == 0) {
        color = 1;
      } else if (typestring.find("test capacity") < typestring.size()) {
        color = 18;
      }
      h_markers[types.size() - 1]->SetMarkerColor(color);

      h_markers[types.size() - 1]->SetName(typestring.c_str());
      h_categories[types.size() - 1]->SetLineColor(color);
      h_categories[types.size() - 1]->SetMarkerColor(color);
      h_categories[types.size() - 1]->SetName(typestring.c_str());
      gr_categories_sensors[types.size() - 1]->SetLineColor(color);
      gr_categories_sensors[types.size() - 1]->SetMarkerColor(color);
      gr_categories_sensors[types.size() - 1]->SetName(typestring.c_str());
      gr_categories_sensors[types.size() - 1]->SetMarkerSize(0.7);
      gr_categories_sensors[types.size() - 1]->SetMarkerStyle(20);
    }
    double cont = h_fill->GetBinContent(i_bin + 1);
    // if (zoom.size() && cont>h_fill->GetMaximum()) cont=h_fill->GetMaximum(); //if outside, show at the edge
    // if (zoom.size() && cont<h_fill->GetMinimum()) cont=h_fill->GetMinimum(); //if outside, show at the edge
    h_markers[pos]->SetBinContent(i_bin + 1, cont);
    h_markers[pos]->SetBinError(i_bin + 1, 0.01 * cont);
    for (int i_files = 0; i_files < N_VALID_FILES; ++i_files) {
      double fileBinCont = GR_CONTENTS[i_bin]->GetY()[i_files];
      totalMax = TMath::Max(fileBinCont, totalMax);
      totalMin = TMath::Min(fileBinCont, totalMin);
      // printf("Bin %d file %d pos %d type %d content %f (in h_fill %f)\n",i_bin,i_files,pos,types.at(pos),
      // fileBinCont,cont);
      h_cat_sensors[pos][i_files]->Fill(fileBinCont);
    }
    if (reduceBinIndex) {
      --i_bin;
    }
  }

  for (unsigned int i = 0; i < cellWithType.size(); ++i) {
    for (unsigned int j = 0; j < cellWithType.at(i).size(); ++j) {
      DEBUG_MSG("cellWithType.at(%d).at(%d) (pad %d): %d", i, j, i + 1, cellWithType.at(i).at(j));
    }
  }

  // legend
  leg = new TLegend(0.15, 0.8, 0.95, 0.88);
  // leg->SetFillColor(kNone);
  // leg->SetLineColor(kNone);
  leg->SetNColumns(4);
  standardpos = find(types.begin(), types.end(), 0) - types.begin();
  if (standardpos < int(types.size())) {
    leg->AddEntry(h_markers[standardpos], h_markers[standardpos]->GetName(), "p");
    legendorder.push_back(standardpos);
  }
  for (unsigned int i = 0; i < types.size(); ++i) {
    // printf("Looping on type %d / %d : %d\n",i,int(types.size()),types.at(i) );
    if (is_in_vector(types.at(i), special_types)) {
      continue;
    }
    if (types.at(i) == 0) {
      continue;
    }
    if (types.at(i) == -1) {
      continue;
    }
    // printf("Fill legend for type[%d]: %d %s\n",i,types.at(i),h_markers[i]->GetName() );
    std::string entryname = h_markers[i]->GetName();
    legendorder.push_back(i);
    leg->AddEntry(h_markers[i], entryname.c_str(), "p");
  }
  // now special types
  full_leg = dynamic_cast<TLegend *>(leg->Clone());
  special_leg = dynamic_cast<TLegend *>(leg->Clone());
  special_leg->Clear();
  if (!SINGLE_CELLS_FOR_AVERAGE) {
    special_leg->SetHeader("Warning: different bin sizes!");
  }
  for (unsigned int i = 0; i < special_types.size(); ++i) {
    int thispos = find(types.begin(), types.end(), MAX_CELL_TYPE + i) - types.begin();
    // printf("Filling legend for special_types[%d]: %d %s\n",i,types.at(thispos),h_markers[thispos]->GetName() );
    full_leg->AddEntry(h_markers[thispos], h_markers[thispos]->GetName(), "p");
    special_leg->AddEntry(h_markers[thispos], h_markers[thispos]->GetName(), "p");
    legendorder.push_back(thispos);
  }
  int unspecpos = find(types.begin(), types.end(), -1) - types.begin();
  if (unspecpos < int(types.size())) {
    leg->AddEntry(h_markers[unspecpos], h_markers[unspecpos]->GetName(), "p");
    full_leg->AddEntry(h_markers[unspecpos], h_markers[unspecpos]->GetName(), "p");
    special_leg->AddEntry(h_markers[unspecpos], h_markers[unspecpos]->GetName(), "p");
  }
  for (unsigned int i = 0; i < legendorder.size(); ++i) {
    int legbin = find(legendorder.begin(), legendorder.end(), i) - legendorder.begin();
    invlegendorder.push_back(legbin);
    // printf("Legendorder type entry %d (%s) displayed at legend position
    // %d\n",i,h_markers[i]->GetName(),invlegendorder.at(i));
  }

  if (isRawData) {
    padVoltageInfo = new TPaveText(0.1, 0.92, 0.1, 0.92, "NDC");
    padVoltageInfo->SetFillColor(kNone);
    padVoltageInfo->SetLineColor(kNone);
    padVoltageInfo->SetTextSize(0.03);
    padVoltageInfo->SetTextAlign(12);
    padVoltageInfo->SetTextColor(kBlack);
    std::string all_sensors_string =
        isAverageValues ? int_to_string(N_VALID_FILES) + " sensor" + (N_VALID_FILES > 1 ? "s" : "") + " at " : "";
    padVoltageInfo->AddText(Form("Values for %s%s = %.1f %s", all_sensors_string.c_str(), selectorLabels.at(1).c_str(),
                                 selector, selectorLabels.at(2).c_str()));
  }

  for (unsigned int itype = 0; itype < types.size(); ++itype) {
    if (!SINGLE_CELLS_FOR_AVERAGE || !isAverageValues) {
      h_markers_proj[itype] =
          dynamic_cast<TH1F *>(project_to_y_axis(h_markers[itype], true)->Clone(Form("h_markers_proj[%d]", itype)));
      h_markers_proj_zoom[itype] = dynamic_cast<TH1F *>(
          project_to_y_axis(h_markers[itype], true,
                            h_markers[standardpos]->GetBinContent(h_markers[standardpos]->GetMaximumBin()))
              ->Clone(Form("h_markers_proj[%d]", itype)));
      h_markers_proj[itype]->SetTitle(
          Form("%s;%s;Number of pads", h_markers[itype]->GetName(), h_fill->GetYaxis()->GetTitle()));
      h_markers_proj_zoom[itype]->SetTitle(
          Form("%s;%s;Number of pads", h_markers[itype]->GetName(), h_fill->GetYaxis()->GetTitle()));
    } else {
      // determine appropriate min and max for plot
      double minCont = MAX_DOUBLE;
      double maxCont = -MAX_DOUBLE;
      for (int i = 0; i < N_VALID_FILES; ++i) {
        for (int j = 0; j < int(padTypes.size()); ++j) {
          if (types.at(itype) != 1 && padTypes.at(j) == 1) {
            // printf("Skipping pad %d with type %d\n",j+1, hc->get_pad_type(j+1));
            continue;
          }
          double cont = SENSOR_VALS_FOR_AVERAGE[i][j];
          if (cont > maxCont) {
            // printf("File %d cell %d: new max %.2f\n",i,j,cont );
            maxCont = cont;
          }
          if (cont < minCont) {
            // printf("File %d cell %d: new max %.2f\n",i,j,cont );
            minCont = cont;
          }
        }
      }
      double range = maxCont - minCont;
      minCont -= range * 0.05;
      maxCont += range * 0.05;
      double zoomRange = zoomMax - zoomMin;
      int nbin = zoomRange > 0 ? int(20. * range / zoomRange) : 40;
      // printf("Final range %.2f - %.2f with %d bins\n",minCont,maxCont,nbin);
      h_markers_proj[itype] =
          new TH1F(Form("h_markers_proj[%d]", itype), Form("h_markers_proj[%d]", itype), nbin, minCont, maxCont);
      h_markers_proj_zoom[itype] = new TH1F(Form("h_markers_proj_zoom[%d]", itype),
                                            Form("h_markers_proj_zoom[%d]", itype), nbin, minCont, maxCont);
      // fill the plots with single cells of each type
      for (unsigned int i = 0; i < cellWithType.size(); ++i) {
        for (unsigned int j = 0; j < cellWithType.at(i).size(); ++j) {
          int thistype = cellWithType.at(i).at(j);
          if (thistype == types.at(itype)) {
            for (int k = 0; k < N_VALID_FILES; ++k) {
              h_markers_proj[itype]->Fill(SENSOR_VALS_FOR_AVERAGE[k][i]);
              h_markers_proj_zoom[itype]->Fill(SENSOR_VALS_FOR_AVERAGE[k][i]);
            }
          }
          // DEBUG_MSG("cellWithType.at(%d).at(%d): %d",i,j,cellWithType.at(i).at(j));
        }
      }
      h_markers_proj[itype]->SetTitle(
          Form("%s;%s;Number of cells", h_markers[itype]->GetName(), h_fill->GetYaxis()->GetTitle()));
      h_markers_proj_zoom[itype]->SetTitle(
          Form("%s;%s;Number of cells", h_markers[itype]->GetName(), h_fill->GetYaxis()->GetTitle()));
    }

    // double average=SuccessfullyLoaded?get_average_bin_content(h_markers[itype])[0]:0;
    // double meanError=SuccessfullyLoaded?get_average_bin_content(h_markers[itype])[1]:-1;
    // double STDDeviation=SuccessfullyLoaded?get_average_bin_content(h_markers[itype])[2]:-1;
    double average = SuccessfullyLoaded ? h_markers_proj[itype]->GetMean() : 0;
    double meanError = SuccessfullyLoaded ? h_markers_proj[itype]->GetMeanError() : -1;
    double STDDeviation = SuccessfullyLoaded ? h_markers_proj[itype]->GetStdDev() : -1;
    printf("Cells %s have average value of %.2f +- %.2f (meanerr %.2f)\n", h_markers[itype]->GetName(), average,
           STDDeviation, meanError);
    // int legbin=find(legendorder.begin(), legendorder.end(), itype) - legendorder.begin();

    int legbin = invlegendorder.at(itype);
    h_categories[itype]->SetBinContent(legbin + 1, average);
    h_categories[itype]->SetBinError(
        legbin + 1, TMath::Max(STDDeviation, h_fill->GetMaximum() * 1e-10));  // needed to get rid of drawn empty bins
    h_categories[itype]->SetMarkerStyle(20);
    h_categories[itype]->SetMarkerSize(0.8);
    h_categories_meanError[itype] = dynamic_cast<TH1F *>(h_categories[itype]->Clone());
    h_categories_meanError[itype]->SetBinError(
        legbin + 1, TMath::Max(meanError, h_fill->GetMaximum() * 1e-10));  // needed to get rid of drawn empty bins
    h_categories[MAX_CELL_TYPE]->SetBinContent(legbin + 1, average);
    h_categories[MAX_CELL_TYPE]->SetBinError(
        legbin + 1, TMath::Max(STDDeviation, h_fill->GetMaximum() * 1e-10));  // needed to get rid of drawn empty bins
    h_categories[MAX_CELL_TYPE]->GetXaxis()->SetBinLabel(legbin + 1, "");     // Form("%d -> %d",itype,legbin));
    for (int i_files = 0; i_files < N_VALID_FILES; ++i_files) {
      // printf("File %d %s: %f\n",
      // i_files,gr_categories_sensors[itype]->GetName(),h_cat_sensors[itype][i_files]->GetMean()); double
      // xPos=i_files+(itype+1.)*(1./(types.size()+1));
      double xPos = i_files + 0.5;
      // printf("gr_categories_sensors[%d]->SetPoint(%d,%.2f,%.2f)\n",itype,i_files,xPos,h_cat_sensors[itype][i_files]->GetMean());
      gr_categories_sensors[itype]->SetPoint(i_files, xPos, h_cat_sensors[itype][i_files]->GetMean());
      gr_categories_sensors[itype]->SetPointError(i_files, 0, h_cat_sensors[itype][i_files]->GetRMS());
      gr_categories_sensors[itype]->SetLineWidth(2);
    }
  }
  h_categories[MAX_CELL_TYPE]->GetXaxis()->SetRangeUser(0, types.size());
  h_categories[MAX_CELL_TYPE]->SetTitle("");
  h_categories[MAX_CELL_TYPE]->SetXTitle("Cell categories");
  h_categories[MAX_CELL_TYPE]->GetXaxis()->SetTitleOffset(0.7);
  h_categories[MAX_CELL_TYPE]->SetYTitle(h_fill->GetYaxis()->GetTitle());
  h_categories[MAX_CELL_TYPE]->GetYaxis()->SetTitleOffset(1.);
  h_categories[MAX_CELL_TYPE]->GetXaxis()->SetTitleSize(0.05);
  h_categories[MAX_CELL_TYPE]->GetXaxis()->SetLabelSize(0.06);
  h_categories[MAX_CELL_TYPE]->GetYaxis()->SetTitleSize(0.05);
  h_categories[MAX_CELL_TYPE]->GetYaxis()->SetLabelSize(0.05);
  h_categories[MAX_CELL_TYPE]->SetLineColor(kBlack);
  h_categories[MAX_CELL_TYPE]->SetMarkerStyle(20);
  h_categories[MAX_CELL_TYPE]->SetMarkerSize(1);
  if (zoomMin != zoomMax) {
    h_categories[MAX_CELL_TYPE]->GetYaxis()->SetRangeUser(zoomMin, zoomMax);
  } else if (!useLogVal) {
    // h_categories[MAX_CELL_TYPE]->SetMaximum(h_categories[MAX_CELL_TYPE]->GetMaximum()+(h_categories[MAX_CELL_TYPE]->GetMaximum()-h_categories[MAX_CELL_TYPE]->GetMinimum())*0.2);
    h_categories[MAX_CELL_TYPE]->SetMinimum(h_fill->GetMinimum());
    h_categories[MAX_CELL_TYPE]->SetMaximum(h_fill->GetMaximum());
  } else {
    h_categories[MAX_CELL_TYPE]->SetMaximum(
        h_categories[MAX_CELL_TYPE]->GetMaximum() +
        (h_categories[MAX_CELL_TYPE]->GetMaximum() - h_categories[MAX_CELL_TYPE]->GetMinimum()) * 4);
  }

  // first determine xrange and yMax
  double yMax = 0;
  for (unsigned int itype = 0; itype < types.size(); ++itype) {
    int itype_ordered = legendorder.at(itype);
    if (!is_in_vector(types.at(itype_ordered), special_types)) {
      continue;
    }
    // h_markers_proj_zoom[itype_ordered]->Rebin(5);
    double thisyMax = h_markers_proj_zoom[itype_ordered]->GetBinContent(h_markers_proj_zoom[itype]->GetMaximumBin());
    if (yMax < thisyMax) {
      yMax = thisyMax;
    }
  }
  for (unsigned int itype = 0; itype < types.size(); ++itype) {
    int itype_ordered = legendorder.at(itype);
    h_markers_proj_zoom[itype_ordered]->SetTitle("");
    h_markers_proj_zoom[itype_ordered]->SetLineColor(h_markers[itype_ordered]->GetMarkerColor());
    h_markers_proj_zoom[itype_ordered]->SetLineWidth(2);
    h_markers_proj_zoom[itype_ordered]->SetMaximum(yMax * 1.2);
    if (zoomMin != zoomMax) {
      h_markers_proj_zoom[itype_ordered]->GetXaxis()->SetRangeUser(zoomMin, zoomMax);
    }
    h_markers_proj[itype_ordered]->SetLineColor(h_markers[itype_ordered]->GetMarkerColor());
    h_markers_proj[itype_ordered]->SetLineWidth(2);
    if (zoomMin != zoomMax) {
      h_markers_proj[itype_ordered]->GetXaxis()->SetRangeUser(zoomMin, zoomMax);
    }
  }
  initializedFlat = true;
  DEBUG_MSG("init_flat finished!");
}
void flat_plot::draw_flat(const std::string &outputFile) {
  DEBUG_MSG("Executing draw_flat...\n");
  init_flat();
  auto *h_fill_draw = dynamic_cast<TH1F *>(h_fill->Clone("h_fill_draw"));
  auto *c_chan = new TCanvas("c_chan", "c_chan", 800, 500, aspectRatioX, aspectRatioY);
  c_chan->cd();
  gPad->SetRightMargin(0.01);
  if (useLogVal) {
    gPad->SetLogy();
  }
  h_fill_draw->Draw("*");
  auto *gr_fill = new TGraph(h_fill);
  gr_fill->Draw("plsame");
  gPad->SetGridx();
  gPad->SetGridy();
  // draw colored dots
  for (unsigned int i = 0; i < types.size(); ++i) {
    if (is_in_vector(types.at(i), special_types)) {
      continue;
    }
    h_markers[i]->Draw("psame");
  }
  h_fill_draw->Draw("axissame");
  leg->Draw();
  if (isRawData) {
    padVoltageInfo->Draw("same");
  }
  draw_warnings();
  c_chan->Print(outputFile.c_str());
  DEBUG_MSG("draw_flat finished!");
}
void flat_plot::create_summary_file(const std::string &outputFile, bool yesToAll) {
  DEBUG_MSG("Executing create_summary_file...");
  if (outputFile.empty()) {
    return;
  }
  printf("Write summary info to %s\n", outputFile.c_str());
  if (!yesToAll && is_file(outputFile)) {
    std::string warnFileExists = "Warning: file " + outputFile + " exists! Overwrite?";
    if (!yes_no_request(warnFileExists)) {
      printf("Info: no summary data file is written out!\n");
      return;
    }
  }
  fileout.open(outputFile.c_str());
  fileout << "# Summary file for data from" << std::endl;
  for (const auto &inputFile : inputFiles) {
    fileout << "# " << inputFile << std::endl;
  }
  fileout << std::endl;

  printf("Created new file for sensor analysis: %s\n", outputFile.c_str());
  DEBUG_MSG("create_summary_file finished!");
}
void flat_plot::write_average_values(const std::string &outputFile, bool yesToAll) {
  DEBUG_MSG("Executing write_average_values...");
  create_summary_file(outputFile, yesToAll);
  replace_val_two_col_file(outputFile, "#Categories", "Mean\tRMS");
  init_flat();
  for (unsigned int itype = 0; itype < types.size(); ++itype) {
    int legbin = invlegendorder.at(itype);
    std::string lineName =
        Form("%s %s [%s]", valueLabels.at(0).c_str(), h_markers[itype]->GetName(), valueLabels.at(2).c_str());
    std::string lineVal = Form("%.2f\t%.2f", h_categories[itype]->GetBinContent(legbin + 1),
                               h_categories[itype]->GetBinError(legbin + 1));
    if (!outputFile.empty()) {
      replace_val_two_col_file(outputFile, lineName, lineVal);
    }
  }
  fileout.close();
  DEBUG_MSG("write_average_values finished!");
}
void flat_plot::draw_flat_category_distr(const std::string &outputFile) {
  DEBUG_MSG("Executing draw_flat_category_distr...");
  init_flat();
  auto *c_cat_distr = new TCanvas("c_cat_distr", "c_cat_distr", 800, 500, aspectRatioX, aspectRatioY);
  c_cat_distr->Divide(5, 4);
  for (unsigned int i = 0; i < types.size(); ++i) {
    c_cat_distr->cd(i + 1);
    gPad->SetRightMargin(0.01);
    int itype = legendorder.at(i);
    h_markers_proj[itype]->Draw("");
    draw_warnings();
  }
  c_cat_distr->Print(outputFile.c_str());
  DEBUG_MSG("draw_flat_category_distr finished!");
}
void flat_plot::draw_flat_category_distr_spec(const std::string &outputFile) {
  DEBUG_MSG("Executing draw_flat_category_distr_spec...");
  init_flat();
  if (specialCellCollection.empty()) {
    return;
  }
  auto *c_cat_distr_spec = new TCanvas("c_cat_distr_spec", "c_cat_distr_spec", 800, 500, aspectRatioX, aspectRatioY);
  c_cat_distr_spec->cd();
  gPad->SetRightMargin(0.01);
  bool isDrawn = false;
  for (unsigned int i = 0; i < types.size(); ++i) {
    int itype_ordered = legendorder.at(i);
    if (!is_in_vector(types.at(itype_ordered), special_types)) {
      continue;
    }
    h_markers_proj_zoom[itype_ordered]->Draw(isDrawn ? "same" : "");
    isDrawn = true;
  }
  special_leg->Draw();
  draw_warnings();
  c_cat_distr_spec->Print(outputFile.c_str());
  DEBUG_MSG("draw_flat_category_distr_spec finished!");
}
void flat_plot::draw_flat_category(const std::string &outputFile) {
  DEBUG_MSG("Executing draw_flat_category...");
  init_flat();
  auto *c_cat = new TCanvas("c_cat", "c_cat", 800, 500, aspectRatioX, aspectRatioY);
  c_cat->cd();
  gPad->SetRightMargin(0.01);
  gPad->SetGridx();
  gPad->SetGridy();
  if (useLogVal) {
    gPad->SetLogy();
  }
  if (onlySpecial) {
    h_categories[MAX_CELL_TYPE]->GetXaxis()->SetRangeUser(types.size() - special_types.size(), types.size());
  }
  h_categories[MAX_CELL_TYPE]->Draw("*ex0");

  auto *gr_cat = new TGraph(h_categories[MAX_CELL_TYPE]);
  gr_cat->Draw("psame");

  for (unsigned int i = 0; i < types.size(); ++i) {
    h_categories[i]->Draw("pex0same");
    h_categories_meanError[i]->Draw("e1x0same");
  }
  if (isRawData) {
    padVoltageInfo->Draw("same");
  }
  if (onlySpecial) {
    special_leg->Draw();
  } else {
    full_leg->Draw();
  }
  draw_warnings();
  c_cat->Print(outputFile.c_str());
  DEBUG_MSG("draw_flat_category finished!");
}
void flat_plot::draw_flat_category_rel(const std::string &outputFile, std::vector<double> padSurfaceVec) {
  DEBUG_MSG("Executing draw_flat_category_rel...");
  init_flat();
  auto *c_relcat = new TCanvas("c_relcat", "c_relcat", 800, 500, aspectRatioX, aspectRatioY);
  c_relcat->cd();
  gPad->SetRightMargin(0.01);
  gPad->SetGridx();
  gPad->SetGridy();
  auto *h_dummy = dynamic_cast<TH1F *>(h_categories[MAX_CELL_TYPE]->Clone("h_dummy"));
  h_dummy->Reset();
  h_dummy->SetYTitle(
      Form("(%s #times A^{std})/ (%s^{std} #times A)", valueLabels.at(1).c_str(), valueLabels.at(1).c_str()));
  h_dummy->SetMinimum(0);
  h_dummy->SetMaximum(4);
  h_dummy->Draw();
  TH1F *h_categories_norm[MAX_CELL_TYPE];
  TH1F *h_categories_meanError_norm[MAX_CELL_TYPE];
  for (unsigned int i = 0; i < types.size(); ++i) {
    if (is_in_vector(types.at(i), special_types)) {
      continue;
    }
    h_categories_norm[i] = dynamic_cast<TH1F *>(h_categories[i]->Clone(Form("h_categories_norm[%d]", i)));
    h_categories_meanError_norm[i] =
        dynamic_cast<TH1F *>(h_categories_meanError[i]->Clone(Form("h_categories_meanError_norm[%d]", i)));
    double scaleFactor = 1. / h_categories[MAX_CELL_TYPE]->GetBinContent(1);
    scaleFactor /= padSurfaceVec.at(types.at(i));
    h_categories_norm[i]->Scale(scaleFactor);
    h_categories_norm[i]->Draw("pex0same");
    h_categories_meanError_norm[i]->Scale(scaleFactor);
    h_categories_meanError_norm[i]->Draw("e1x0same");
  }
  if (isRawData) {
    padVoltageInfo->Draw("same");
  }
  full_leg->Draw();
  TPaveText *padcalibnorm = nullptr;
  padcalibnorm = new TPaveText(0.96, 0.92, 0.96, 0.92, "NDC");
  padcalibnorm->SetFillColor(kNone);
  padcalibnorm->SetLineColor(kNone);
  padcalibnorm->SetTextSize(0.03);
  padcalibnorm->SetTextAlign(32);
  padcalibnorm->SetTextColor(kBlack);
  padcalibnorm->AddText("Test capacities normalized to design values");
  padcalibnorm->Draw("same");
  draw_warnings();
  c_relcat->Print(outputFile.c_str());
  DEBUG_MSG("draw_flat_category_rel finished!");
}
void flat_plot::draw_flat_category_sensors(const std::string &outputFile) {
  DEBUG_MSG("Executing draw_flat_category_sensors...");
  init_flat();
  totalMin = 0.01;
  totalMax = 10000;
  if (!isAverageValues) {
    return;
  }
  auto *c_sensors = new TCanvas("c_sensors", "c_sensors", 800, 500, aspectRatioX, aspectRatioY);
  c_sensors->cd();
  gPad->SetRightMargin(0.01);
  if (useLogVal) {
    gPad->SetLogy();
  }
  gPad->SetGridx();
  gPad->SetGridy();
  auto *h_gr_contents = new TH1F("h_gr_contents", "h_gr_contents", N_VALID_FILES, 0, N_VALID_FILES);
  for (int i = 0; i < N_VALID_FILES; ++i) {
    // h_gr_contents->SetBinContent(i+1,gr_contents[135]->GetY()[i]);
    h_gr_contents->GetXaxis()->SetBinLabel(i + 1, VALID_SENSOR_NAMES[i].c_str());
  }
  h_gr_contents->SetTitle("");
  h_gr_contents->SetXTitle("Sensors");
  h_gr_contents->GetXaxis()->SetTitleOffset(1.3);
  h_gr_contents->SetYTitle(h_fill->GetYaxis()->GetTitle());
  h_gr_contents->SetMarkerColor(0);
  h_gr_contents->SetLineColor(0);

  if (zoomMin != zoomMax) {
    h_gr_contents->GetYaxis()->SetRangeUser(zoomMin, zoomMax);
  } else if (!useLogVal) {
    double totalRange = totalMax - totalMin;
    h_gr_contents->SetMinimum(totalMin - totalRange * 0.1);
    h_gr_contents->SetMaximum(totalMax + totalRange * 0.2);
  } else {
    h_gr_contents->SetMinimum(totalMin / 2);
    h_gr_contents->SetMaximum(totalMax * 10);
  }

  h_gr_contents->Draw("*ex0");
  for (unsigned int i = 0; i < types.size(); ++i) {
    int drawnow = legendorder.at(types.size() - (i + 1));
    shift_x_values(gr_categories_sensors[drawnow], 0.5, i, types.size());
    gr_categories_sensors[drawnow]->Draw("plsame");
    // printf("%s: %f\n",gr_categories_sensors[i]->GetName(), gr_categories_sensors[i]->GetY()[0]);
  }
  if (isRawData) {
    padVoltageInfo->Draw("same");
  }
  full_leg->Draw();
  draw_warnings();
  c_sensors->Print(outputFile.c_str());
  DEBUG_MSG("draw_flat_category_sensors finished!");
}
void flat_plot::draw_pad(const std::string &outputFile, bool isCVInput) {
  DEBUG_MSG("Executing draw_pad...\n");
  h_fill->SetXTitle(Form("%s [%s]", selectorLabels.at(1).c_str(), selectorLabels.at(2).c_str()));
  h_fill->GetXaxis()->SetTitleOffset(1);

  auto *c_chan = new TCanvas("c_chan", "c_chan", 800, 500, aspectRatioX, aspectRatioY);
  c_chan->cd();
  gPad->SetRightMargin(0.01);
  gPad->SetGridx();
  gPad->SetGridy();
  if (useLogVal) {
    gPad->SetLogy();
  }
  // gPad->SetLogx();
  h_fill->Draw("pl");

  TPaveText *topLeftInfo = make_label(0.1, 0.92, 0.1, 0.92, 0.03, 12);
  if (topLeftLabel == "") {
    std::string all_sensors_string =
        isAverageValues ? int_to_string(N_VALID_FILES) + " sensor" + (N_VALID_FILES > 1 ? "s" : "") + " in " : "";
    topLeftInfo->AddText(Form("Values for %spad %d", all_sensors_string.c_str(), int(selector)));
  } else {
    topLeftInfo->AddText(topLeftLabel.c_str());
  }
  topLeftInfo->Draw("same");

  // add some textual info to the CV plot
  if (isCVInput) {
    // get the position of the last bin
    int lastBin = h_fill->GetNbinsX();
    double lastCapacitance = h_fill->GetBinContent(lastBin);  // pF
    const char *lastVoltage = h_fill->GetXaxis()->GetBinLabel(lastBin);

    // vacuum permittivity and relative permittivity for silicon
    const double epsilon_0 = 8.854187817e-12;
    const double epsilon_r = 11.7;

    // TODO: read cell area from the geometry file
    const double cellArea = 1;                                                                            // cm^2
    double effectiveWidth = 1e6 * epsilon_r * epsilon_0 * (cellArea * 1e-4) / (lastCapacitance * 1e-12);  // micrometers

    TPaveText *inPlotInfo = make_label(0.9, 0.25, 0.9, 0.32);
    inPlotInfo->AddText(Form("Sat. Capacitance: %.1fpF at %sV\n", lastCapacitance, lastVoltage));
    inPlotInfo->AddText(Form(" Effective Thickness: %.0fum at %sV", effectiveWidth, lastVoltage));
    inPlotInfo->Draw("same");
  }
  draw_warnings();
  c_chan->Print(outputFile.c_str());
  c_chan->Delete();
  DEBUG_MSG("draw_pad finished!");
}
void flat_plot::draw_pads(const std::string &outputFile) {
  DEBUG_MSG("Executing draw_pads...\n");

  auto *c_chan = new TCanvas("c_chan", "c_chan", 800, 500, aspectRatioX, aspectRatioY);
  c_chan->cd();
  gPad->SetRightMargin(0.01);
  gPad->SetGridx();
  gPad->SetGridy();
  if (useLogVal) {
    gPad->SetLogy();
  }
  // gPad->SetLogx();

  auto *leg = new TLegend(0.1, 0.86, 0.95, 0.98);
  int nLabelsForColumn = 7;
  int nColumns = h_fill_vec.size() / nLabelsForColumn;
  int d = h_fill_vec.size() % nLabelsForColumn;
  if (d > 0) {
    nColumns++;
  }
  leg->SetNColumns(nColumns);

  // Moving from TH1F to TGraph
  for (int i = 0; i < h_fill_vec.size(); ++i) {
    if (h_fill_vec[i]->GetEntries() != h_fill_vec[i]->GetNbinsX()) {
      ERROR_MSG(
          "For pad#%d the number of entries and x binning are not the same!\n Check if there are more than one "
          "measurement at the same voltage or O.L.",
          i + 1);
    }

    // Fillig TGraph
    auto *g_current = new TGraph(h_fill_vec[i]->GetNbinsX());
    double bin_content = 0.0;
    double bin_label = 0.0;
    for (int bin = 1; bin <= h_fill_vec[i]->GetNbinsX(); bin++) {
      INFO_MSG("\tBin %d (x,y) = (%s,%f)", bin, h_fill_vec[i]->GetXaxis()->GetBinLabel(h_fill_vec[i]->GetBin(bin)),
               h_fill_vec[i]->GetBinContent(bin));

      bin_content = h_fill_vec[i]->GetBinContent(bin);
      bin_label = atof(h_fill_vec[i]->GetXaxis()->GetBinLabel(h_fill_vec[i]->GetBin(bin)));

      g_current->SetPoint(bin, bin_label, bin_content);
    }
    g_current->SetLineColor(get_color_line_fill_style(i)[0]);
    g_current->SetLineStyle(get_color_line_fill_style(i)[1]);
    g_current->SetMarkerColor(get_color_line_fill_style(i)[0]);
    if (i == 0) {
      g_current->SetTitle("");

      g_current->GetYaxis()->SetTitle(h_fill_vec[i]->GetYaxis()->GetTitle());
      g_current->GetYaxis()->SetTitleOffset(h_fill_vec[i]->GetYaxis()->GetTitleOffset());
      g_current->GetYaxis()->SetTitleSize(h_fill_vec[i]->GetYaxis()->GetTitleSize());
      g_current->GetYaxis()->SetLabelSize(h_fill_vec[i]->GetYaxis()->GetLabelSize());
      g_current->GetYaxis()->SetRangeUser(h_fill_vec[i]->GetMinimum(), h_fill_vec[i]->GetMaximum());

      g_current->GetXaxis()->SetTitle(Form("%s [%s]", selectorLabels.at(1).c_str(), selectorLabels.at(2).c_str()));
      g_current->GetXaxis()->SetTitleOffset(h_fill_vec[i]->GetXaxis()->GetTitleOffset());
      g_current->GetXaxis()->SetTitleSize(h_fill_vec[i]->GetXaxis()->GetTitleSize());
      g_current->GetXaxis()->SetLabelSize(h_fill_vec[i]->GetXaxis()->GetLabelSize());

      g_current->Draw("APL");

    } else {
      g_current->Draw("PLsame");
    }
    std::string label = h_fill_vec[i]->GetName();
    leg->AddEntry(g_current, label.c_str(), "pl");
    //  std::cout << "Graph: " << g_current->GetN() << std::endl;
    //  for (int bin = 1; bin <= h_fill_vec[i]->GetEntries(); bin++) {
    //    std::cout << "bin_content = " << g_current->GetPoint(i) << std::endl;
    //  }
  }

  // if an additional label is set, the legend is not plotted
  if (topLeftLabel == "") {
    leg->Draw();
  } else {
    TPaveText *topLeftInfo = make_label(0.1, 0.92, 0.1, 0.92, 0.03, 12);
    topLeftInfo->AddText(topLeftLabel.c_str());
    topLeftInfo->Draw("same");
  }

  c_chan->Print(outputFile.c_str());

  DEBUG_MSG("draw_pads finished!");
}
std::map<double, std::vector<double> > flat_plot::profile_pads() {
  // create a map
  // If the x-binning is not the same, the one of the first PAD is used (h_fill_vec[0])
  const int N_X_BINS = h_fill_vec[0]->GetNbinsX();

  std::map<double, std::vector<double> > map;
  INFO_MSG("map bins:\n");

  for (auto &i : h_fill_vec) {
    for (int ibin = 1; ibin <= N_X_BINS; ++ibin) {
      INFO_MSG("i->GetBinCenter(ibin): %.2f, i->GetBinContent(ibin) : %.2f", i->GetBinCenter(ibin),
               i->GetBinContent(ibin));
      remove_OF_bins(i);
      remove_INF_bins(i);
      remove_NAN_bins(i);
      if (i->GetBinContent(ibin) == 0) {
        continue;
      }
      map[atof(i->GetXaxis()->GetBinLabel(i->GetBin(ibin)))].push_back(i->GetBinContent(ibin));
    }
  }

  DEBUG_MSG("Map values in the bins:");
  for (auto &bin : map) {
    DEBUG_MSG("%f bin has values: ", bin.first);
    for (auto &value : bin.second) {
      DEBUG_MSG("%.2f", value);
    }
  }

  return map;
}
TGraphErrors *flat_plot::profile_mean(const std::map<double, std::vector<double> > map_profile,
                                      std::map<float, float> bin_widths) {
  auto *gr_mean = new TGraphErrors();

  int ibin = 0;
  float current_mean = 0.0, current_error = 0.0;
  for (auto &current_bin : map_profile) {
    // computing mean and its error
    current_mean = get_mean(current_bin.second);
    current_error = get_mean_err(current_bin.second);
    gr_mean->SetPoint(ibin, current_bin.first, current_mean);
    gr_mean->SetPointError(ibin, bin_widths.find(current_bin.first)->second, current_error);
    ibin++;
  }

  DEBUG_MSG("mean plot - nbins tot: %d\n", ibin);
  return gr_mean;
}
TGraphErrors *flat_plot::profile_median(std::map<double, std::vector<double> > map_profile,
                                        std::map<float, float> bin_widths) {
  auto *gr_median = new TGraphErrors();

  int ibin = 0, current_size = 0;
  float current_median = 0.0, current_error = 0.0;
  for (auto &current_bin : map_profile) {
    // computing median and its error
    current_median = get_median(current_bin.second);
    current_error = get_median_err(current_bin.second);
    gr_median->SetPoint(ibin, current_bin.first, current_median);
    gr_median->SetPointError(ibin, bin_widths.find(current_bin.first)->second, current_error);
    ibin++;
  }

  DEBUG_MSG("median plot - nbins tot: %d\n", ibin);
  return gr_median;
}
void flat_plot::draw_pads_stat(const std::string &outputFile, bool summary, bool yesToAll,
                               const std::string &summaryFileName, bool saving) {
  INFO_MSG("Executing draw_pads_stat...\n");
  // mean/median map

  // saving width for each bin
  std::map<float, float> bin_widths;
  for (int ibin = 1; ibin <= h_fill_vec[0]->GetNbinsX(); ++ibin) {
    bin_widths[h_fill_vec[0]->GetXaxis()->GetBinCenter(ibin)] = h_fill_vec[0]->GetXaxis()->GetBinWidth(ibin) / 2;
  }

  auto map = profile_pads();

  auto *gr_mean = profile_mean(map, bin_widths);
  auto *gr_median = profile_median(map, bin_widths);
  // gr_mean->Print();
  // gr_median->Print();

  gr_mean->SetTitle("");
  gr_mean->GetYaxis()->SetTitleOffset(1.3);
  gr_mean->SetLineColor(kRed);
  gr_mean->SetFillColor(kRed);
  gr_mean->SetFillStyle(3002);
  gr_mean->SetMarkerColor(kRed);
  gr_mean->SetMarkerStyle(20);
  gr_mean->SetMarkerSize(1);

  auto *c_mean = new TCanvas("c_mean", "c_mean", 800, 500, aspectRatioX, aspectRatioY);
  c_mean->cd();
  gPad->SetGridx();
  gPad->SetGridy();
  if (useLogVal) {
    gPad->SetLogy();
  }
  // gPad->SetLogx();
  gPad->SetRightMargin(0.01);

  gr_mean->GetYaxis()->SetTitle(h_fill_vec[0]->GetYaxis()->GetTitle());
  gr_mean->GetYaxis()->SetTitleOffset(h_fill_vec[0]->GetYaxis()->GetTitleOffset());
  gr_mean->GetYaxis()->SetTitleSize(h_fill_vec[0]->GetYaxis()->GetTitleSize());
  gr_mean->GetYaxis()->SetLabelSize(h_fill_vec[0]->GetYaxis()->GetLabelSize());
  gr_mean->GetYaxis()->SetRangeUser(h_fill_vec[0]->GetMinimum(), h_fill_vec[0]->GetMaximum());

  gr_mean->GetXaxis()->SetTitle(Form("%s [%s]", selectorLabels.at(1).c_str(), selectorLabels.at(2).c_str()));
  gr_mean->GetXaxis()->SetTitleOffset(h_fill_vec[0]->GetXaxis()->GetTitleOffset());
  gr_mean->GetXaxis()->SetTitleSize(h_fill_vec[0]->GetXaxis()->GetTitleSize());
  gr_mean->GetXaxis()->SetLabelSize(h_fill_vec[0]->GetXaxis()->GetLabelSize());

  gr_mean->Draw("apl3same");

  if (saving) {
    gr_means.push_back(gr_mean);
  }

  gr_median->SetTitle("");
  gr_median->GetYaxis()->SetTitleOffset(1.3);
  gr_median->SetLineColor(kBlue);
  gr_median->SetFillColor(kBlue);
  gr_median->SetFillStyle(3002);
  gr_median->SetMarkerColor(kBlue);
  gr_median->SetMarkerStyle(20);
  gr_median->SetMarkerSize(1);

  gr_median->GetYaxis()->SetTitle(h_fill_vec[0]->GetYaxis()->GetTitle());
  gr_median->GetYaxis()->SetTitleOffset(h_fill_vec[0]->GetYaxis()->GetTitleOffset());
  gr_median->GetYaxis()->SetTitleSize(h_fill_vec[0]->GetYaxis()->GetTitleSize());
  gr_median->GetYaxis()->SetLabelSize(h_fill_vec[0]->GetYaxis()->GetLabelSize());
  gr_median->GetYaxis()->SetRangeUser(h_fill_vec[0]->GetMinimum(), h_fill_vec[0]->GetMaximum());

  gr_median->GetXaxis()->SetTitle(Form("%s [%s]", selectorLabels.at(1).c_str(), selectorLabels.at(2).c_str()));
  gr_median->GetXaxis()->SetTitleOffset(h_fill_vec[0]->GetXaxis()->GetTitleOffset());
  gr_median->GetXaxis()->SetTitleSize(h_fill_vec[0]->GetXaxis()->GetTitleSize());
  gr_median->GetXaxis()->SetLabelSize(h_fill_vec[0]->GetXaxis()->GetLabelSize());

  gr_median->Draw("pl3same");

  if (saving) {
    gr_medians.push_back(gr_median);
  }

  auto *leg = new TLegend(0.55, 0.86, 0.95, 0.98);
  leg->AddEntry(gr_mean, "Mean", "pl");
  leg->AddEntry(gr_median, "Median", "pl");
  leg->Draw();

  c_mean->Update();
  c_mean->Print((strip_extension(outputFile) + "_stats." + get_extension(outputFile)).c_str());

  if (summary) {
    printf("Saving stats on a summary file...\n");
    create_summary_file(summaryFileName, yesToAll);
    std::ofstream fileout;
    fileout.open(summaryFileName.c_str(), std::ofstream::app);
    fileout << std::left << std::setw(50) << Form("#%s", selectorLabels.at(0).c_str()) << "\t"
            << "Mean\tRMS\tMedian\tError" << std::endl;
    fileout << std::left << std::setw(50) << Form("[%s]", selectorLabels.at(2).c_str()) << "\t"
            << Form("[%s]", valueLabels.at(2).c_str()) << std::endl;

    int N_BINS = gr_mean->GetN();
    INFO_MSG("N_BINS: %d", N_BINS);
    for (int ibin = 0; ibin < N_BINS; ++ibin) {
      double x_mean, y_mean, x_median, y_median;
      gr_mean->GetPoint(ibin, x_mean, y_mean);
      gr_median->GetPoint(ibin, x_median, y_median);

      // The binning in the TGraph is given by the points and starts from 0
      // while in the TH1F starts from 1
      std::string lineName = Form("%s", h_fill_vec[0]->GetXaxis()->GetBinLabel(h_fill_vec[0]->GetBin(x_mean) + 1));
      std::string lineVal =
          Form("%.2f\t%.2f\t%.2f\t%.2f", y_mean, gr_mean->GetErrorY(ibin), y_median, gr_median->GetErrorY(ibin));

      if (!summaryFileName.empty()) {
        fileout << std::left << std::setw(50) << lineName << "\t" << lineVal << std::endl;
        INFO_MSG("lineName: %s", lineName.c_str());
        INFO_MSG("lineVal: %s", lineVal.c_str());
      }
    }

    fileout.close();
  }

  fileout.close();

  DEBUG_MSG("draw_pads_stat finished!");
}
void flat_plot::draw_pads_stats(const std::vector<std::string> &inputFileList, const std::string &outputFile) {
  INFO_MSG("Executing draw_pads_stats...\n");
  INFO_MSG("GR_MEANS.SIZE() : %zu", gr_means.size());

  if (inputFileList.size() != gr_means.size() && gr_medians.size() != gr_means.size())
    ERROR_MSG("Each graph should belong to an input file!");

  auto *c_mean_summary = new TCanvas("c_mean_summary", "c_mean_summary", 800, 500, aspectRatioX, aspectRatioY);
  c_mean_summary->cd();
  gPad->SetGridx();
  gPad->SetGridy();
  if (useLogVal) {
    gPad->SetLogy();
  }
  // gPad->SetLogx();

  auto *leg = new TLegend(0.1, 0.86, 0.95, 0.98);
  leg->SetNColumns(2);

  // if an additional label is set, the legend header is set to that value
  if (topLeftLabel == "") {
    leg->SetHeader("Sensors mean");
  } else {
    leg->SetHeader(topLeftLabel.c_str());
  }

  for (int i = 0; i < inputFileList.size(); i++) {
    gr_means.at(i)->SetMarkerColor(get_color_line_fill_style(i)[0]);
    gr_means.at(i)->SetFillColor(get_color_line_fill_style(i)[0]);
    gr_means.at(i)->SetLineColor(get_color_line_fill_style(i)[0]);
    gr_means.at(i)->SetLineStyle(get_color_line_fill_style(i)[1]);
    if (i == 0) {
      gr_means.at(i)->Draw("apl3same");
    } else {
      gr_means.at(i)->Draw("pl3same");
    }
    leg->AddEntry(gr_means.at(i), get_file_name_no_ext(inputFileList.at(i)).c_str(), "pl");
  }

  leg->Draw();
  c_mean_summary->Print((strip_extension(outputFile) + "_means_summary." + get_extension(outputFile)).c_str());

  auto *c_median_summary = new TCanvas("c_median_summary", "c_median_summary", 800, 500, aspectRatioX, aspectRatioY);
  c_median_summary->cd();
  gPad->SetGridx();
  gPad->SetGridy();
  if (useLogVal) {
    gPad->SetLogy();
  }
  // gPad->SetLogx();

  // if an additional label is set, the legend header is set to that value
  if (topLeftLabel == "") {
    leg->SetHeader("Sensors median");
  } else {
    leg->SetHeader(topLeftLabel.c_str());
  }

  h_fill_vec[0]->Draw("axis");
  for (int i = 0; i < inputFileList.size(); i++) {
    gr_medians.at(i)->SetMarkerColor(get_color_line_fill_style(i)[0]);
    gr_medians.at(i)->SetFillColor(get_color_line_fill_style(i)[0]);
    gr_medians.at(i)->SetLineColor(get_color_line_fill_style(i)[0]);
    gr_medians.at(i)->SetLineStyle(get_color_line_fill_style(i)[1]);
    if (i == 0) {
      gr_medians.at(i)->Draw("apl3same");
    } else {
      gr_medians.at(i)->Draw("pl3same");
    }
  }

  leg->Draw();
  c_median_summary->Print((strip_extension(outputFile) + "_medians_summary." + get_extension(outputFile)).c_str());
}

void flat_plot::write_full(const std::string &outputFile, bool showMaxValue, bool yesToAll) {
  DEBUG_MSG("Executing write_full...");

  create_summary_file(outputFile, yesToAll);
  replace_val_two_col_file(outputFile, "#Categories", "Mean\tRMS");

  int nsels_help = h_fill_2D->GetNbinsY();
  int minpad_help = 1;
  int maxpad_help = h_fill_2D->GetNbinsX();

  // find asymptotic values
  auto *h_saturation = new TH1F("h_saturation", Form("h_saturation;Pad;saturation %s [%s]",
                                                     selectorLabels.at(0).c_str(), selectorLabels.at(2).c_str()),
                                maxpad_help - minpad_help + 1, minpad_help - 0.5, maxpad_help + 0.5);
  for (int i = minpad_help; i <= maxpad_help; ++i) {
    auto *h_proj = (TH1F *)h_fill_2D->ProjectionY("h_proj", i, i);  // NOLINT
    double minVal = showMaxValue ? 0 : 1;
    double maxVal = h_proj->GetBinContent(h_proj->GetMaximumBin());
    // printf("This pad %d has min %f and max %f\n",i,minVal,maxVal );
    int saturatedSel = -1;
    for (int j = 0; j < nsels_help; ++j) {
      double cont = h_fill_2D->GetBinContent(i, j + 1);
      if (showMaxValue) {
        if (cont > 0.9 * maxVal) {
          // printf("Maximum reached at %s (%f)\n",h_fill_2D->GetYaxis()->GetBinLabel(j+1),cont);
          saturatedSel = j;
          break;
        }
      } else {
        if (cont < 1.1 * minVal) {
          // printf("Minimum reached at %s (%f)\n",h_fill_2D->GetYaxis()->GetBinLabel(j+1),cont);
          saturatedSel = j;
          break;
        }
      }
    }
    if (saturatedSel != -1) {
      if (padTypeStrings.at(i - 1) == "standard") {
        h_saturation->SetBinContent(i, std::atof(h_fill_2D->GetYaxis()->GetBinLabel(saturatedSel + 1)));
      }
    }
  }
  double average = get_average_bin_content(h_saturation)[0];
  double STDDeviation = get_average_bin_content(h_saturation)[2];
  std::string lineName = Form("saturation %s standard cells at %s [%s]", valueLabels.at(0).c_str(),
                              selectorLabels.at(0).c_str(), selectorLabels.at(2).c_str());
  std::string lineVal = Form("%.2f\t%.2f", average, STDDeviation);
  replace_val_two_col_file(outputFile, lineName, lineVal);
  DEBUG_MSG("write_full finished!");
}
void flat_plot::draw_full_rel(const std::string &outputFile, bool showMaxValue) {
  DEBUG_MSG("Executing draw_full_rel...");

  int overlayColor = kRed;
  int nsels_help = h_fill_2D->GetNbinsY();
  int minpad_help = 1;
  int maxpad_help = h_fill_2D->GetNbinsX();

  // get max of each column
  auto *hmax = new TH1F("hmax", "hmax", maxpad_help - minpad_help + 1, minpad_help - 0.5, maxpad_help + 0.5);
  hmax->SetLineColor(overlayColor);
  hmax->SetLineWidth(2);
  auto *hmin = new TH1F("hmin", "hmin", maxpad_help - minpad_help + 1, minpad_help - 0.5, maxpad_help + 0.5);
  hmin->SetLineColor(overlayColor);
  hmin->SetLineWidth(2);
  for (int j = 0; j < nsels_help; ++j) {
    for (int i = minpad_help; i <= maxpad_help; ++i) {
      double binc = h_fill_2D->GetBinContent(i, j + 1);
      if (hmax->GetBinContent(i + 1) < binc || j == 0) {
        hmax->SetBinContent(i + 1, binc);
      }
      if (hmin->GetBinContent(i + 1) > binc || j == 0) {
        hmin->SetBinContent(i + 1, binc);
      }
    }
  }

  auto *h2draw = dynamic_cast<TH2F *>(h_fill_2D->Clone());
  TH1F *h_scale = nullptr;
  if (showMaxValue) {
    h_scale = dynamic_cast<TH1F *>(hmax->Clone());
  } else {
    h_scale = dynamic_cast<TH1F *>(hmin->Clone());
  }
  h2draw->Reset();
  // fill them normalised to column max
  for (int i = minpad_help; i <= maxpad_help; ++i) {
    for (int j = 0; j < nsels_help; ++j) {
      double contNew = h_fill_2D->GetBinContent(i + 1, j + 1);
      double contScale = h_scale->GetBinContent(i + 1);
      if (contScale != 0.0) {
        contNew /= contScale;
      }
      h2draw->Fill(i, j, contNew);
    }
  }

  // draw it
  auto *c_full_rel = new TCanvas("c_full_rel", "c_full_rel", 800, 500, int(aspectRatioX), int(aspectRatioY));
  c_full_rel->cd();
  // gPad->SetLogz();
  gPad->SetRightMargin(0.19);
  if (showMaxValue) {
    h2draw->GetZaxis()->SetTitle(Form("%s/%s_{max}", valueLabels.at(1).c_str(), valueLabels.at(1).c_str()));
  } else {
    h2draw->GetZaxis()->SetTitle(Form("%s/%s_{min}", valueLabels.at(1).c_str(), valueLabels.at(1).c_str()));
  }
  h2draw->GetZaxis()->SetTitleOffset(0.9);
  if (showMaxValue) {
    h2draw->GetZaxis()->SetRangeUser(0, 1);
  } else {
    h2draw->GetZaxis()->SetRangeUser(1, h2draw->GetMaximum());
  }
  // h2draw->GetZaxis()->SetNdivisions(101);
  h2draw->SetTitle("");
  h2draw->SetXTitle("Pad number");
  h2draw->SetYTitle(Form("%s [%s]", selectorLabels.at(1).c_str(), selectorLabels.at(2).c_str()));
  h2draw->Draw("COLZ");
  // scale hmax to the pad coordinates
  double rightMax = 1.1 * h_scale->GetMaximum();
  double scale = 1. * nsels_help / rightMax;
  h_scale->Scale(scale);
  h_scale->Draw("samehist");
  // draw an axis on the right side
  auto *redaxis = new TGaxis(maxpad_help + 0.5, 0, maxpad_help + 0.5, nsels_help, 0, rightMax, 110, "+L");
  if (showMaxValue) {
    redaxis->SetTitle(Form("%s_{max} [%s]", valueLabels.at(1).c_str(), valueLabels.at(2).c_str()));
  } else {
    redaxis->SetTitle(Form("%s_{min} [%s]", valueLabels.at(1).c_str(), valueLabels.at(2).c_str()));
  }
  redaxis->SetTitleOffset(1);
  redaxis->SetTitleSize(0.035);
  redaxis->SetTitleColor(overlayColor);
  redaxis->SetLabelOffset(0.01);
  redaxis->SetLabelColor(overlayColor);
  redaxis->SetLabelSize(0.035);
  redaxis->SetLineColor(overlayColor);
  redaxis->Draw();
  resize_palette_axis(h2draw, 0.895, 0.92);
  h2draw->Draw("axissame");

  c_full_rel->Print(outputFile.c_str());
  DEBUG_MSG("draw_full_rel finished!");
}
void flat_plot::draw_full_lego(const std::string &outputFile) {
  DEBUG_MSG("Executing draw_full_profiles...")
  auto *c_full_lego = new TCanvas("c_full_lego", "c_full_lego", 800, 500, int(aspectRatioX), int(aspectRatioY));
  c_full_lego->cd();
  gPad->SetRightMargin(0.19);
  h_fill_2D->GetXaxis()->SetTitleOffset(1.5);
  h_fill_2D->GetYaxis()->SetTitleOffset(1.5);
  h_fill_2D->GetZaxis()->SetTitleOffset(1.2);
  h_fill_2D->Draw("LEGO2Z");
  shift_palette_axis(h_fill_2D, 0.05);
  c_full_lego->Print(outputFile.c_str());
  DEBUG_MSG("draw_full_profiles finished!");
}
void flat_plot::draw_full_profiles(const std::string &outputFile) {
  DEBUG_MSG("Executing draw_full_profiles...");
  auto *c_full_profiles =
      new TCanvas("c_full_profiles", "c_full_profiles", 800, 500, int(aspectRatioX), int(aspectRatioY));
  c_full_profiles->cd();
  gPad->SetRightMargin(0.05);

  gPad->SetTopMargin(0.15);
  auto *leg = new TLegend(0.1, 0.86, 0.95, 0.98);
  leg->SetNColumns(7);

  TGraphErrors *gr_profile[MAX_SELECTOR_NUMBER];
  int nsels_help = h_fill_2D->GetNbinsY();
  int maxpad_help = h_fill_2D->GetNbinsX();
  double maxCont = h_fill_2D->GetMaximum();
  double minCont = h_fill_2D->GetMinimum();

  for (int i = 0; i < nsels_help; ++i) {
    int iY = i + 1;
    gr_profile[i] = new TGraphErrors();
    for (int j = 0; j < maxpad_help; ++j) {
      gr_profile[i]->SetPoint(j, j + 1, h_fill_2D->GetBinContent(j + 1, iY));
    }
    gr_profile[i]->SetLineColor(get_color_line_fill_style(i)[0]);
    gr_profile[i]->SetLineStyle(get_color_line_fill_style(i)[1]);
    if (i == 0) {
      gr_profile[i]->GetHistogram()->GetXaxis()->SetRangeUser(1, maxpad_help);
      gr_profile[i]->GetHistogram()->GetXaxis()->SetTitle(h_fill_2D->GetXaxis()->GetTitle());
      gr_profile[i]->GetHistogram()->GetYaxis()->SetTitle(h_fill_2D->GetZaxis()->GetTitle());
      gr_profile[i]->GetHistogram()->GetYaxis()->SetTitleOffset(1.3);
      if (zoomMin != zoomMax) {
        gr_profile[i]->GetHistogram()->GetYaxis()->SetRangeUser(zoomMin, zoomMax);
      } else {
        gr_profile[i]->GetHistogram()->GetYaxis()->SetRangeUser(minCont, maxCont);
      }
      gr_profile[i]->DrawClone("pla");
    } else {
      gr_profile[i]->Draw("plsame");
    }
    std::string label = Form("%s %s", h_fill_2D->GetYaxis()->GetBinLabel(iY), selectorLabels.at(2).c_str());
    leg->AddEntry(gr_profile[i], label.c_str(), "l");
  }
  leg->Draw();

  c_full_profiles->Print(outputFile.c_str());
  DEBUG_MSG("draw_full_profiles finished!");
}

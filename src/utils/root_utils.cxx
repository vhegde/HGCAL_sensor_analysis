#include "root_utils.h"

//______________________________________________________________________________
//______________________________________________________________________________
//
// STATISTICS / MATHS MAGIC
//______________________________________________________________________________
//______________________________________________________________________________

double* get_barlow_corr_factor(double m, double n, double N, int verbose, int replacement) {
  // m: events in MC sample
  // n: events in pseudo-dataset
  // N: number of pseudo-experiments

  double rho;
  double CF;
  if (replacement != 0) {
    rho = 1. - exp(-n / m);
  } else {
    rho = n / m;
  }
  CF = sqrt(1. / N + rho);
  if (N > 1) {
    CF = sqrt(1. / (N - 1) + rho);
  }  // Bessel correction for unknown population mean
  // if (rho < 0.99) CF /= 1-rho; // correlation reduces spread of the sample
  if (verbose != 0) {
    printf("BarlowCorrection: m %3.0f, n %3.0f, N %3.0f; rho = %3.5f CF %3.3f noCF %3.3f CF/noCF %3.1f\n", m, n, N, rho,
           CF, sqrt(1. / N), CF / sqrt(1. / N));
  }
  auto* value = new double[2];
  value[0] = CF;
  value[1] = rho;
  return value;
}
double round_to_n_digits(double value, int nDigits) {
  double scale = pow(10.0, nDigits - ceil(log10(fabs(value))));
  return TMath::Nint(value * scale) / scale;
}
void round_to_error_n_digits(double value, double unc, int nDigits, std::string& outputValue, std::string& outputUnc) {
  // first round error:
  double _err = round_to_n_digits(unc, nDigits);

  // now get order of number and error:
  auto ax = int(ceil(log10(fabs(value))));
  auto ae = int(ceil(log10(fabs(_err))));

  std::stringstream sval;
  std::stringstream serr;
  // check if you need more decimal 0s:
  if (nDigits - ae > 0) {
    sval << std::fixed << std::setprecision(nDigits - ae) << round_to_n_digits(value, ax - ae + nDigits);
    serr << std::fixed << std::setprecision(nDigits - ae) << _err;
  } else {
    sval << round_to_n_digits(value, ax - ae + nDigits);
    serr << _err;
  }

  // return output strings:
  outputValue = sval.str();
  outputUnc = serr.str();
}
void rounding_example() {
  std::string num, err;

  round_to_error_n_digits(128345, 22, 3, num, err);
  std::cout << "-> " << num << " " << err << std::endl;

  round_to_error_n_digits(128345, 120, 2, num, err);
  std::cout << "-> " << num << " " << err << std::endl;

  round_to_error_n_digits(12800, 5.4, 2, num, err);
  std::cout << "-> " << num << " " << err << std::endl;

  round_to_error_n_digits(12800, 0.44, 3, num, err);
  std::cout << "-> " << num << " " << err << std::endl;

  round_to_error_n_digits(0.4, 5.4, 2, num, err);
  std::cout << "-> " << num << " " << err << std::endl;

  round_to_error_n_digits(0.05, 0.44, 2, num, err);
  std::cout << "-> " << num << " " << err << std::endl;

  round_to_error_n_digits(0.05689, 0.44, 2, num, err);
  std::cout << "-> " << num << " " << err << std::endl;

  round_to_error_n_digits(0.05689, 0.005, 2, num, err);
  std::cout << "-> " << num << " " << err << std::endl;

  round_to_error_n_digits(0.04920, 0.012988, 1, num, err);
  std::cout << "-> " << num << " " << err << std::endl;
}
double unc_of_diff(double unc0, double unc1, double rho, double factor) {
  double unc = factor * sqrt(pow(unc0, 2) + pow(unc1, 2) - 2 * rho * unc0 * unc1);
  return unc;
}
double unc_of_ratio(double n, double N, double ne, double Ne, double corr, bool verbose) {
  double val = -1;
  if (N != 0) {
    double nunc = ne / N;
    double Nunc = Ne * n / pow(N, 2);
    val = sqrt(pow(nunc, 2) + pow(Nunc, 2) - 2 * corr * nunc * Nunc);
  }
  if (verbose) {
    printf("%f / %f = %f +- %f = %.1f +- %.1f%% (rho = %.2f)\n", n, N, 1. * n / N, val, 100. * n / N, 100 * val, corr);
  }
  return val;
}
double unc_of_ratio(int n, int N, double corr, bool verbose) {
  // double val=-1;
  // if (N != 0){
  //  double ne=sqrt(1.*n);
  //  double Ne=sqrt(1.*N);
  //  double nunc=ne/N;
  //  double Nunc=Ne*n/pow(N,2);
  //  val=sqrt( pow(nunc,2)+pow(Nunc,2)-2*corr*nunc*Nunc);
  // }
  // if (v) printf("%f / %f = %f +- %f = %.1f +- %.1f%% (rho = %.2f)\n",n,N,1.*n/N,val,100.*n/N,100*val,corr);
  // return val;
  double ne = sqrt(1. * n);
  double Ne = sqrt(1. * N);
  return unc_of_ratio(n, N, ne, Ne, corr, verbose);
}
double unc_of_ratio(int n, int N, bool verbose) {
  // double val=-1;
  // if (N != 0) val=sqrt(1.*pow(n,1)/pow(N,2)+pow(n,2)/pow(N,3));
  // if (v) printf("%d / %d = %f +- %f = %.1f +- %.1f%%\n",n,N,1.*n/N,val,100.*n/N,100*val);
  //  return val;
  return unc_of_ratio(n, N, 0, verbose);
}
double get_phi_from_xy(double x, double y) {
  double radangle = TMath::ASin(y / sqrt(pow(x, 2) + pow(y, 2)));
  if (x < 0) {
    if (y > 0) {
      radangle = TMath::Pi() - fabs(radangle);
    } else {
      radangle = -TMath::Pi() + fabs(radangle);
    }
  }
  // double degangle=radangle*360/(2*TMath::Pi());
  return radangle;
}
double get_corr_factor_unc(TH2F* h) {
  std::cout << "Warning: get_corr_factor_unc not fully implemented yet!" << std::endl;
  double n = h->GetEntries();
  double rho = h->GetCorrelationFactor();
  double rhoe = (1 - pow(rho, 2)) / sqrt(n - 1);
  return rhoe;
  // http://de.mathworks.com/matlabcentral/newsreader/view_thread/107045
  // ---> Hotelling, H. (1953). New light on the correlation coefficient and its transforms. Journal of the Royal
  // Statistical Society, Series B, 15(2), 193-232.
  // ---> Ghosh, B. K. (1966). Asymptotic expansions for the moments of the distribution of correlation coefficient.
  // Biometrika, 53(1/2), 258-262.
}
double* get_min_hist_region(TH1F* h, double frac, const std::string& quant, bool verbose) {
  double oldmin = h->GetXaxis()->GetXmin();
  double oldmax = h->GetXaxis()->GetXmax();
  double oldintegral = h->Integral();
  double oldrange = oldmax - oldmin;
  double tmprange(oldrange);
  double finalmin(oldmin);
  double finalmax(oldmax);
  auto* value = new double[3];
  for (int i = 0; i < h->GetNbinsX(); ++i) {
    for (int j = i + 1; j < h->GetNbinsX(); ++j) {
      double newmin = h->GetBinCenter(i + 1) - h->GetBinWidth(i + 1) / 2;
      double newmax = h->GetBinCenter(j + 1) + h->GetBinWidth(j + 1) / 2;
      double newrange = newmax - newmin;
      h->GetXaxis()->SetRangeUser(newmin, newmax);
      double newintegral = h->Integral();
      if (newrange >= tmprange) {
        break;
      }
      // if (verbose)  printf("Bin %d/%d %.2f - %.2f: %.2f %.2f %.2f %.2f
      // %.2f\n",i,j,newmin,newmax,newrange,tmprange,newintegral,oldintegral,newintegral/oldintegral );
      if (newrange < tmprange && newintegral / oldintegral >= frac) {
        tmprange = newrange;
        finalmin = newmin;
        finalmax = newmax;
        if (quant == "RMS") {
          value[0] = h->GetRMS();
        }
        if (quant == "StdDev") {
          value[0] = h->GetStdDev();
        }
        if (quant == "Mean") {
          value[0] = h->GetMean();
        }
        if (quant == "MeanError") {
          value[0] = h->GetMean();
        }
        if (quant.empty()) {
          value[0] = -1;
        }
        if (verbose) {
          printf("Bin %d/%d: new smallest region %.2f - %.2f with integral %.2f and %s = %.2f\n", i, j, newmin, newmax,
                 newintegral, quant.c_str(), value[0]);
        }
      }
    }
  }
  value[1] = finalmin;
  value[2] = finalmax;
  h->GetXaxis()->SetRangeUser(oldmin, oldmax);
  return value;
}
double incr_mean_calc(double oldMean, int newN, double newVal) { return (oldMean * (newN - 1) + newVal) / newN; }
double incr_STD_dev_calc(double oldSigma, double oldMean, int newN, double newVal) {
  printf("Doesn't work yet!\n");
  oldSigma = oldSigma;
  oldMean = oldMean;
  newVal = newVal;
  return 0;
  // as shown on http://math.stackexchange.com/questions/102978/incremental-computation-of-standard-deviation
  if (newN < 2) {
    return 0;
  }
  // return sqrt(1.*(newN-2)/(newN-1)*pow(oldSigma,2)+1./newN*pow((newVal-oldMean),2)); //comment
  // return
  // sqrt(((newN-2)*pow(oldSigma,2)+(newN-1)*pow(oldMean-incr_mean_calc(oldMean,newN,newVal),2)+pow(newVal-incr_mean_calc(oldMean,newN,newVal),2)
  // ) /(newN-1) );

  // as shown on http://mathcentral.uregina.ca/qq/hDatabase/qq.09.06/h/murtaza2.html
  // double s=oldSigma;
  // double SSx=(newN-2)*pow(s,2)+1./(newN-1)*pow(Sx,2);
  // double newSSx=SSx+pow(newVal,2);
  // double Sx=(newN-1)*oldMean;
  // double newSx=Sx+newVal;
  // double newvariance=1./(newN*(newN-1))* ((newN)*newSSx-pow(newSx,2));
  // return newvariance;
}
void RMS_test(int nmax) {
  double tmpmean(0);
  double tmpRMS(0);
  double tmpStd(0);
  auto* h = new TH1F("h", "h", 100, 0, 10);
  double valarr[10] = {2, 4, 5, 3, 4, 5, 7, 2, 4, 1};
  for (int i = 0; i < nmax; ++i) {
    printf("Adding %f\n", valarr[i]);
    double oldMean = tmpmean;
    tmpmean = incr_mean_calc(oldMean, i + 1, valarr[i]);
    tmpRMS = incr_STD_dev_calc(tmpRMS, oldMean, i + 1, valarr[i]);
    h->Fill(valarr[i]);
    printf("     Mine    Histogram\n");
    printf("Mean %f   %f\n", tmpmean, h->GetMean());
    printf("RMS  %f   %f\n", tmpRMS, h->GetRMS());
    printf("Std  %f   %f\n", tmpStd, h->GetStdDev());
  }

  printf("Final result:\n");
  printf("     Mine    Histogram    By hand\n");
  printf("Mean %f   %f\n", tmpmean, h->GetMean());
  printf("RMS  %f   %f\n", tmpRMS, h->GetRMS());
  printf("Std  %f   %f\n", tmpStd, h->GetStdDev());
}
double get_delta_R(double eta0, double phi0, double eta1, double phi1) {
  double deta = eta0 - eta1;
  double dphi = TVector2::Phi_mpi_pi(phi0 - phi1);
  return TMath::Sqrt(deta * deta + dphi * dphi);
}
double get_eta_from_theta(double theta) {
  if (theta < 0 || theta > TMath::Pi()) {
    printf("Warning: theta = %.3f but it has to be 0 < theta < Pi\n", theta);
    return std::numeric_limits<double>::quiet_NaN();
  }
  return -TMath::Log(TMath::Tan(theta / 2));
}
double get_theta_from_eta(double eta) { return 2 * TMath::ATan(exp(-eta)); }

//______________________________________________________________________________
//______________________________________________________________________________
//
// ROOT MAGIC
//______________________________________________________________________________
//______________________________________________________________________________
const static int NVISIBLECOLORS = 15;
static int mycolorvisible[NVISIBLECOLORS] = {kBlack, 62,       kViolet, 95, 92, kGreen + 3, kRed,   kTeal,
                                             kGreen, kMagenta, 15,      28, 46, kBlue,      kViolet};
static int mycolorvisiblenoblack[NVISIBLECOLORS - 1] = {62,     kViolet,  95, 92, kGreen + 3, kRed,  kTeal,
                                                        kGreen, kMagenta, 15, 28, 46,         kBlue, kViolet};
const static int NSTYLES = 2;
static int mystyles[NSTYLES] = {3001, 3244};

int* get_color_line_fill_style(int number) {
  int color = mycolorvisible[number % NVISIBLECOLORS];
  int linestyle = 1 + int(number / NVISIBLECOLORS);
  int fillstyle = mystyles[int(number / NVISIBLECOLORS) % NSTYLES];
  auto* value = new int[3];
  value[0] = color;
  value[1] = linestyle;
  value[2] = fillstyle;
  // printf("Number %d -> color %d, linestyle %d, fillstyle %d\n", number, color, linestyle, fillstyle);
  return value;
}
int* get_color_line_fill_style_no_black(int number) {
  int color = mycolorvisiblenoblack[number % (NVISIBLECOLORS - 1)];
  int linestyle = 1 + int(number / (NVISIBLECOLORS - 1));
  int fillstyle = mystyles[int(number / (NVISIBLECOLORS - 1)) % NSTYLES];
  auto* value = new int[3];
  value[0] = color;
  value[1] = linestyle;
  value[2] = fillstyle;
  return value;
}

TH1F* cut_off_bins(TH1F* h, int nLastBins) {
  printf("Cutting off the last %d bins of histogram %s with %d bins and integral %f, ranging from %.1f to %.1f!\n",
         nLastBins, h->GetName(), h->GetNbinsX(), h->Integral(), h->GetXaxis()->GetXmin(), h->GetXaxis()->GetXmax());
  int newnbins = h->GetNbinsX() - nLastBins;
  double newmin = h->GetXaxis()->GetXmin();
  double newmax = h->GetBinCenter(newnbins) + h->GetBinWidth(newnbins) / 2;
  auto* h_cut = new TH1F(h->GetName(), h->GetTitle(), newnbins, newmin, newmax);
  for (int i = 0; i < newnbins; ++i) {
    h_cut->SetBinContent(i + 1, h->GetBinContent(i + 1));
    h_cut->SetBinError(i + 1, h->GetBinError(i + 1));
    // printf("Filled bin %d at x= %.2f with %f +-
    // %f\n",i+1,h->GetBinCenter(i+1),h->GetBinContent(i+1),h->GetBinError(i+1) );
  }
  delete h;
  printf("Cutting done! New histogram has %d bins and ranges from %.1f to %.1f\n", h_cut->GetNbinsX(),
         h_cut->GetXaxis()->GetXmin(), h_cut->GetXaxis()->GetXmax());
  return h_cut;
}
void save_pad(TPad* pad, const char* saveName, int x, int y) {
  // Resize the pad to full size
  auto* temp = new TCanvas("temp", "temp", x, y);
  temp->cd();
  auto* clone = dynamic_cast<TPad*>(pad->Clone());
  clone->SetPad(0, 0, 1, 1);
  clone->Draw();
  temp->Print(saveName);
  delete temp;
}
void save_pad(TVirtualPad* pad, const char* saveName, int x, int y) {
  save_pad(dynamic_cast<TPad*>(pad), saveName, x, y);
}
int remove_negative_bins(TH1* hist) {
  int nnegbins = 0;
  for (int mb = 1; mb <= hist->GetNbinsX(); mb++) {
    double cont = hist->GetBinContent(mb);
    if (cont < 0) {
      hist->SetBinContent(mb, 0);
      ++nnegbins;
    }
  }
  if (nnegbins != 0) {
    printf("Warning: set %d negative bins in histogram %s to 0!\n", nnegbins, hist->GetName());
  }
  return nnegbins;
}
int remove_NAN_bins(TH1* hist) {
  int NaNbins = 0;
  for (int mb = 1; mb <= hist->GetNbinsX(); mb++) {
    double cont = hist->GetBinContent(mb);
    if (cont != cont) {
      hist->SetBinContent(mb, DBL_MAX);
      ++NaNbins;
    }
  }
  if (NaNbins != 0) {
    printf("Warning: set %d NaN bins in histogram %s to %e!\n", NaNbins, hist->GetName(), DBL_MAX);
  }
  return NaNbins;
}
int remove_OF_bins(TH1* hist, double OFvalue) {
  int OFbins = 0;
  for (int mb = 1; mb <= hist->GetNbinsX(); mb++) {
    double cont = hist->GetBinContent(mb);
    if (cont > OFvalue) {
      hist->SetBinContent(mb, DBL_MAX);
      ++OFbins;
    }
  }
  if (OFbins != 0) {
    printf("Warning: set %d OF bins in histogram %s to %e!\n", OFbins, hist->GetName(), DBL_MAX);
  }
  return OFbins;
}
int remove_INF_bins(TH1* hist) {
  int INFbins = 0;
  for (int mb = 1; mb <= hist->GetNbinsX(); mb++) {
    double cont = hist->GetBinContent(mb);
    if (isinf(cont) != 0) {
      hist->SetBinContent(mb, DBL_MAX);
      ++INFbins;
    }
  }
  if (INFbins != 0) {
    printf("Warning: set %d INF bins in histogram %s to %e!\n", INFbins, hist->GetName(), DBL_MAX);
  }
  return INFbins;
}
void make_bins_absolute_values(TH1F* hist) {
  double bcont = 0;
  for (int mb = 1; mb <= hist->GetNbinsX(); mb++) {
    bcont = hist->GetBinContent(mb);
    if (bcont < 0) {
      hist->SetBinContent(mb, -bcont);
    }
  }
}
void make_bins_square_roots(TH1F* hist) {
  double bcont = 0;
  for (int mb = 1; mb <= hist->GetNbinsX(); mb++) {
    bcont = hist->GetBinContent(mb);
    if (bcont > 0) {
      hist->SetBinContent(mb, sqrt(bcont));
    }
  }
}
void add_symm_bin_err_from_diff(TH1F* hErr, TH1F* hOrig, TH1F* hVarUp, TH1F* hVarDw, bool noNormUnc) {
  double int_up = hVarUp->Integral();
  double int_dw = hVarDw->Integral();
  // double integral = hOrig->Integral();

  for (int mb = 1; mb <= hErr->GetNbinsX(); mb++) {
    double bcont = hOrig->GetBinContent(mb);
    double bcont_up = hVarUp->GetBinContent(mb);
    double bcont_dw = hVarDw->GetBinContent(mb);
    double sym_err = (fabs(bcont - bcont_up) + fabs(bcont - bcont_dw)) / 2;
    double old_err = hErr->GetBinContent(mb);
    double new_err = 0;
    if (!noNormUnc) {
      new_err = sqrt(pow(sym_err, 2) + pow(old_err, 2));
    }
    if (noNormUnc) {
      new_err = sqrt(pow(fabs(bcont_up / int_up - bcont_dw / int_dw) / 2 * bcont, 2) + pow(old_err, 2));
    }

    hErr->SetBinContent(mb, new_err);
    // std::cout<<mb<<": bcont "<<bcont<<", old_err "<<old_err<<", bcont_up "<<bcont_up<<", bcont_dw "<<bcont_dw<<",
    // new_err "<<hErr->GetBinContent(mb)<<std::endl;
  }
}
void set_bin_err_from_diff(TGraphAsymmErrors* grOrig, TGraph* grVar0, TGraph* grVar1) {
  for (int mb = 0; mb < grOrig->GetN(); mb++) {
    double cont_orig = get_graph_value(grOrig, mb);
    double cont_var0 = get_graph_value(grVar0, mb);
    double cont_var1 = get_graph_value(grVar1, mb);
    double max = TMath::Max(TMath::Max(cont_var0, cont_var1), cont_orig);
    double min = TMath::Min(TMath::Min(cont_var0, cont_var1), cont_orig);
    double eyl = fabs(cont_orig - min);
    double eyh = fabs(cont_orig - max);
    grOrig->SetPointError(mb, 0, 0, eyl, eyh);
  }
}
void set_bin_err_from_diff(TH1F* hOrig, TH1F* hVar0, TH1F* hVar1, bool norm) {
  if (norm) {
    hOrig->Scale(1. / hOrig->Integral());
    hVar0->Scale(1. / hVar0->Integral());
    hVar1->Scale(1. / hVar1->Integral());
  }
  for (int mb = 0; mb < hOrig->GetNbinsX(); mb++) {
    double cont_orig = hOrig->GetBinContent(mb + 1);
    double cont_var0 = hVar0->GetBinContent(mb + 1);
    double cont_var1 = hVar1->GetBinContent(mb + 1);
    double max = TMath::Max(TMath::Max(cont_var0, cont_var1), cont_orig);
    double min = TMath::Min(TMath::Min(cont_var0, cont_var1), cont_orig);
    // bool sameside = (cont_orig-cont_var0)*(cont_orig-cont_var1) > 0;
    // double error = sameside ? TMath::Max(fabs(cont_orig-max),fabs(cont_orig-min)) : (max-min)/2;
    double error = TMath::Max(fabs(cont_orig - max), fabs(cont_orig - min));
    hOrig->SetBinError(mb + 1, error);
    // printf("Bin %d x = %.2f orig %.3f var0 %.3f var1 %.3f error %.3f\n", mb+1, hOrig->GetBinCenter(mb+1), cont_orig,
    // cont_var0, cont_var1, hOrig->GetBinError(mb+1) );
  }
}
double get_hist_chi2_diff(TH1F* h1, TH1F* h2, bool norm, double xMin, double xMax) {
  int badbincounter(0);
  int emptybincounter(0);
  // h2 is considered error free
  double chi2 = 0;
  if (xMin == xMax) {
    xMin = h1->GetXaxis()->GetXmin();
  }
  if (xMin == xMax) {
    xMax = h1->GetXaxis()->GetXmax();
  }
  double int1 = 1.;
  double int2 = 1.;
  if (norm) {
    int1 = h1->Integral();
  }
  if (norm) {
    int2 = h2->Integral();
  }
  if (h1->GetNbinsX() != h2->GetNbinsX()) {
    return -1;
  }
  for (int mb = 1; mb <= h1->GetNbinsX(); mb++) {
    double binx = h1->GetBinCenter(mb);
    double bine1 = h1->GetBinError(mb) / int1;
    if (binx < xMin || binx > xMax) {
      continue;
    }
    double binc1 = h1->GetBinContent(mb) / int1;
    double binc2 = h2->GetBinContent(mb) / int2;
    if (bine1 == 0) {
      if (binc1 == 0) {
        ++emptybincounter;
      } else {
        std::cout << "Warning: Bin " << mb << " error = 0 (x=" << binx << ", y=" << binc1 << "). Omitting bin."
                  << std::endl;
        ++badbincounter;
      }
      continue;
    }
    chi2 += pow((binc1 - binc2) / bine1, 2);
    // printf("bin %d chi2 %.3f h %.3f f %.3f he %.3f\n", mb, chi2, binc1,binc2,bine1);
  }
  if (badbincounter != 0) {
    printf("Warning: omitted %d bins with 0 error\n", badbincounter);
  }
  if (emptybincounter != 0) {
    printf("Warning: omitted %d empty bins\n", emptybincounter);
  }
  return chi2;
}
double get_hist_chi2_diff(TH1F* hist, TF1* func, bool norm, double xMin, double xMax) {
  int badbincounter(0);
  // func is considered error free
  if (xMin == xMax) {
    xMin = hist->GetXaxis()->GetXmin();
  }
  if (xMin == xMax) {
    xMax = hist->GetXaxis()->GetXmax();
  }
  double chi2 = 0;
  double inth = 1.;
  double intf = 1.;
  if (norm) {
    inth = hist->Integral();
  }
  if (norm) {
    intf = func->Integral(hist->GetXaxis()->GetXmin(), hist->GetXaxis()->GetXmax());
  }
  if (inth == 0 || intf == 0) {
    printf("Error: integral histogram %f function %f\n", inth, intf);
    return 0;
  }
  for (int mb = 1; mb <= hist->GetNbinsX(); mb++) {
    double binx = hist->GetBinCenter(mb);
    double bineh = hist->GetBinError(mb) / inth;
    // printf("histo error %f integral %f\n", hist->GetBinError(mb),inth);
    if (binx < xMin || binx > xMax) {
      continue;
    }
    double binch = hist->GetBinContent(mb) / inth;
    double bincf = func->Eval(binx) / intf;
    if (bineh == 0) {
      std::cout << "Warning: Bin " << mb << " error = 0 (x=" << binx << ", y=" << binch << "). Omitting bin."
                << std::endl;
      ++badbincounter;
      continue;
    }
    chi2 += pow((binch - bincf) / bineh, 2);
    // printf("bin %d chi2 %.3f h %.3f f %.3f he %.3f\n", mb, chi2, binch,bincf,bineh);
  }
  if (badbincounter != 0) {
    printf("Warning: %d bins with 0 error, probably empty\n", badbincounter);
  }
  // printf("Final chi2 %.2f\n",chi2);
  return chi2;
}
void smear_bins(TH1F* hist, TH1F* hUnc, int seed) {
  auto* rdm = new TRandom();
  rdm->SetSeed(seed);
  double bcont = 0;
  double berr = 0;
  double newcont = 0;
  for (int mb = 1; mb <= hist->GetNbinsX(); mb++) {
    bcont = hist->GetBinContent(mb);
    berr = hUnc->GetBinContent(mb);
    newcont = rdm->Gaus(bcont, berr);
    hist->SetBinContent(mb, newcont);
  }
  delete rdm;
}
void smear_bins(TH1F* hist, int seed) {
  auto* hUnc = dynamic_cast<TH1F*>(hist->Clone());
  make_bins_square_roots(hUnc);
  smear_bins(hist, hUnc, seed);
}
void print_matrix(const TMatrixD& m, std::string format, const std::string& name, int colsPerSheet) {
  // Print the matrix as a table of elements.
  // Based on TMatrixTBase<>::Print, but allowing user to specify name and colsPerSheet (also option -> format).
  // By default the format "%11.4g" is used to print one element.
  // One can specify an alternative format with eg
  //  format ="%6.2f  "

  if (!m.IsValid()) {
    m.Error("print_matrix", "%s is invalid", name.c_str());
    return;
  }

  const int ncols = m.GetNcols();
  const int nrows = m.GetNrows();
  const int collwb = m.GetColLwb();
  const int rowlwb = m.GetRowLwb();

  if (format.empty()) {
    format = "%8.4g ";
  }
  char topbar[1000];
  snprintf(topbar, 1000, format.c_str(), 123.456789);
  int nch = strlen(topbar) + 1;
  if (nch > 18) {
    nch = 18;
  }
  char ftopbar[20];
  for (int i = 0; i < nch; i++) {
    ftopbar[i] = ' ';
  }
  int nk = 1 + int(log10(ncols));
  snprintf(ftopbar + nch / 2, 20 - nch / 2, "%s%dd", "%", nk);
  int nch2 = strlen(ftopbar);
  for (int i = nch2; i < nch; i++) {
    ftopbar[i] = ' ';
  }
  ftopbar[nch] = '|';
  ftopbar[nch + 1] = 0;

  printf("%dx%d %s is as follows", nrows, ncols, name.c_str());

  if (colsPerSheet <= 0) {
    colsPerSheet = 5;
    if (nch <= 8) {
      colsPerSheet = 10;
    }
  }
  nk = 5 + nch * (colsPerSheet < ncols ? colsPerSheet : ncols);
  for (int i = 0; i < nk + 1; i++) {
    topbar[i] = '-';
  }
  topbar[nk + 1] = 0;
  for (int sheet_counter = 1; sheet_counter <= ncols; sheet_counter += colsPerSheet) {
    printf("\n\n     | ");
    for (int j = sheet_counter; j < sheet_counter + colsPerSheet && j <= ncols; j++) {
      printf(ftopbar, j + collwb - 1);
    }
    printf("\n%s\n", topbar);
    if (m.GetNoElements() <= 0) {
      continue;
    }
    for (int i = 1; i <= nrows; i++) {
      printf("%4d |", i + rowlwb - 1);
      for (int j = sheet_counter; j < sheet_counter + colsPerSheet && j <= ncols; j++) {
        printf(Form(" %s", format.c_str()), m(i + rowlwb - 1, j + collwb - 1));
      }
      printf("\n");
    }
  }
  printf("\n");
}
TMatrixD* remove_row_column(TMatrixD* m, std::vector<int> rows, std::vector<int> cols, bool verbose) {
  const int ncols = m->GetNcols();
  const int nrows = m->GetNrows();
  // const int collwb = m->GetColLwb();
  // const int rowlwb = m->GetRowLwb();

  if (rows.empty() && cols.empty()) {
    printf("remove_row_column: no row or column to remove specified!\n");
    return nullptr;
  }
  if (verbose) {
    printf("Removing rows ");
    for (int row : rows) {
      printf("%d ", row);
    }
    printf(" and columns ");
    for (int col : cols) {
      printf("%d ", col);
    }
    printf("\n");
  }

  int new_nrows = nrows - rows.size();
  int new_ncols = ncols - cols.size();

  // print warning if index appears twice
  for (unsigned int i = 0; i < rows.size(); ++i) {
    for (unsigned int j = i + 1; j < rows.size(); ++j) {
      if (rows.at(i) == rows.at(j)) {
        printf("Warning: row index %d appears more than once! Returnin 0!\n", rows.at(j));
        return nullptr;
      }
    }
  }
  for (unsigned int i = 0; i < cols.size(); ++i) {
    for (unsigned int j = i + 1; j < cols.size(); ++j) {
      if (cols.at(i) == cols.at(j)) {
        printf("Warning: column index %d appears more than once! Returning 0!\n", cols.at(j));
        return nullptr;
      }
    }
  }

  TMatrixD m_new(new_nrows, new_ncols);
  int new_i = 0;
  for (int i = 0; i < nrows; ++i) {
    bool skipthat = false;
    for (int row : rows) {
      if (i == row) {
        skipthat = true;
      }
    }
    if (skipthat) {
      continue;
    }
    int new_j = 0;
    for (int j = 0; j < ncols; ++j) {
      skipthat = false;
      for (int col : cols) {
        if (j == col) {
          skipthat = true;
        }
      }
      if (skipthat) {
        continue;
      }
      // printf("%d %d %d %d\n",new_i,new_j++,i,j );
      m_new(new_i, new_j++) = (*m)(i, j);
    }
    new_i++;
  }
  m = dynamic_cast<TMatrixD*>(m_new.Clone());
  // if (verbose) print_matrix(*m,"","matrix after removal",30);
  return m;
}
TMatrixD* remove_row_column(TMatrixD* m, int row, int col, int verbose) {
  std::vector<int> rows;
  std::vector<int> cols;
  rows.push_back(row);
  cols.push_back(col);
  return remove_row_column(m, rows, cols, verbose != 0);
}
TH2D* convert_matrix_to_hist(TMatrixD m) {
  TIter next(gDirectory->GetList());
  TH2D* h_del;
  TObject* obj;
  while ((obj = next()) != nullptr) {
    if (obj->InheritsFrom(TH2D::Class())) {
      h_del = dynamic_cast<TH2D*>(obj);
      if (std::string(h_del->GetName()).find("hconvert_matrix_to_hist") < 2) {
        h_del->Delete();
      }
    }
  }
  auto* h = new TH2D("hconvert_matrix_to_hist", "hconvert_matrix_to_hist", m.GetNrows(), 0, m.GetNrows(), m.GetNcols(),
                     0, m.GetNcols());
  const double* pData = m.GetMatrixArray();
  int index(0);
  for (int irow = 0; irow < m.GetNrows(); irow++) {
    for (int icol = 0; icol < m.GetNcols(); icol++) {
      h->Fill(irow, icol, pData[index++]);
    }
  }
  return h;
}
TMatrixD convert_hist_to_matrix(TH2D* h) {
  int nrows = h->GetNbinsY();
  int ncols = h->GetNbinsX();
  TMatrixD m(nrows, ncols);
  for (int i = 0; i < nrows; i++) {
    for (int j = 0; j < ncols; j++) {
      m(i, j) = h->GetBinContent(i + 1, j + 1);
    }
  }
  return m;
}
void scale_bins(TH1F* hist, double scale) {
  // like TH1::Scale() but error goes like scale instead of sqrt(scale)
  for (int mb = 1; mb <= hist->GetNbinsX(); mb++) {
    double bcont = hist->GetBinContent(mb);
    double berr = hist->GetBinError(mb);
    hist->SetBinContent(mb, bcont * scale);
    hist->SetBinError(mb, berr * scale);
  }
}
TH1F* function_ratio(TF1* f0, TF1* f1, double min, double max, int nPoints) {
  auto* hratio = new TH1F("hratio", "hratio", nPoints, min, max);
  for (int i = 0; i < hratio->GetNbinsX(); ++i) {
    double x = hratio->GetBinCenter(i + 1);
    double val0 = f0->Eval(x);
    double val1 = f1->Eval(x);
    double ratio = val0 / val1;
    hratio->SetBinContent(i + 1, ratio);
  }
  hratio->SetLineColor(f0->GetLineColor());
  hratio->SetLineWidth(2);
  return hratio;
}
double average_error_per_bin(TH1F* h) {
  double err(0);
  for (int i = 0; i < h->GetNbinsX(); ++i) {
    err += h->GetBinError(i + 1);
  }
  err /= h->GetNbinsX();
  return err;
}
double cumulative_bin_residual(TH1F* h, TH1F* g) {
  double diff(0);
  for (int i = 0; i < h->GetNbinsX(); ++i) {
    diff += h->GetBinContent(i + 1) - g->GetBinContent(i + 1);
  }
  return diff;
}
double get_maximum(TGraph* gr, bool extraTopSpace) {
  double max = -MAX_DOUBLE;
  double xval(0), yValue(0);
  for (int i = 0; i < gr->GetN(); ++i) {
    gr->GetPoint(i, xval, yValue);
    double newmax = yValue + (extraTopSpace ? gr->GetErrorY(i) * 1.5 : 0);
    if (i == 0 || newmax > max) {
      max = newmax;
    }
  }
  // printf("Graph %s has maximum %.2e\n", gr->GetName(), max);
  return max;
}
double get_minimum(TGraph* gr, bool extraBottomSpace) {
  double min = MAX_DOUBLE;
  double xval(0), yValue(0);
  for (int i = 0; i < gr->GetN(); ++i) {
    gr->GetPoint(i, xval, yValue);
    double newmin = yValue - (extraBottomSpace ? gr->GetErrorY(i) * 1.5 : 0);
    if (i == 0 || newmin < min) {
      min = newmin;
    }
  }
  // printf("Graph %s has minimum %.2e\n", gr->GetName(), min);
  return min;
}
double* get_plot_ranges(TGraphErrors** gr, int nGraphs, int skipNGraphs, bool extraTopSpace) {
  std::vector<int> onlyThose;
  for (int i = skipNGraphs; i < skipNGraphs + nGraphs; i++) {
    onlyThose.push_back(i);
  }
  return get_plot_ranges(gr, onlyThose, extraTopSpace);
}
double* get_plot_ranges(TGraphErrors** gr, std::vector<int> onlyThose, bool extraTopSpace) {
  auto* value = new double[2];
  double absMax = -MAX_DOUBLE;
  double absMin = MAX_DOUBLE;
  for (int i : onlyThose) {
    TGraphErrors* gr_dummy = gr[i];
    double thisMax = get_maximum(gr_dummy);
    double thisMin = get_minimum(gr_dummy);
    if (absMax < thisMax) {
      absMax = thisMax;
    }
    if (absMin > thisMin) {
      absMin = thisMin;
    }
  }
  double totalRange = absMax - absMin;
  value[0] = absMin - (extraTopSpace ? totalRange * 1.05 : 0);
  value[1] = absMax + (extraTopSpace ? totalRange * 1.05 : 0);
  // printf("Final total range for graphs %.2e to %.2e\n", value[0], value[1] );
  return value;
}
void shift_x_values(TGraph* gr, double shift) {
  double* xvals = gr->GetX();
  double* yValues = gr->GetY();
  for (int i = 0; i < gr->GetN(); ++i) {
    gr->SetPoint(i, xvals[i] + shift, yValues[i]);
  }
}
void shift_x_values(TGraph* gr, double maxShift, int thisIdx, int nGraphs) {
  shift_x_values(gr, -maxShift / 2 + thisIdx * maxShift / (nGraphs - 1));
}
void print_TTree_to_txt(const std::string& fileName, int maxEvts) {
  auto* f = TFile::Open(fileName.c_str());
  auto* t = dynamic_cast<TTree*>(f->Get("nominal"));
  auto* tp = static_cast<TTreePlayer*>(t->GetPlayer());
  tp->SetScanRedirect(kTRUE);
  tp->SetScanFileName("output.txt");
  // std::string varexp = "*"; //all branches
  std::string varexp =
      "ssf:eventNumber:runNumber:channel_DL:lep_isTight:lep_trigMatched:lep_type:lep_pt:lep_eta:lep_phi:lep_E:lep_"
      "tracksigd0pvunbiased:lep_trackd0pvunbiased:lep_d0sig:el_cl_eta:lep_dR_lj_min:lep_dPhi_lj_min:lep_dEta_lj_min:"
      "lep_RealEff:lep_FakeEff";
  std::string selection = "jet_pt>45 && channel_DL == 3";
  if (maxEvts != 0) {
    t->Scan(varexp.c_str(), selection.c_str(), "", maxEvts);
  } else {
    t->Scan(varexp.c_str(), selection.c_str());
  }
}
double bin_positive_error(TH1F* hist, int bin) {
  double cont = hist->GetBinContent(bin);
  double err = hist->GetBinError(bin);
  int nbinhi = bin;
  while (nbinhi < hist->GetNbinsX()) {
    ++nbinhi;
    double bincont = hist->GetBinContent(nbinhi);
    double binerr = hist->GetBinError(nbinhi);
    double comperr = sqrt(pow(err, 2) + pow(binerr, 2));
    // printf("hist: bin %d cont %f +- %f test: bin %d cont %f +- %f\n",bin,cont,err,nbinhi,bincont,binerr);
    if ((bincont < cont && bincont + comperr < cont) || (bincont > cont && bincont - comperr > cont)) {
      // printf("break\n");
      break;
    }
  }
  int nbinlo = bin;
  while (nbinlo > 0) {
    --nbinlo;
    double bincont = hist->GetBinContent(nbinlo);
    double binerr = hist->GetBinError(nbinlo);
    double comperr = sqrt(pow(err, 2) + pow(binerr, 2));
    // printf("hist: bin %d cont %f +- %f test: bin %d cont %f +- %f\n",bin,cont,err,nbinlo,bincont,binerr);
    if ((bincont < cont && bincont + comperr < cont) || (bincont > cont && bincont - comperr > cont)) {
      // printf("break\n");
      break;
    }
  }
  double result = (hist->GetBinCenter(nbinhi) - hist->GetBinCenter(nbinlo)) / 2;
  // printf("returning %f\n",result);
  return result;
}
double get_FWHM(TH1F* hist) {
  int bin1 = hist->FindFirstBinAbove(hist->GetMaximum() / 2);
  int bin2 = hist->FindLastBinAbove(hist->GetMaximum() / 2);
  double fwhm = hist->GetBinCenter(bin2) - hist->GetBinCenter(bin1);
  // printf("bin1 %d bin2 %d max %f max/2 %f fwhm %f\n", bin1,bin2,hist->GetMaximum(),hist->GetMaximum()/2,fwhm);
  return fwhm;
}
double get_FWHM_unc(TH1F* hist) {
  int bin1 = hist->FindFirstBinAbove(hist->GetMaximum() / 2);
  int bin2 = hist->FindLastBinAbove(hist->GetMaximum() / 2);
  int binm = hist->GetMaximumBin();
  double fwhm = hist->GetBinCenter(bin2) - hist->GetBinCenter(bin1);
  if (hist->GetEntries() < 1000) {
    return fwhm;
  }
  // printf("bin1 %d binm %d bin2 %d\n",bin1,binm,bin2);
  double fwhmerr = sqrt(pow(bin_positive_error(hist, bin1), 2) + pow(bin_positive_error(hist, binm), 2) +
                        pow(bin_positive_error(hist, bin2), 2));
  return fwhmerr;
}
void print_bins(TH1F* hist) {
  printf("Histo integral %f mean %f nbins %d\n", hist->Integral(), hist->GetMean(), hist->GetNbinsX());
  for (int mb = 1; mb <= hist->GetNbinsX(); mb++) {
    double bcont = hist->GetBinContent(mb);
    double berr = hist->GetBinError(mb);
    printf("bin %d: %f +- %f\n", mb, bcont, berr);
  }
}
TH2F* divide_hists(TH2F* h, TH2F* g) {
  auto* ratio2d = dynamic_cast<TH2F*>(h->Clone());
  for (int mbx = 1; mbx <= h->GetNbinsX(); mbx++) {
    for (int mby = 1; mby <= h->GetNbinsY(); mby++) {
      ratio2d->SetBinContent(mbx, mby, 1.);
      ratio2d->SetBinError(mbx, mby, 1.);
      double binc_h = h->GetBinContent(mbx, mby);
      double binc_g = g->GetBinContent(mbx, mby);
      double bine_h = h->GetBinError(mbx, mby);
      double bine_g = g->GetBinError(mbx, mby);
      // if (fabs(binc_h) > 3*bine_h && fabs(binc_g) > 3*bine_g){ //both contents significant
      // if (fabs(binc_h) > 3*bine_h && fabs(binc_g) > 3*bine_g && fabs(binc_h-binc_g) >
      // sqrt(pow(bine_h,2)+pow(bine_g,2))){ //both contents significant and difference significant
      // if (fabs(binc_h) > 1*bine_h && fabs(binc_g) > 1*bine_g && fabs(binc_h-binc_g) >
      // sqrt(pow(bine_h,2)+pow(bine_g,2))){ //both contents significant and difference significant
      if (fabs(binc_h) > 0 && fabs(binc_g) > 0) {
        ratio2d->SetBinContent(mbx, mby, binc_h / binc_g);
        double binerror = sqrt(pow(bine_h / binc_g, 2) + pow(bine_g * binc_h / pow(binc_g, 2), 2));
        ratio2d->SetBinError(mbx, mby, binerror);
      }
    }
  }
  ratio2d->GetZaxis()->SetTitle("Ratio");
  return ratio2d;
}
void set_close_bins_to_one(TH2F* hist) {
  // useful for ratio plots
  for (int mbx = 1; mbx <= hist->GetNbinsX(); mbx++) {
    for (int mby = 1; mby <= hist->GetNbinsY(); mby++) {
      double binc = hist->GetBinContent(mbx, mby);
      double bine = hist->GetBinError(mbx, mby);
      if (bine > fabs(binc - 1)) {
        hist->SetBinContent(mbx, mby, 1.);
        hist->SetBinError(mbx, mby, 0.);
      }
    }
  }
}
void set_max_of_hists(TH1* h, TH1* h1) {
  if (h == nullptr) {
    return;
  }
  // Add 10% to match behaviour of ROOT's automatic scaling
  double maxval = h1 != nullptr ? h1->GetMaximum() : -MAX_DOUBLE;
  h->SetMinimum(0.0);
  if (maxval > h->GetMaximum()) {
    h->SetMaximum(1.1 * maxval);
  }
}
void replace_contents(TH1* hOrig, TH1* hNew) {
  for (int i = 0; i < hOrig->GetNbinsX(); ++i) {
    hOrig->SetBinContent(i + 1, hNew->GetBinContent(i + 1));
    hOrig->SetBinError(i + 1, hNew->GetBinError(i + 1));
  }
}
void delete_obj_from_file(const std::string& fileName, const std::string& objectName) {
  auto* file = new TFile(fileName.c_str(), "update");
  gDirectory->Delete(objectName.c_str());
  file->Close();
}
TH2D* rotate_90_degrees(TH2D* h) {
  int dim = h->GetNbinsX();
  int dimy = h->GetNbinsY();
  if (dim != dimy) {
    printf("Rotation non implemented for non sqare histos! Returning input histo.\n");
    return h;
  }
  auto* hRot = dynamic_cast<TH2D*>(h->Clone());
  hRot->SetXTitle(h->GetXaxis()->GetTitle());
  hRot->SetYTitle(h->GetYaxis()->GetTitle());
  for (int i = 0; i < dim; ++i) {
    hRot->GetXaxis()->SetBinLabel(i + 1, h->GetYaxis()->GetBinLabel(i + 1));
    hRot->GetYaxis()->SetBinLabel(i + 1, h->GetXaxis()->GetBinLabel(i + 1));
    for (int j = 0; j < dim; ++j) {
      hRot->SetBinContent(j + 1, dim - i, h->GetBinContent(i + 1, j + 1));
    }
  }
  return hRot;
}
TH1F* randomize_hist(TH1F* h, int entries) {
  auto* hRan = dynamic_cast<TH1F*>(h->Clone());
  hRan->Reset();
  if (entries == -1) {
    entries = int(h->Integral());
  }
  hRan->FillRandom(h, entries);
  return hRan;
}
void draw_white_bin(int iBinX, int iBinY, TH2D* h) {
  double x_edge_low, x_edge_high, y_edge_low, y_edge_high;
  if (h == nullptr) {
    x_edge_low = double(iBinX);
    x_edge_high = double(iBinX + 1);
    y_edge_low = double(iBinY);
    y_edge_high = double(iBinY + 1);
  } else {
    x_edge_low = h->GetXaxis()->GetBinLowEdge(iBinX + 1);
    x_edge_high = h->GetXaxis()->GetBinLowEdge(iBinX + 2);
    y_edge_low = h->GetYaxis()->GetBinLowEdge(iBinY + 1);
    y_edge_high = h->GetYaxis()->GetBinLowEdge(iBinY + 2);
  }
  double x[4] = {x_edge_low, x_edge_high, x_edge_high, x_edge_low};
  double y[4] = {y_edge_low, y_edge_low, y_edge_high, y_edge_high};
  auto* pline = new TPolyLine(4, x, y);
  pline->SetFillColor(kWhite);
  pline->SetLineColor(kWhite);
  pline->Draw("f");
}
void FILL(TH1F* hist, double x, double w) {
  // Fill taking care of overflows and underflows
  // Those are reported in the first and last bin respectively
  double xMin = hist->GetXaxis()->GetXmin();
  double xMax = hist->GetXaxis()->GetXmax();
  double binwh = hist->GetBinWidth(1) / 2;
  if (x >= xMin && x <= xMax) {
    hist->Fill(x, w);
  }
  if (x < xMin) {
    hist->Fill(xMin + binwh, w);
  }
  if (x > xMax) {
    hist->Fill(xMax - binwh, w);
  }
}
TH1F* get_ratio(TH1F* hData, TH1F* hMC, int color, double min, double max, bool normalized, double corrSingleBins) {
  // TH1F *ratio = new TH1F ("ratio", "ratio", hData->GetNbinsX(), hData->GetXaxis()->GetXmin(),
  // hData->GetXaxis()->GetXmax() );
  auto* ratio = dynamic_cast<TH1F*>(hData->Clone("ratio"));
  ratio->Sumw2();

  if (normalized) {
    ratio->Divide(hData, hMC, 1 / hData->Integral(), 1 / hMC->Integral());
  } else {
    ratio->Divide(hData, hMC);
  }
  ratio->SetLabelSize(0.18, "X");
  ratio->SetLabelSize(0.16, "Y");
  ratio->SetTitleSize(0.18, "Y");
  ratio->SetTitleOffset(0.3, "Y");
  ratio->SetTitleSize(0.18, "X");
  ratio->SetTitleOffset(1.2, "X");
  // ratio->SetTitleOffset(0.5,"X");
  ratio->SetXTitle(hData->GetXaxis()->GetTitle());
  ratio->SetYTitle("data/MC");
  ratio->SetName(Form("%s/%s", hData->GetName(), hMC->GetName()));
  if (color != -1) {
    ratio->SetFillColor(color);
  } else {
    ratio->SetFillColor(hData->GetFillColor());
  }
  if (color != -1) {
    ratio->SetLineColor(color);
  } else {
    ratio->SetLineColor(hData->GetLineColor());
  }
  ratio->SetLineWidth(2);
  ratio->SetMarkerStyle(20);
  ratio->SetMarkerColor(color);
  ratio->GetYaxis()->SetNdivisions(5);
  if (max != -1) {
    ratio->SetMaximum(max);
  }
  if (min != -1) {
    ratio->SetMinimum(min);
  }

  if (corrSingleBins != 0.0) {
    for (int i = 0; i < hData->GetNbinsX(); ++i) {
      double n = hData->GetBinContent(i + 1) / (normalized ? hData->Integral() : 1);
      double ne = hData->GetBinError(i + 1) / (normalized ? hData->Integral() : 1);
      double N = hMC->GetBinContent(i + 1) / (normalized ? hMC->Integral() : 1);
      double Ne = hMC->GetBinError(i + 1) / (normalized ? hMC->Integral() : 1);
      double bine = unc_of_ratio(n, N, ne, Ne, corrSingleBins);
      ratio->SetBinError(i + 1, bine);
    }
  }

  int ndiv = TMath::Min(int(hData->GetXaxis()->GetNdivisions()), int(hMC->GetXaxis()->GetNdivisions()));
  ratio->GetXaxis()->SetNdivisions(ndiv);

  return ratio;
}
TGraphAsymmErrors* poissonize(TH1* h) {
  std::vector<int> points_to_remove;
  auto* gr = new TGraphAsymmErrors(h);
  for (unsigned int i = 0; i < fabs(double(gr->GetN())); i++) {
    double content = (gr->GetY())[i];
    gr->SetPointError(i, 0, 0, GC_down(content), GC_up(content));
    if (content == 0) {
      gr->RemovePoint(i);
      i--;
    }
  }
  return gr;
}
double GC_up(double hData) {
  // Used in WZ observation
  if (hData == 0) {
    return 0;
  }
  return 0.5 * TMath::ChisquareQuantile(1. - 0.1586555, 2. * (hData + 1)) - hData;
}
double GC_down(double hData) {
  // Used in WZ observation
  if (hData == 0) {
    return 0;
  }
  return hData - 0.5 * TMath::ChisquareQuantile(0.1586555, 2. * hData);
}
TGraphAsymmErrors* asymm_bin_unc_graph(TH1F* hOrig, TH1F* hVarUp, TH1F* hVarDw) {
  auto* gr = new TGraphAsymmErrors(hOrig);
  for (int mb = 0; mb < hOrig->GetNbinsX(); mb++) {
    double bcont_up = hVarUp->GetBinContent(mb + 1);
    double bcont_dw = hVarDw->GetBinContent(mb + 1);
    gr->SetPointError(mb, 0, 0, bcont_dw, bcont_up);
  }
  gr->SetLineColor(hOrig->GetLineColor());
  return gr;
}
void check_hist_compatibility(TH1F* hData, TH1F* hMC, double textXPos, bool displayChi2, float xMin, float xMax,
                              int nFuncPar, TH2D* hInvCov) {
  // Perform
  // 1) Kolmogorov test for histo compatibility,
  // 2) Chi2 test, assuming one of the histo to be error free: assume hMC has no error.
  // 3) run test
  bool performChisqtest = true;
  bool performRuntest = false;
  bool performKStest = false;
  if (!performRuntest && !performKStest && !performChisqtest) {
    return;
  }
  bool POISSONIZE = false;  // true=make use of asym err

  // make poisson errors for hData
  TGraphAsymmErrors* gr = nullptr;
  if (POISSONIZE) {
    gr = new TGraphAsymmErrors(hData);
  }

  int minbin = 1;
  int maxbin = hData->GetNbinsX();

  if (xMin != xMax) {
    std::cout << "Info: Histo compatibility tests with restricted range use full outer bins including xMax and xMin."
              << std::endl;
    minbin = hData->FindBin(xMin);
    maxbin = hData->FindBin(xMax);
  }

  // calculate chi2
  double chi2 = 0;
  int ndof = 0;
  double nbins = maxbin - minbin + 1;
  for (int idx = minbin; idx <= maxbin; idx++) {
    double b_s = hData->GetBinContent(idx);
    double be_s = hData->GetBinError(idx);
    double b_b = hMC->GetBinContent(idx);
    double be_b = hMC->GetBinError(idx);

    if (hInvCov == nullptr) {
      if (POISSONIZE) {
        // get the pos/ned error for the asym graph
        double content = (gr->GetY())[idx - 1];

        if (b_s >= b_b) {
          be_s = GC_down(content);
        } else {
          be_s = GC_up(content);
        }
      }

      if (b_s > 0 && b_b > 0) {
        double err = sqrt(be_s * be_s + be_b * be_b);
        chi2 += pow((b_s - b_b) / err, 2);
        ndof++;
      }
      // std::cout<<"Bin "<<idx<<" xval "<<hData->GetBinCenter(idx)<<", hData: "<<b_s<<" +- "<<be_s<<", exp: "<<b_b<<"
      // +- "<<be_b<<", chi2: "<<chi2<<std::endl;
    } else {
      for (int jdx = minbin; jdx <= maxbin; jdx++) {
        double jb_s = hData->GetBinContent(jdx);
        double jb_b = hMC->GetBinContent(jdx);
        double invcov = hInvCov->GetBinContent(idx, jdx);
        double f = (b_s - b_b) * invcov * (jb_s - jb_b);
        chi2 += f;
        // printf("Bin %d/%d: ( %f - %f ) * %e * ( %f - %f ) = %f, chi2 =
        // %f\n",idx,jdx,b_s,b_b,invcov,jb_s,jb_b,f,chi2);
      }
      ndof++;
    }
  }
  ndof -= nFuncPar;
  double prob = TMath::Prob(chi2, ndof);

  // run-test: from R.J. Barlow "Statistics" pp 153-154
  float nA = 0;  // n measurements above
  float nB = 0;  // n measurements below
  float Ntot = 0;
  int run = 0;
  double diff = 0;
  double exprun = 0;
  double varrun = 0;
  if (performRuntest) {
    if (hData->Integral() == 0 || hMC->Integral() == 0 || nbins < 3) {
      //      hData->Integral()==hMC->Integral()) { // likely this is the same histo
      std::cout << "skipping run-test" << std::endl;
      return;
    }
    for (int idx = minbin; idx <= maxbin; idx++) {
      double b_s = hData->GetBinContent(idx) / hData->Integral();
      double b_b = hMC->GetBinContent(idx) / hMC->Integral();

      if ((b_s <= 0) || (b_b <= 0)) {
        continue;
      }
      if (b_s >= b_b) {
        nA++;
      } else {
        nB++;
      }

      if (b_s - b_b != 0) {
        if ((b_s - b_b) / fabs(b_s - b_b) != diff) {
          run++;
          diff = (b_s - b_b) / fabs(b_s - b_b);
        }
      }
    }
    Ntot = nA + nB;
    exprun = 1 + (2 * nA * nB) / Ntot;
    varrun = (2 * nA * nB * (2 * nA * nB - Ntot)) / (Ntot * Ntot * (Ntot - 1));
  }

  char result[200];
  TPaveText* label;
  label = new TPaveText(textXPos, 0.96, 0.95, 0.98, "NDC");
  label->SetFillColor(kNone);

  if (performRuntest && performKStest && performChisqtest) {
    sprintf(result, "#chi2/dof = %1.2f/%d; Prob(#chi2|KS): (%1.2f | %1.2f); Run-test: %1.2f #sigma ", chi2, ndof, prob,
            hData->KolmogorovTest(hMC, ""), fabs(float(run - exprun) / varrun));
  } else if (performRuntest && performChisqtest) {
    sprintf(result, "#chi2/dof = %1.2f/%d; Prob(#chi2): (%1.2f); Run-test: %1.2f #sigma ", chi2, ndof, prob,
            fabs(float(run - exprun) / varrun));
  } else if (performKStest && performChisqtest) {
    sprintf(result, "#chi2/dof = %1.2f/%d; Prob(#chi2|KS): (%1.2f | %1.2f);", chi2, ndof, prob,
            hData->KolmogorovTest(hMC, ""));
  } else if (performKStest && performRuntest) {
    sprintf(result, "Prob(KS): %1.2f; Run-test: %1.2f #sigma ", hData->KolmogorovTest(hMC, ""),
            fabs(float(run - exprun) / varrun));
  } else if (performChisqtest) {
    sprintf(result, "#chi2/dof = %1.2f/%d; Prob(#chi2) = %1.2f;", chi2, ndof, prob);
  } else if (performKStest) {
    sprintf(result, "Prob(KS) = (%1.2f)", hData->KolmogorovTest(hMC, ""));
  } else if (performRuntest) {
    sprintf(result, "Run-test: %1.2f #sigma ", fabs(float(run - exprun) / varrun));
  } else {
    sprintf(result, "check_hist_compatibility: It's impossible to read this.");
  }
  // sprintf(result, "#chi2/dof = %1.1f/%d; Prob(#chi2|KS): (%1.2f | %1.2f); run-test: %1.2f #sigma ", chi2, ndof,
  // prob,hData->KolmogorovTest(hMC,"X"), fabs(float(run - exprun)/varrun ) );
  TText* t1 = label->AddText(result);
  t1->SetTextSize(0.033);
  t1->SetTextColor(kBlack);
  t1->SetTextAlign(11);
  if (displayChi2) {
    label->Draw("same");
  }

  std::cout << result << std::endl;

  delete gr;
}
void check_hist_compatibility(TH1F* hData, TF1* func, double textXPos, bool displayChi2, float xMin, float xMax) {
  auto* h_fit = dynamic_cast<TH1F*>(hData->Clone());
  auto* f_fit = dynamic_cast<TF1*>(func->Clone());
  h_fit->Reset();
  for (int ibin = 1; ibin < h_fit->GetNbinsX(); ibin++) {
    h_fit->SetBinContent(ibin, f_fit->Eval(h_fit->GetBinCenter(ibin)));
    h_fit->SetBinError(ibin, 0);
  }
  int nFuncPar = f_fit->GetNumberFreeParameters();
  check_hist_compatibility(hData, h_fit, textXPos, displayChi2, xMin, xMax, nFuncPar);
  delete h_fit;
  delete f_fit;
}
TH1F* project_to_y_axis(TH1F* h, bool noEmptyBins, double maxY) {
  TIter next(gDirectory->GetList());
  TH1F* h_del;
  TObject* obj;
  while ((obj = next()) != nullptr) {
    if (obj->InheritsFrom(TH1F::Class())) {
      h_del = dynamic_cast<TH1F*>(obj);
      if (std::string(h_del->GetName()).find("h_proj") < 2) {
        h_del->Delete();
      }
    }
  }
  int newnbins = 100;
  double newmax = h->GetBinContent(h->GetMaximumBin());
  if (maxY != 0.0) {
    newmax = maxY;
  }
  double newmin = newmax;
  for (int i = 0; i < h->GetNbinsX(); ++i) {
    double cont = h->GetBinContent(i + 1);
    if (cont != 0 && cont < newmin) {
      newmin = cont;
    }
  }
  auto* h_proj = new TH1F("h_proj", Form("Projection of %s;Bin content;Number of bins", h->GetName()), newnbins, newmin,
                          newmax + (newmax - newmin) * 0.01);
  for (int i = 0; i < h->GetNbinsX(); ++i) {
    double cont = h->GetBinContent(i + 1);
    if (noEmptyBins && cont == 0) {
      continue;
    }
    h_proj->Fill(cont);
  }
  return h_proj;
}
double* get_average_bin_content(TH1F* h) {
  // printf("Histogram %s has %.0f entries\n",h->GetName(),h->GetEntries() );
  bool noEmptyBins = true;
  auto* h_proj = project_to_y_axis(h, noEmptyBins);
  auto* value = new double[2];
  value[0] = h_proj->GetMean();
  value[1] = h_proj->GetMeanError();
  value[2] = h_proj->GetRMS();
  return value;
}
double graph_integral(TGraph* g) {
  double xMin = TMath::MinElement(g->GetN(), g->GetX());
  double xMax = TMath::MaxElement(g->GetN(), g->GetX());
  TF1 f1("f", [&](double* x, double*) { return g->Eval(x[0]); }, xMin, xMax, 0);

#ifdef _WIN32
  return f1.Integral(xMin, xMax, 0, 0, 1e-6);  // Root5
#else
  return f1.Integral(xMin, xMax, 1e-6);  // Root6
#endif
}
double get_graph_value(TGraph* g, int point) {
  return g->GetY()[point];
  ;
}
double* get_graph_point(TGraphErrors* g, int point) {
  auto* value = new double[2];
  value[0] = g->GetY()[point];
  value[1] = g->GetErrorY(point);
  return value;
}
double* get_graph_point(TGraphAsymmErrors* g, int num) {
  auto* value = new double[3];
  value[0] = g->GetY()[num];
  value[1] = g->GetErrorYlow(num);
  value[2] = g->GetErrorYhigh(num);
  return value;
}
void draw_TH2F(TH2F* h, const std::string& drawoption) {
  gPad->SetRightMargin(0.17);
  gPad->SetLeftMargin(0.15);
  gPad->SetBottomMargin(0.15);
  h->GetXaxis()->SetLabelOffset(0.01);
  h->GetXaxis()->SetTitleOffset(1);
  h->GetYaxis()->SetTitleOffset(1.1);
  h->GetXaxis()->SetLabelSize(0.06);
  h->GetYaxis()->SetLabelSize(0.06);
  h->GetXaxis()->SetTitleSize(0.06);
  h->GetYaxis()->SetTitleSize(0.06);
  h->GetZaxis()->SetTitleSize(0.06);
  h->Draw(drawoption.c_str());
}
void shift_palette_axis(TH2F* h, double xShift) {
  gPad->Update();
  auto* palette = dynamic_cast<TPaletteAxis*>(h->GetListOfFunctions()->FindObject("palette"));
  resize_palette_axis(h, palette->GetX1NDC() + xShift, palette->GetX2NDC() + xShift);
}
void resize_palette_axis(TH2F* h, double x0, double x1) {
  gPad->Update();
  auto* palette = dynamic_cast<TPaletteAxis*>(h->GetListOfFunctions()->FindObject("palette"));
  // the following lines moe the paletter. Choose the values you need for the position.
  palette->SetX1NDC(x0);
  palette->SetX2NDC(x1);
  palette->SetY1NDC(palette->GetY1NDC());
  palette->SetY2NDC(palette->GetY2NDC());
  gPad->Modified();
  gPad->Update();
}
void invert_polygon(TPolyLine* pline, double xCenter, double yCenter) {
  int n = pline->GetN();
  double* oldX = pline->GetX();
  double* oldY = pline->GetY();
  const int maxpolynumber = 10000;
  double newX[maxpolynumber];
  double newY[maxpolynumber];
  for (int i = 0; i < n; ++i) {
    TVector2 vec;
    vec.Set(-1 * (oldX[i] - xCenter), oldY[i] - yCenter);
    newX[i] = vec.X() + xCenter;
    newY[i] = vec.Y() + yCenter;
  }
  pline->SetPolyLine(n, newX, newY);
}
void rotate_polygon(TPolyLine* pline, double xCenter, double yCenter, double angle) {
  if (angle == 0.0) {
    return;
  }
  angle = angle / 360 * 2 * TMath::Pi();
  int n = pline->GetN();
  double* oldX = pline->GetX();
  double* oldY = pline->GetY();
  const int maxpolynumber = 10000;
  double newX[maxpolynumber];
  double newY[maxpolynumber];
  for (int i = 0; i < n; ++i) {
    TVector2 vec;
    vec.Set(oldX[i] - xCenter, oldY[i] - yCenter);
    TVector2 vecnew = vec.Rotate(angle);
    newX[i] = vecnew.X() + xCenter;
    newY[i] = vecnew.Y() + yCenter;
  }
  pline->SetPolyLine(n, newX, newY);
}
void enrich_polygon(TPolyLine* pline, int granularity) {
  int n = pline->GetN();
  double* oldXPointer = pline->GetX();
  double* oldYPointer = pline->GetY();
  const int maxpolynumber = 10000;
  double oldX[maxpolynumber];
  double oldY[maxpolynumber];
  for (int i = 0; i < n; ++i) {
    oldX[i] = oldXPointer[i];
    oldY[i] = oldYPointer[i];
  }
  int currentPointIdx = 0;
  for (int i = 0; i < n; ++i) {
    double thisX = oldX[i];
    double thisY = oldY[i];
    double nextX = oldX[positive_modulo(i + 1, n)];
    double nextY = oldY[positive_modulo(i + 1, n)];
    double xDiff = nextX - thisX;
    double yDiff = nextY - thisY;
    auto thisNDivisions = int(sqrt(pow(xDiff, 2) + pow(yDiff, 2)) * granularity);
    // printf("%d: %.2f / %.2f thisNDivisions %d\n", i, thisX, thisY, thisNDivisions);
    for (int j = 0; j < thisNDivisions; j++) {
      double newX = thisX + j * xDiff / thisNDivisions;
      double newY = thisY + j * yDiff / thisNDivisions;
      // printf("--new point %d: %.2f / %.2f\n", j, newX, newY);
      insert_polygon_point(pline, newX, newY, currentPointIdx);
      ++currentPointIdx;
    }
    ++currentPointIdx;
  }
  // printf("After enriching:\n");
  // for (size_t i = 0; i < n; i++) {
  //   printf(" %d: %.2f / %.2f\n",i,pline->GetX()[i],pline->GetY()[i] );
  // }
}
void bend_polygon(TPolyLine* pline, double strength, int granularity) {
  if (strength == 0) {
    return;
  }
  double radius = 1. / strength;
  enrich_polygon(pline, granularity);
  int n = pline->GetN();
  double* oldX = pline->GetX();
  double* oldY = pline->GetY();
  double yMin = *std::min_element(oldY, oldY + n);
  const int maxpolynumber = 10000;
  double* center = get_poly_center_of_gravity(pline, true);
  double xCenter = center[0];
  double yCenter = yMin - radius;
  double newX[maxpolynumber];
  double newY[maxpolynumber];
  for (int i = 0; i < n; ++i) {
    double xDiff = oldX[i] - xCenter;
    double yDiff = oldY[i] - yCenter;
    double angle = TMath::ATan(xDiff / yDiff);
    double newYDiff = yDiff * TMath::Cos(angle);
    newY[i] = yCenter + newYDiff;
    // double scaleFactor = newYDiff / yDiff;
    // printf("point %d: %.2f / %.2f scaleFactor %.2f\n", i, oldX[i], oldY[i], scaleFactor);
    newX[i] = oldX[i];
    // newX[i] = xCenter + xDiff * scaleFactor;
    // newX[i] = xCenter + xDiff;
  }
  pline->SetPolyLine(n, newX, newY);
}
void stretch_polygon(TPolyLine* pline, double xStretch, double yStretch) {
  if (xStretch == 1 && yStretch == 1) {
    return;
  }
  int n = pline->GetN();
  double* oldX = pline->GetX();
  double* oldY = pline->GetY();
  const int maxpolynumber = 10000;
  double* center = get_poly_center_of_gravity(pline, true);
  double xCenter = center[0];
  double yCenter = center[1];
  double newX[maxpolynumber];
  double newY[maxpolynumber];
  for (int i = 0; i < n; ++i) {
    newX[i] = xCenter + (oldX[i] - xCenter) * xStretch;
    newY[i] = yCenter + (oldY[i] - yCenter) * yStretch;
  }
  pline->SetPolyLine(n, newX, newY);
}
void shear_polygon(TPolyLine* pline, double xFactor, double yFactor) {
  if (xFactor == 0 && yFactor == 0) {
    return;
  }
  int n = pline->GetN();
  double* oldX = pline->GetX();
  double* oldY = pline->GetY();
  double xMax = *std::max_element(oldX, oldX + n);
  double xMin = *std::min_element(oldX, oldX + n);
  double yMax = *std::max_element(oldY, oldY + n);
  double yMin = *std::min_element(oldY, oldY + n);
  double halfXRange = (xMax - xMin) / 2;
  double halfYRange = (yMax - yMin) / 2;
  const int maxpolynumber = 10000;
  double* center = get_poly_center_of_gravity(pline, true);
  double xCenter = center[0];
  double yCenter = center[1];
  double newX[maxpolynumber];
  double newY[maxpolynumber];
  for (int i = 0; i < n; ++i) {
    double shearSizeX = xFactor * halfXRange * (oldY[i] - yCenter) / halfYRange;
    double shearSizeY = yFactor * halfYRange * (oldX[i] - xCenter) / halfXRange;
    newX[i] = oldX[i] + shearSizeX;
    newY[i] = oldY[i] + shearSizeY;
  }
  pline->SetPolyLine(n, newX, newY);
}
void crop_polygon(TPolyLine* pline, double xFrac, double yFrac) {
  if ((xFrac == 0.0) && (yFrac == 0.0)) {
    return;
  }
  int n = pline->GetN();
  double* oldX = pline->GetX();
  double* oldY = pline->GetY();
  double xMax = *std::max_element(oldX, oldX + n);
  double xMin = *std::min_element(oldX, oldX + n);
  double yMax = *std::max_element(oldY, oldY + n);
  double yMin = *std::min_element(oldY, oldY + n);
  double xCrop = xMax - (xMax - xMin) * xFrac;
  double yCrop = yMax - (yMax - yMin) * yFrac;
  // printf("Polygon: %.2f < x < %.2f, %.2f < y < %.2f crop at x = %.2f and y = %.2f\n", xMin, xMax, yMin, yMax, xCrop,
  // yCrop);
  crop_polygon_above(pline, xCrop, "X");
  crop_polygon_above(pline, yCrop, "Y");
}
void crop_polygon_above(TPolyLine* pline, double val, const std::string& axis) {
  // printf("Now crop %s!\n", axis.c_str());
  if (axis != "X" && axis != "Y") {
    printf("Warning: axis %s is not known. Abort crop polygon!\n", axis.c_str());
  }
  int iterCounter = 0;
  while (true) {
    // printf("Start another main loop\n" );
    int n = pline->GetN();
    double* oldX = pline->GetX();
    double* oldY = pline->GetY();
    int startIdx = -1;
    for (int i = 0; i < n; i++) {
      double compareValue = (axis == "X") ? oldX[i] : oldY[i];
      if (compareValue < val && startIdx == -1) {
        startIdx = i;
        break;
      }
    }
    if (startIdx == -1) {
      return;
    }

    double lastValidX = -1000000;
    double lastValidY = -1000000;
    bool replacedOne = false;
    int idx = startIdx;
    int counter = 0;
    while (!replacedOne) {
      if (counter == n) {
        break;
      }
      double thisX = oldX[idx];
      double thisY = oldY[idx];
      double nextX = oldX[positive_modulo(idx + 1, n)];
      double nextY = oldY[positive_modulo(idx + 1, n)];
      double nextNextX = oldX[positive_modulo(idx + 2, n)];
      double nextNextY = oldY[positive_modulo(idx + 2, n)];
      bool isDuplicatePointAhead = (thisX == nextX && thisY == nextY);
      double compareValue = (axis == "X") ? thisX : thisY;
      // printf("%d (%d): analyzing this point x/y = %.2f / %.2f, next x/y = %.2f / %.2f, next next x/y = %.2f /
      // %.2f\n", counter, idx, thisX, thisY, nextX, nextY, nextNextX, nextNextY);
      if (compareValue > val) {
        // printf("This point is bad: %.2f > %.2f\n", compareValue, val );
        // replace with point along connection to last valid point
        double newX;
        double newY;
        double dX = lastValidX - thisX;
        double dY = lastValidY - thisY;
        if (axis == "X") {
          newX = val;
          newY = lastValidY - dY * (lastValidX - val) / dX;
        } else {
          newX = lastValidX - dX * (lastValidY - val) / dY;
          newY = val;
          // printf("%.2f = %.2f - (%.2f - %.2f) / %.2f * %.2f\n",newX, lastValidX, lastValidY, val, dY, dX);
        }
        replace_polygon_point(pline, newX, newY, idx);
        lastValidX = newX;
        lastValidY = newY;
        // insert connection to next point if on valid side
        if (isDuplicatePointAhead) {
          nextX = nextNextX;
          nextY = nextNextY;
        }
        dX = nextX - thisX;
        dY = nextY - thisY;
        // printf("dX = %.2f = %.2f - %.2f, dY =  %.2f = %.2f - %.2f\n",dX,nextX, thisX, dY,nextY,thisY );
        if (axis == "X") {
          newX = val;
          newY = nextY - dY * (nextX - val) / dX;
          // printf("newY: %.2f = - %.2f + %.2f * (%.2f - %.2f) / %.2f\n", newY, nextY, dY, nextX, val, dX);
          if (nextX < val) {
            insert_polygon_point(pline, newX, newY, idx);
          }
        } else {
          newX = nextX - dX * (nextY - val) / dY;
          newY = val;
          // printf("newX: %.2f = - %.2f + %.2f * (%.2f - %.2f) / %.2f\n", newX, nextX, dX, nextY, val, dY);
          if (nextY < val) {
            insert_polygon_point(pline, newX, newY, idx);
          }
        }
        replacedOne = true;
      } else {
        lastValidX = thisX;
        lastValidY = thisY;
      }
      idx = positive_modulo(idx + 1, n);
      ++counter;
    }
    ++iterCounter;
    if (!replacedOne) {
      break;
    }
  }
  remove_duplicates(pline);
}
void remove_duplicates(TPolyLine* pline) {
  const int maxpolynumber = 10000;
  double newX[maxpolynumber];
  double newY[maxpolynumber];
  int newIdx = 0;
  int n = pline->GetN();
  for (int i = 0; i < n - 1; i++) {
    double thisX = pline->GetX()[i];
    double nextX = pline->GetX()[i + 1];
    double thisY = pline->GetY()[i];
    double nextY = pline->GetY()[i + 1];
    bool isIdentical = (thisX == nextX && thisY == nextY);
    if (!isIdentical) {
      newX[newIdx] = thisX;
      newY[newIdx] = thisY;
      ++newIdx;
    }
    // else printf("Warning: point %d (%.3f / %.3f) is a duplicate!\n", i, thisX, thisY);
  }
  newX[newIdx] = pline->GetX()[n - 1];
  newY[newIdx] = pline->GetY()[n - 1];
  ++newIdx;
  pline->SetPolyLine(newIdx, newX, newY);
}
int get_polygon_point(TPolyLine* pline, double x, double y) {
  int n = pline->GetN();
  double* oldX = pline->GetX();
  double* oldY = pline->GetY();
  for (int i = 0; i < n; ++i) {
    if (oldX[i] == x && oldY[i] == y) {
      return i;
    }
  }
  return -1;
}
void remove_polygon_point(TPolyLine* pline, int idx) {
  if (idx == -1) {
    idx = pline->GetN() - 1;
  }
  int n = pline->GetN();
  double* oldX = pline->GetX();
  double* oldY = pline->GetY();
  const int maxpolynumber = 10000;
  double newX[maxpolynumber];
  double newY[maxpolynumber];
  for (int i = 0; i < n; ++i) {
    newX[i] = oldX[i];
    newY[i] = oldY[i];
  }
  int newIdx = 0;
  for (int i = 0; i < n; ++i) {
    if (i == idx) {
      continue;
    }
    pline->SetPoint(newIdx, newX[i], newY[i]);
    ++newIdx;
  }
  pline->SetPolyLine(n - 1);
}
void replace_polygon_point(TPolyLine* pline, double x, double y, int idx) {
  // printf("replace_polygon_point: %.2f %.2f %d\n",x,y,idx );
  int n = pline->GetN();
  // for (size_t i = 0; i < n; i++) {
  //   printf("%d: %.2f / %.2f\n",i,pline->GetX()[i],pline->GetY()[i] );
  // }
  if (idx >= n) {
    printf("Warning: index %d >= %d number of points in poly. Abort replacing!\n", idx, n);
    return;
  }
  double* newx = pline->GetX();
  double* newy = pline->GetY();
  bool polyBackClosed =
      (newx[idx] == newx[positive_modulo(idx - 1, n)] && newy[idx] == newy[positive_modulo(idx - 1, n)]);
  bool polyFrontClosed =
      (newx[idx] == newx[positive_modulo(idx + 1, n)] && newy[idx] == newy[positive_modulo(idx + 1, n)]);
  bool polyClosed = polyBackClosed || polyFrontClosed;
  if (polyClosed) {
    // printf("Duplicate point %s, x/y: %2f / %.2f\n", polyBackClosed?"before":"after", x,y);
    pline->SetPoint(idx, x, y);
    int closedIdx = (polyBackClosed ? positive_modulo(idx - 1, n) : positive_modulo(idx + 1, n));
    pline->SetPoint(closedIdx, x, y);
  } else {
    pline->SetPoint(idx, x, y);
  }
  // TEllipse *pel = new TEllipse (x, y, 0.1, 0.2);
  // pel->SetFillColor(kRed);
  // pel->Draw();
  // printf("Done replacing! Now poly points:\n" );
  // for (size_t i = 0; i < pline->GetN(); i++) {
  //   printf("%d: %.2f / %.2f\n",i,pline->GetX()[i],pline->GetY()[i] );
  // }
}
void insert_polygon_point(TPolyLine* pline, double x, double y, int idx) {
  // printf("Insert polypoint: %.2f %.2f %d\n",x,y,idx );
  // printf("Before inserting poly points:\n" );
  // for (size_t i = 0; i < pline->GetN(); i++) {
  //   printf("%d: %.2f / %.2f\n",i,pline->GetX()[i],pline->GetY()[i] );
  // }
  int n = pline->GetN();
  if (idx > n) {
    printf("Warning: index %d > %d number of points in poly. Abort replacing!\n", idx, n);
    return;
  }

  const int maxpolynumber = 10000;
  double newx[maxpolynumber];
  double newy[maxpolynumber];
  for (int i = 0; i < n; ++i) {
    newx[i] = pline->GetX()[i];
    newy[i] = pline->GetY()[i];
  }
  bool polyFrontClosed =
      (newx[idx] == newx[positive_modulo(idx + 1, n)] && newy[idx] == newy[positive_modulo(idx + 1, n)]);
  if (polyFrontClosed) {
    insert_polygon_point(pline, x, y, positive_modulo(idx + 1, n));
    return;
  }

  pline->SetPoint(idx + 1, x, y);
  for (int i = idx + 2; i < n + 1; ++i) {
    // printf("Replacing %d with %.2f / %.2f\n",i, newx[i-1], newy[i-1] );
    pline->SetPoint(i, newx[i - 1], newy[i - 1]);
  }
  pline->SetPolyLine(n + 1);
  // TEllipse *pel = new TEllipse (x, y, 0.2, 0.1);
  // pel->SetFillColor(kOrange);
  // pel->Draw();
  // printf("Done inserting! Now poly points:\n" );
  // for (size_t i = 0; i < pline->GetN(); i++) {
  //   printf("%d: %.2f / %.2f\n",i,pline->GetX()[i],pline->GetY()[i] );
  // }
}
double* get_poly_center_of_gravity(TPolyLine* pline, bool dropLastPoint) {
  int n = pline->GetN();
  if (dropLastPoint) {
    n -= 1;
  }
  double* x = pline->GetX();
  double* y = pline->GetY();
  double x_center(0), y_center(0);
  for (int i = 0; i < n; ++i) {
    x_center += x[i] / n;
    y_center += y[i] / n;
  }
  auto* value = new double[2];
  value[0] = x_center;
  value[1] = y_center;
  return value;
}
double find_first_x_above(TSpline* spline, double yValue, double xMin, double xMax) {
  int niter = 2;
  int nsteps = 100;
  double iter_xMin = xMin;
  double iter_xMax = xMax;
  double prev_val = spline->Eval(xMin);
  double thisX = xMin;
  double prevx = xMin;
  bool foundit = false;
  double curr_val;
  for (int i_it = 0; i_it < niter; ++i_it) {
    for (int i_st = 0; i_st < nsteps; ++i_st) {
      thisX = iter_xMin + (iter_xMax - iter_xMin) / (nsteps - 1) * i_st;
      curr_val = spline->Eval(thisX);
      // printf("Iter %d step %d prevval %.2f currval %.2f %.2f <= %.2f --> %.2f <-- <
      // %.2f\n",i_it,i_st,prev_val,curr_val,iter_xMin,prevx,thisX ,iter_xMax);
      if ((yValue >= prev_val && yValue < curr_val) || (yValue <= prev_val && yValue > curr_val)) {
        iter_xMin = prevx;
        iter_xMax = thisX;
        foundit = true;
        break;
      }
      prev_val = curr_val;
      prevx = thisX;
    }
  }
  return foundit ? (prevx + thisX) / 2 : std::numeric_limits<double>::quiet_NaN();
}
bool check_hist_for_outliers(TH1F* h, int verbose, double sigmaThreshold) {
  bool outlier = false;
  for (int i = 1; i < h->GetNbinsX() - 1; ++i) {
    double prev_cont = h->GetBinContent(i);
    double this_cont = h->GetBinContent(i + 1);
    double this_err = h->GetBinError(i + 1);
    double next_cont = h->GetBinContent(i + 2);
    double aver_cont = (prev_cont + next_cont) / 2;
    double sigdiff = (this_cont - aver_cont) / this_err;
    double prevsigdiff = (this_cont - prev_cont) / this_err;
    double nextsigdiff = (this_cont - next_cont) / this_err;
    if (prev_cont == 0 || this_cont == 0 || next_cont == 0) {
    } else if (fabs(prevsigdiff) > sigmaThreshold && fabs(nextsigdiff) > sigmaThreshold) {
      if (verbose != 0) {
        printf("Warning: bin %d has %.1f (%.1f) > %.1f sigma difference to previous (next) bin!\n", i + 1, prevsigdiff,
               nextsigdiff, sigmaThreshold);
      }
      outlier = true;
    } else if (fabs(sigdiff) > sigmaThreshold) {
      if (verbose != 0) {
        printf("Warning: bin %d has %.1f > %.1f sigma difference to average of neighbour bins!\n", i + 1, sigdiff,
               sigmaThreshold);
      }
      // outlier=true;
    }
  }
  return outlier;
}
void draw_TPave(std::vector<std::string> lines, double x1, double y1, double x2, double y2, double textSize,
                const std::string& option, int color) {
  auto* pave = new TPaveText(x1, y1, x2, y2, option.c_str());
  pave->SetFillStyle(0);
  pave->SetFillColor(kNone);
  pave->SetLineColor(kNone);
  pave->SetTextSize(float(textSize));
  pave->SetTextColor(color);
  pave->SetTextAlign(12);
  for (auto& line : lines) {
    pave->AddText(line.c_str());
  }
  pave->Draw("same");
}
void draw_TPave(const std::string& line, double x1, double y1, double x2, double y2, double textSize,
                const std::string& option, int color) {
  std::vector<std::string> v;
  v.push_back(line);
  draw_TPave(v, x1, y1, x2, y2, textSize, option, color);
}
TPaveText* make_label(double x1, double y1, double x2, double y2, double size, double align) {
  auto* label = new TPaveText(x1, y1, x2, y2, "NDC");
  label->SetFillColor(kNone);
  label->SetLineColor(kNone);
  label->SetTextColor(kBlack);
  label->SetTextSize(float(size));
  label->SetTextAlign(Short_t(align));
  return label;
}
void make_log_bins(TH1* h, const std::string& axis) {
  TAxis* thisAxis = nullptr;
  if (axis == "x") {
    thisAxis = h->GetXaxis();
  } else if (axis == "y") {
    thisAxis = h->GetYaxis();
  } else if (axis == "z") {
    thisAxis = h->GetZaxis();
  } else {
    printf("Warning: axis '%s' not implemented!\n", axis.c_str());
    return;
  }
  int bins = thisAxis->GetNbins();
  double from = thisAxis->GetXmin();
  double to = thisAxis->GetXmax();
  double width = (to - from) / bins;
  const int MAX_N_BINS = 1000;
  if (bins > MAX_N_BINS) {
    printf("Error: increase MAX_N_BINS (%d < %d)\n", MAX_N_BINS, bins);
  }
  double new_bins[MAX_N_BINS];
  for (int i = 0; i <= bins; i++) {
    new_bins[i] = TMath::Power(10, from + i * width);
  }
  thisAxis->Set(bins, new_bins);
}
void draw_bin_grid(TH1* h, const std::string& axis, int lineStyle, int lineColor, int lineWidth) {
  TAxis* thisAxis;
  if (axis == "x") {
    thisAxis = h->GetXaxis();
  } else if (axis == "y") {
    thisAxis = h->GetYaxis();
  } else {
    printf("Warning: axis '%s' not implemented!\n", axis.c_str());
    return;
  }
  int bins = thisAxis->GetNbins();
  double min = (axis == "x") ? h->GetYaxis()->GetXmin() : h->GetXaxis()->GetXmin();
  double max = (axis == "x") ? h->GetYaxis()->GetXmax() : h->GetXaxis()->GetXmax();
  for (int i = 1; i < bins; i++) {
    double x = thisAxis->GetBinLowEdge(i + 1);
    TLine* resultLine;
    if (axis == "x") {
      resultLine = new TLine(x, min, x, max);
    } else {
      resultLine = new TLine(min, x, max, x);
    }
    resultLine->SetLineWidth(lineWidth);
    resultLine->SetLineStyle(lineStyle);
    resultLine->SetLineColor(lineColor);
    resultLine->Draw();
  }
}
double* get_pad_boundary(TPad* pad) {
  auto* par = new double[4];
  pad->GetPadPar(par[0], par[1], par[2], par[3]);
  return par;
}
std::string unique_ID(TH1* h) {
  std::string finalString = "unique_ID Histogram";
  finalString += " Name:";
  finalString += h->GetName();
  // replace special characters for testing
  std::replace(finalString.begin(), finalString.end(), '[', ' ');
  std::replace(finalString.begin(), finalString.end(), ']', ' ');
  std::replace(finalString.begin(), finalString.end(), '(', ' ');
  std::replace(finalString.begin(), finalString.end(), ')', ' ');
  finalString += " ContHash:";
  uint64_t binHash = 0;
  binHash = std::hash<double>{}(binHash + h->GetEntries());
#ifdef _WIN32
  int nBinsX = h->GetNbinsX();
  int nBinsY = h->GetNbinsY();
  int nBinsZ = h->GetNbinsZ();
  int maxBins = nBinsX + 2;
  if (nBinsY > 1) {
    maxBins = maxBins * (nBinsY + 2);
  }
  if (nBinsZ > 1) {
    maxBins = maxBins * (nBinsZ + 2);
  }
#else
  int maxBins = h->GetNcells();
#endif
  for (int i = 0; i < maxBins + 1; ++i) {
    double binc = h->GetBinContent(i);
    if (binc == 0) {
      continue;
    }
    uint64_t thisHash = modulo_hash_double(binc);
    binHash = modulo_merge_hash(thisHash, binHash);
    // printf("Bin %d content %f new hash %lu\n", i, binc, binHash);
  }
  finalString += Form("%lu", binHash);
  return finalString;
}

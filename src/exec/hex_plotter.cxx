#include "hex_plotter.h"

hex_plotter::hex_plotter(int argC, char **argV) {
  argc = argC;
  argv = argV;
  additionalLabel = "";
  invertX = false;
  invertVal = false;
  printHelp = false;
  printExamples = false;
  printVersion = false;
  printDefaults = false;
  noInfo = false;
  isIVInput = false;
  isCVInput = false;
  yesToAll = false;
  testInfo = false;
  showMaxValue = true;
  onlySpecial = false;
  noAxis = false;
  openDoxygen = false;
  detailedPlots = false;
  noLog = false;
  useLogVal = false;
  noTrafo = false;
  verbose = 0;
  overwritePadColors = -1;
  padNumberOption = 1;
  paletteNumber = 57;
  valuePrecision = 1;
  appearance = -1;
  executeExample = -1;
  inputFile = "";
  mapFile = "";
  outputFormat = "pdf";
  plotOption = "GEO";
  geoFile = "geo/hex_positions_HPK_128ch_6inch.txt";
  specialCellString = "";
  specialCellNameString = "";
  mergeFileList = "";
  zoom = "";
  zoomUnc = "";
  aspectRatio = "1125:750";
  setAspectRatio = "";
  fitFunc = "linlin";
  mlw = 10;
  setFitFunc = "";
  isAverageValues = false;
  setNoAverageValues = false;
  isRatioValues = false;
  yScale = 1;
  setYScale = 0;
  selector = AUTOPAD_SELECTOR;
  setSelector = AUTOPAD_SELECTOR;
  selectorName = "voltage:U:V";
  setSelectorName = "";
  inputFormat = "SELECTOR:PADNUM:VAL";
  setInputFormat = "";
  outputFile = "";
  setOutputFile = "";
  setOutputDir = "";
  infoString = "";
  valueName = "current:I:nA";
  setValueName = "";
  summaryFile = false;
  summaryFileName = "";
  specialCellOption = "dottedredbordersmallnumbers";
  setSpecialCellOption = "";
  textColor = 1;
  isInitialized = false;
  zoomMin = 0;
  zoomMax = 0;
  zoomUncMin = 0;
  zoomUncMax = 0;
  valueOption = "";
  isInterPadPlot = false;
  exampleHandler = new example_handler(argv[0]);
  helpHandler = new help_handler();
}
int hex_plotter::parse_input() {
  std::vector<std::string> argv_vec(argv, argv + argc);
  return parse_input(argv_vec);
}
int hex_plotter::parse_input(std::vector<std::string> argv_vec) {
  for (size_t i = 1; i < argv_vec.size(); i += 2) {
    // clang-format off
    if (match(argv_vec[i], "--ap")) {appearance = int(std::atof(std::string(argv_vec[i + 1]).c_str()));}
    else if (match(argv_vec[i], "--aspectratio")) {setAspectRatio = argv_vec[i + 1];}
    else if (match(argv_vec[i], "--addLabel")) {additionalLabel = argv_vec[i + 1];}
    else if (match(argv_vec[i], "--CV")) {isCVInput = true; isIVInput = false; --i; continue;}
    else if (match(argv_vec[i], "--defaults")) {printDefaults = true; --i; continue;}
    else if (match(argv_vec[i], "--detailed")) {detailedPlots = true; --i; continue;}
    else if (match(argv_vec[i], "--nolog")) {noLog = true; --i; continue;}
    else if (match(argv_vec[i], "--useLogVal")) {useLogVal = true; --i; continue;}
    else if (match(argv_vec[i], "--doxygen")) {openDoxygen = true; --i; continue;}
    else if (match(argv_vec[i], "--example")) {executeExample = int(std::atof(std::string(argv_vec[i + 1]).c_str()));}
    else if (match(argv_vec[i], "--examples")) {printExamples = true; --i; continue;}
    else if (match(argv_vec[i], "--version")) {printVersion = true; --i; continue;}
    else if (match(argv_vec[i], "--fitfunc")){setFitFunc = argv_vec[i + 1];}
    else if (match(argv_vec[i], "--mlw")) { mlw = std::atoi(std::string(argv_vec[i + 1]).c_str());}
    else if (match(argv_vec[i], "--help")) {printHelp = true; --i; continue;}
    else if (match(argv_vec[i], "--if")){setInputFormat = argv_vec[i + 1];}
    else if (match(argv_vec[i], "--info")) {infoString = argv_vec[i + 1];}
    else if (match(argv_vec[i], "--invertx")) {invertX = true; --i; continue;}
    else if (match(argv_vec[i], "--invertVal")) {invertVal = true; --i; continue;}
    else if (match(argv_vec[i], "--IV")) {isIVInput = true; isCVInput = false; --i; continue;}
    else if (match(argv_vec[i], "--mf")){mergeFileList = argv_vec[i + 1];}
    else if (match(argv_vec[i], "--nd")){valuePrecision = int(std::atof(std::string(argv_vec[i + 1]).c_str()));}
    else if (match(argv_vec[i], "--noav")) {setNoAverageValues = true; --i; continue;}
    else if (match(argv_vec[i], "--ratio")) {isRatioValues = true; --i; continue;}
    else if (match(argv_vec[i], "--noaxis")) {noAxis = true; --i; continue;}
    else if (match(argv_vec[i], "--noinfo")) {noInfo = true; --i; continue;}
    else if (match(argv_vec[i], "--notrafo")) {noTrafo = true; --i; continue;}
    else if (match(argv_vec[i], "--od")){setOutputDir = argv_vec[i + 1];}
    else if (match(argv_vec[i], "--of")){outputFormat = argv_vec[i + 1];}
    else if (match(argv_vec[i], "--padcolor")){overwritePadColors = int(std::atof(std::string(argv_vec[i + 1]).c_str()));}
    else if (match(argv_vec[i], "--pn")){padNumberOption = int(std::atof(std::string(argv_vec[i + 1]).c_str()));}
    else if (match(argv_vec[i], "--colorpalette")){paletteNumber = int(std::atof(std::string(argv_vec[i + 1]).c_str()));}
    else if (match(argv_vec[i], "--textcolor")){textColor = int(std::atof(std::string(argv_vec[i + 1]).c_str()));}
    else if (match(argv_vec[i], "--sc")){specialCellString = argv_vec[i + 1];}
    else if (match(argv_vec[i], "--scn")){specialCellNameString = argv_vec[i + 1];}
    else if (match(argv_vec[i], "--sco")){setSpecialCellOption = argv_vec[i + 1];}
    else if (match(argv_vec[i], "--select")){setSelector = std::atof(std::string(argv_vec[i + 1]).c_str());}
    else if (match(argv_vec[i], "--sf")) {summaryFile = true; --i; continue;}
    else if (match(argv_vec[i], "--showmaxval")) {showMaxValue = true; --i; continue;}
    else if (match(argv_vec[i], "--onlyspecial")) {onlySpecial = true; --i; continue;}
    else if (match(argv_vec[i], "--sn")) {setSelectorName = argv_vec[i + 1];}
    else if (match(argv_vec[i], "--testinfo")) {testInfo = true; --i; continue;}
    else if (match(argv_vec[i], "--vn")) {setValueName = argv_vec[i + 1];}
    else if (match(argv_vec[i], "--volt")){setSelector = std::atof(std::string(argv_vec[i + 1]).c_str());}
    else if (match(argv_vec[i], "--yestoall")) {yesToAll = true; --i; continue;}
    else if (match(argv_vec[i], "--ys")) {setYScale = std::atof(std::string(argv_vec[i + 1]).c_str());}
    else if (match(argv_vec[i], "--zu")) {zoomUnc = argv_vec[i + 1];}
    else if (match(argv_vec[i], "-\\?")) {printHelp = true; --i; continue;}
    else if (match(argv_vec[i], "-g")) {geoFile = argv_vec[i + 1];}
    else if (match(argv_vec[i], "-h")) {printHelp = true; --i; continue;}
    else if (match(argv_vec[i], "-i")) {inputFile = argv_vec[i + 1];}
    else if (match(argv_vec[i], "-m")) {mapFile = argv_vec[i + 1];}
    else if (match(argv_vec[i], "-o")) {setOutputFile = argv_vec[i + 1];}
    else if (match(argv_vec[i], "-p")) {plotOption = argv_vec[i + 1];}
    else if (match(argv_vec[i], "-v")) {verbose = int(std::atof(std::string(argv_vec[i + 1]).c_str()));}
    else if (match(argv_vec[i], "-z")) {zoom = argv_vec[i + 1];}
    else {
      ERROR_MSG("invalid argument:");
      for (int j = 0; j < argv_vec.size(); j++) {
        if (j == i) { switch_color("BOLDRED"); }
        std::cout << argv_vec[j] << " ";
        if (j == i) { switch_color("RESET"); }
      }
      std::cout << std::endl;
      STANDARD_MSG("Consult help for a list of valid arguments!");
      return 1;
    }
    // clang-format on
  }
  if (argv_vec.size() == 1) {
    printHelp = true;
  }
  if (execute_example() != 0) {
    return 1;
  }
  return 0;
}
int hex_plotter::print_help() {
  if (printHelp) {
    helpHandler->print_help();
    return 1;
  }
  return 0;
}
int hex_plotter::print_version() {
  if (printVersion) {
    STANDARD_MSG("HexPlot version %s", HEXPLOT_VERSION);
    return 1;
  }
  return 0;
}
int hex_plotter::print_examples() {
  if (printExamples) {
    exampleHandler->print_examples();
    return 1;
  }
  return 0;
}
int hex_plotter::print_defaults() {
  init();
  if (printDefaults) {
    return 1;
  }
  return 0;
}
void hex_plotter::print_parameters() {
  if ((verbose != 0) || printDefaults) {
    printf("Arguments set to:\n");
    printf("--ap               appearance                   %d\n", appearance);
    printf("--aspectratio      plot aspect ratio            %s\n", aspectRatio.c_str());
    printf("--addLabel         label on the top right corner%s\n", additionalLabel.c_str());
    printf("--CV               use CV inputFormat           %d\n", static_cast<int>(isCVInput));
    printf("--detailed         print detailed plots         %d\n", static_cast<int>(detailedPlots));
    printf("--nolog            use linear scale             %d\n", static_cast<int>(noLog));
    printf("--useLogVal        set logaritmic VAL axis      %d\n", static_cast<int>(useLogVal));
    printf("--fitfunc          fit function                 %s\n", fitFunc.c_str());
    printf("--mlw              minimum local window         %d\n", mlw);
    printf("--if               inputFormat                  %s\n", inputFormat.c_str());
    printf("--IV               use IV inputFormat           %d\n", static_cast<int>(isIVInput));
    printf("--mf               mergeFileList                %s\n", mergeFileList.c_str());
    printf("--nd               number of digits             %d\n", valuePrecision);
    printf("--noav             take value averages          %d\n", static_cast<int>(isAverageValues));
    printf("--ratio            take ratio values            %d\n", static_cast<int>(isRatioValues));
    printf("--noaxis           no axis                      %d\n", static_cast<int>(noAxis));
    printf("--noinfo           noInfo                       %d\n", static_cast<int>(noInfo));
    printf("--notrafo          no transformation for fit    %d\n", static_cast<int>(noTrafo));
    printf("--onlyspecial      onlySpecial                  %d\n", static_cast<int>(onlySpecial));
    printf("--od               outputDir                    %s\n", OUTPUT_DIR.c_str());
    printf("--of               outputFormat                 %s\n", outputFormat.c_str());
    printf("--padcolor         pad color                    %d\n", overwritePadColors);
    printf("--pn               pad number option            %d\n", padNumberOption);
    printf("--colorpalette     color palette number         %d\n", paletteNumber);
    printf("--sc               special cells                %s\n", specialCellString.c_str());
    printf("--scn              special cell names           %s\n", specialCellNameString.c_str());
    printf("--sco              special cell option          %s\n", specialCellOption.c_str());
    printf("--select           selector value               %.2e\n", selector);
    printf("--sf               print summary file?          %d\n", static_cast<int>(summaryFile));
    printf("--showmaxval       showMaxValue                 %d\n", static_cast<int>(showMaxValue));
    printf("--sn               selector name                %s\n", selectorName.c_str());
    printf("--textcolor        text color                   %d\n", textColor);
    printf("--vn               value name                   %s\n", valueName.c_str());
    printf("--yestoall         yestoall                     %d\n", static_cast<int>(yesToAll));
    printf("--ys               yScale                       %.2e\n", yScale);
    printf("--zu               zoom uncertainty             %s\n", zoomUnc.c_str());
    printf("-g                 geometry file                %s\n", geoFile.c_str());
    printf("-i                 inputFile                    %s\n", inputFile.c_str());
    printf("-m                 mapFile                      %s\n", mapFile.c_str());
    printf("-o                 outputFile                   %s\n", outputFile.c_str());
    printf("-p                 plot option                  %s\n", plotOption.c_str());
    printf("-v                 verbosity                    %d\n", verbose);
    printf("-z                 zoom                         %s\n", zoom.c_str());
  }
}
int hex_plotter::execute_example() {
  if (executeExample == -1) {
    return 0;
  }
  std::string exampleModifier;
  if (executeExample >= exampleHandler->get_n_examples()) {
    ERROR_MSG("example number %d is not implemented!", executeExample);
    return 1;
  }
  bool foundExample = false;
  for (int i = 1; i < argc; i++) {
    if (foundExample) {
      exampleModifier += " " + std::string(argv[i]);
    }
    if (match(argv[i], "--example")) {
      ++i;
      foundExample = true;
    }
  }
  std::string exampleDescr = exampleHandler->get_example_description(executeExample);
  switch_color("BOLDBLACK");
  printf("(%d) ", executeExample);
  switch_color("RESET");
  switch_color("BOLDBLUE");
  printf("%s\n", exampleDescr.c_str());
  switch_color("RESET");
  if (!exampleModifier.empty()) {
    printf("Additional flags:");
    switch_color("BOLDBLACK");
    printf("%s\n", exampleModifier.c_str());
    switch_color("RESET");
  }
  std::string executeString = exampleHandler->get_example(executeExample) + exampleModifier;
  printf("%s\n", executeString.c_str());
  executeExample = -1;
  parse_input(get_argv(executeString));
  return 0;
}
int hex_plotter::show_doxygen() {
  if (openDoxygen) {
    std::string addRelativePath;
    if (contains(get_current_dir(), "bin")) {
      addRelativePath = "../";
    }
    std::string finalPath = addRelativePath + "doc/html/index.html";
    std::string command;
    if (IS_WINDOWS_COMPILATION) {
      command = "start /max " + finalPath;
    } else {
      command = "xdg-open " + finalPath;
    }
    printf("Executing '%s'...\n", command.c_str());
    if (!is_file(finalPath)) {
      ERROR_MSG("doxygen file not found. Did you forget to produce it with 'make doc'?");
    } else {
      system(command.c_str());  // NOLINT
    }
    return 1;
  }
  return 0;
}
void hex_plotter::init() {
  if (isInitialized) {
    return;
  }

  INFO_MSG("++++ Initialize HexPlot ++++");

  if (invertX && setSelector != AUTOPAD_SELECTOR) {
    setSelector *= -1;
  }

  mapFile = remove_string_all(mapFile, "'");
  inputFile = remove_string_all(inputFile, "'");
  mergeFileList = remove_string_all(mergeFileList, "'");

  inputFiles = split(inputFile, ',');
  if (!setNoAverageValues && inputFiles.size() > 1) {
    isAverageValues = true;
  }
  if (isRatioValues && inputFiles.size() != 2) {
    isRatioValues = false;
    ERROR_MSG("Impossible to plot the ratio among more (or less) than two files");
    return;
  }
  if (isRatioValues && inputFiles.size() == 2) {
    isAverageValues = false;
  }
  if (contains(plotOption, "VAL")) {
    WARNING_MSG(
        "plot option suffix 'VAL' is deprecated. Use arguments '--fitfunc findvalue:%.2f --notrafo' "
        "instead.",
        setSelector);
    plotOption = remove_string(plotOption, "VAL");
    valueOption = "DEP";
    setFitFunc = "findvalue:" + double_to_string(setSelector);
    noTrafo = true;
  }
  if (contains(plotOption, "DEP") || (!setFitFunc.empty())) {
    valueOption = "DEP";
  }
  plotOption = remove_string(plotOption, valueOption);

  if (contains(plotOption, "DEP")) {
    WARNING_MSG("plot option 'DEP' is deprecated. Use '--fitfunc [your conscious choice]' instead!");
  }
  if (plotOption == "HEX") {
    WARNING_MSG("plot option 'HEX' is deprecated. Use 'GEO' instead!");
    plotOption = "GEO";
  }
  if (plotOption == "CHAN") {
    WARNING_MSG("plot option 'CHAN' is deprecated. Use 'FLAT' instead!");
    plotOption = "FLAT";
  }
  if (plotOption == "SEL") {
    WARNING_MSG("plot option 'SEL' is deprecated. Use 'PAD' instead!");
    plotOption = "PAD";
  }

  if (valueOption == "DEP" || valueOption == "VAL") {
    detailedPlots = true;
  }

  if (!setOutputFile.empty()) {
    OUTPUT_DIR = get_file_dir(setOutputFile);
  }
  if (!setOutputDir.empty()) {
    OUTPUT_DIR = setOutputDir;
  }
  if (!setFitFunc.empty()) {
    fitFunc = setFitFunc;
  }

  std::string outputName = get_file_name_no_ext(geoFile);
  if (isIVInput) {
    if (isAverageValues) {
      baseValueName = "current:<I>:nA";
    } else {
      baseValueName = "current:I:nA";
    }
    valueName = baseValueName;
    outputName = isAverageValues ? "sensors_allvalues_IV" : get_file_name_no_ext(inputFile);
    showMaxValue = true;
  } else if (isCVInput) {
    if (isAverageValues) {
      baseValueName = "capacity:<C>:pF";
    } else {
      baseValueName = "capacity:C:pF";
    }
    if (valueOption == "DEP") {
      if (isAverageValues) {
        valueName = "depletion voltage:<U_{dep}>:V";
      } else {
        valueName = "depletion voltage:U_{dep}:V";
      }
    } else {
      valueName = baseValueName;
    }
    outputName = isAverageValues ? "sensors_allvalues_CV" : get_file_name_no_ext(inputFile);
    showMaxValue = false;
  } else {
    baseValueName = valueName;
    if (!inputFile.empty()) {
      outputName = get_file_name_no_ext(inputFile);
    }
    outputFile = OUTPUT_DIR + (OUTPUT_DIR.empty() ? "" : "/") + outputName + "_" + plotOption +
                 std::string(!specialCellString.empty() ? "_SCELL" : "") + "." + outputFormat;
  }
  if (isCVInput || isIVInput) {
    if (valueOption == "VAL") {
      if (isAverageValues) {
        valueName = "voltage:<U>:V";
      } else {
        valueName = "Voltage:U:V";
      }
      baseValueName = valueName;
      if (isIVInput) {
        selectorName = "current:I:nA";
      } else if (isCVInput) {
        selectorName = "capacity:C:pF";
      }
    }
    outputFile = OUTPUT_DIR + (OUTPUT_DIR.empty() ? "" : "/") + outputName + "_" + plotOption +
                 std::string(!valueOption.empty() ? "_" : "") + valueOption +
                 std::string(!specialCellString.empty() ? "_SCELL" : "") + "." + outputFormat;
  }

  if (setYScale != 0) {
    yScale = setYScale;
  }
  if (invertVal) {
    yScale *= -1;
  }
  if (setSelector != AUTOPAD_SELECTOR) {
    selector = setSelector;
  }
  if (!setInputFormat.empty()) {
    inputFormat = setInputFormat;
  }
  if (!setValueName.empty()) {
    valueName = setValueName;
    baseValueName = valueName;
  }
  if (!setSelectorName.empty()) {
    selectorName = setSelectorName;
  }
  if (!setOutputFile.empty()) {
    outputFile = setOutputFile;
  }
  if (summaryFile) {
    summaryFileName = (get_file_dir(outputFile).empty() ? "" : (get_file_dir(outputFile) + "/")) +
                      get_file_name_no_ext(outputFile) + "_summary.txt";
  }
  if (!setSpecialCellOption.empty()) {
    specialCellOption = setSpecialCellOption;
  }

  isInterPadPlot = (inputFormat.find("INTER") < inputFormat.size());

  mergeFiles = split(mergeFileList, ',');
  selectorLabels = split(selectorName, ':');
  if (invertX) {
    selectorLabels.at(2) = selectorLabels.at(2) + "*(-1)";
  }
  valueLabels = split(valueName, ':');
  if (invertVal) {
    valueLabels.at(2) = valueLabels.at(2) + "*(-1)";
  }
  baseValueLabels = split(baseValueName, ':');
  specialCellNames = split(specialCellNameString, ':');
  specialCellCollection = split(specialCellString, ':');
  for (const auto &i : specialCellCollection) {
    std::vector<std::string> tmpSpecialCell = split(i, ',');
    std::vector<std::string> tmp_cells;
    for (const auto &range : tmpSpecialCell) {
      specialCellCollectionRange = split(range, '-');
      if (specialCellCollectionRange.size() == 1) {
        tmp_cells.push_back(range);
        continue;
      }
      if (specialCellCollectionRange.size() != 2) {
        ERROR_MSG("Cell range not valid.");
      }
      auto min = int(std::atof(specialCellCollectionRange.at(0).c_str()));
      auto max = int(std::atof(specialCellCollectionRange.at(1).c_str()));
      if (min > max) ERROR_MSG("Cell range not in ascending order.");

      for (int cellNum = min; cellNum <= max; cellNum++) {
        tmp_cells.push_back(std::to_string(cellNum));
      }
    }
    specialCells.push_back(tmp_cells);
  }
  for (auto &specialCell : specialCells) {
    std::vector<int> tmpSpecialCellNumbers;
    for (auto &j : specialCell) {
      DEBUG_MSG("Special cell #%s\n", j.c_str());
      auto thisnumber = int(std::atof(j.c_str()));
      tmpSpecialCellNumbers.push_back(thisnumber);
    }
    specialCellNumbers.push_back(tmpSpecialCellNumbers);
  }
  std::vector<std::string> zoom_tmp = split(zoom, ':');
  zoomMin = 0;
  zoomMax = 0;
  if (!zoom.empty()) {
    if (zoom_tmp.size() != 2) {
      ERROR_MSG(
          "zoom parameter should be of type double-double, e.g. 30.5:50.5! You provided %s with %d "
          "parameters!",
          zoom.c_str(), int(zoom_tmp.size()));
    }
    zoomMin = std::atof(zoom_tmp.at(0).c_str());
    zoomMax = std::atof(zoom_tmp.at(1).c_str());
  }
  std::vector<std::string> zoomUnc_tmp = split(zoomUnc, ':');
  zoomUncMin = 0;
  zoomUncMax = 0;
  if (!zoomUnc.empty()) {
    if (zoomUnc_tmp.size() != 2) {
      ERROR_MSG(
          "zoomUnc parameter should be of type double-double, e.g. 30.5:50.5! You provided %s with %d "
          "parameters!",
          zoomUnc.c_str(), int(zoomUnc_tmp.size()));
    }
    zoomUncMin = std::atof(zoomUnc_tmp.at(0).c_str());
    zoomUncMax = std::atof(zoomUnc_tmp.at(1).c_str());
  }

  if (!setAspectRatio.empty()) {
    aspectRatio = setAspectRatio;
  }
  std::vector<std::string> aspectRatio_tmp = split(aspectRatio, ':');
  aspectRatioX = std::atof(aspectRatio_tmp.at(0).c_str());
  aspectRatioY = std::atof(aspectRatio_tmp.at(1).c_str());

  if (!infoString.empty()) {
    infoString = replace_string_all(infoString, "~", " ");
    std::vector<std::string> info_tmp = split(infoString, ':');
    for (size_t i = 0; i < size_t(info_tmp.size() / 2); ++i) {
      infoMap[info_tmp.at(2 * i)] = info_tmp.at(2 * i + 1);
    }
  }

  print_parameters();
  isInitialized = true;
}
int hex_plotter::check_input_parameters() {
  std::vector<int> missingFiles;
  for (size_t i = 0; i < inputFiles.size(); i++) {
    if (!is_file(inputFiles.at(i))) {
      missingFiles.push_back(i);
    }
  }
  if (!missingFiles.empty()) {
    ERROR_MSG("the following input files do not exist:");
    for (size_t i = 0; i < missingFiles.size(); i++) {
      printf("%s\n", inputFiles.at(i).c_str());
    }
    return 1;
  }
  return 0;
}
int hex_plotter::run() {
  init();
  if (plotOption != "MERGE" && (check_input_parameters() != 0)) {
    return 1;
  }
  INFO_MSG("++++ Execute HexPlot ++++");

  gStyle->SetOptStat(0);

  geoPlot = new geo_plot();
  geoPlot->set_verbose(verbose);
  geoPlot->set_geo_file(geoFile);
  geoPlot->set_test_info(testInfo);
  geoPlot->set_color_palette(paletteNumber);
  geoPlot->set_text_color(textColor);
  geoPlot->set_use_logy(useLogVal);

  hexValues = new hex_values();
  hexValues->invert_selector(invertX);
  hexValues->set_no_trafo(noTrafo);
  hexValues->set_average(isAverageValues);
  hexValues->set_ratio(isRatioValues);
  hexValues->set_value_option(valueOption);
  hexValues->set_input_file(inputFile);
  hexValues->set_input_format(inputFormat);
  hexValues->set_output_file(outputFile);
  hexValues->set_output_format(outputFormat);
  hexValues->set_map_file(mapFile);
  hexValues->set_y_scale(yScale);
  hexValues->set_range(zoomMin, zoomMax);
  hexValues->set_test_info(testInfo);
  hexValues->set_verbose(verbose);
  hexValues->set_selector(selector);
  hexValues->set_appearance(appearance);
  hexValues->set_fit_func(fitFunc);
  hexValues->set_fit_mlw(mlw);
  hexValues->set_detailed_plots(detailedPlots, !noLog);
  hexValues->set_single_pad(plotOption == "PAD");
  hexValues->set_value_name(valueLabels.at(1), valueLabels.at(2));
  hexValues->set_selector_name(selectorLabels.at(1), selectorLabels.at(2));

  if (plotOption != "GEO") {
    flatPlot = new flat_plot();
    flatPlot->set_value_labels(valueLabels);
    flatPlot->set_selector_labels(selectorLabels);
    flatPlot->set_verbose(verbose);
    flatPlot->set_only_special(onlySpecial);
    flatPlot->set_test_info(testInfo);
    flatPlot->set_aspect_ratio(aspectRatioX, aspectRatioY);
    flatPlot->set_range(zoomMin, zoomMax);
  }

  if (plotOption == "GEO") {
    plot_geo();
  } else if (plotOption == "FLAT") {
    plot_flat();
  } else if (plotOption == "PAD") {
    plot_pad();
  } else if (plotOption == "FULL") {
    plot_full();
  } else if (plotOption == "MERGE") {
    merge();
  } else {
    WARNING_MSG("Pot option '%s' not implemented. Aborting.", plotOption.c_str());
  }

  INFO_MSG("+++++ End HexPlot +++++");
  return 0;
}
void hex_plotter::set_info_text() {
  for (auto &it : infoMap) {
    if (it.first == "tr") {
      geoPlot->set_top_right_info(it.second);
    }
    if (it.first == "lr") {
      geoPlot->set_low_right_info(it.second);
    }
    if (it.first == "tl") {
      geoPlot->set_top_left_info(it.second);
    }
    if (it.first == "ll") {
      geoPlot->set_low_left_info(it.second);
    }
  }
}
void hex_plotter::plot_geo() {
  if (!inputFile.empty()) {
    TH1F **h_full = hexValues->load_data_histo_1d_interpad();
    TH1F *h_fill = h_full[0];
    if (h_fill == nullptr) {
      ERROR_MSG("Value histogram is not filled! Abort!");
      return;
    }
    geoPlot->fill(h_fill);
    if (isInterPadPlot) {
      geoPlot->fill_inter_cell(h_full[1]);
      geoPlot->fill_inter_cell(h_full[2]);
      geoPlot->fill_inter_cell(h_full[3]);
      geoPlot->fill_inter_cell(h_full[4]);
      geoPlot->fill_inter_cell(h_full[5]);
      geoPlot->fill_inter_cell(h_full[6]);
      geoPlot->fill_inter_cell(h_full[7]);
      geoPlot->fill_inter_cell(h_full[8]);
      geoPlot->fill_inter_cell(h_full[9]);
      geoPlot->fill_inter_cell(h_full[10]);
    }
    geoPlot->set_zero_suppress(true);
    if (overwritePadColors != -1) {
      geoPlot->force_pad_colors(overwritePadColors);
    }
    geoPlot->set_z_title(Form("%s [%s]", valueLabels.at(1).c_str(), valueLabels.at(2).c_str()));
    if (isRatioValues) {
      geoPlot->set_z_title("R");
    }
    if (!zoom.empty()) {
      geoPlot->set_z_range(zoomMin, zoomMax);
    }
    geoPlot->set_no_axis(noAxis);
    geoPlot->set_pad_value_precision(valuePrecision);
    geoPlot->set_pad_number_option(padNumberOption);
    if (isAverageValues) {
      geoPlot->set_top_right_info(Form("Average values of %d sensor%s", N_VALID_FILES, N_VALID_FILES > 1 ? "s" : ""));
    }
    if (isRatioValues) {
      geoPlot->set_top_right_info("Ratio values");
    }
    if (valueOption != "DEP" && valueOption != "VAL" && !noAxis) {
      geoPlot->set_low_right_info(Form("Values for %s = %.1f %s", selectorLabels.at(1).c_str(),
                                       hexValues->get_selector(), selectorLabels.at(2).c_str()));
    }
    if (additionalLabel != "") {
      geoPlot->set_top_left_info(additionalLabel);
    }
    if (padNumberOption == 2) {
      geoPlot->set_low_left_info("Numbering according to pad type");
    }
    geoPlot->set_output_file(outputFile);
    if (noInfo) {
      geoPlot->set_info(false);
    }
    geoPlot->highlight_pad_option(specialCellOption);
    for (auto &specialCellNumber : specialCellNumbers) {
      for (int j : specialCellNumber) {
        geoPlot->highlight_pad(j);
      }
    }
    set_info_text();
    geoPlot->draw();

    // draw uncertainty hexplot
    bool drawRelativeError = true;
    if (isAverageValues) {
      geoPlot->reset();
      auto *h_errors = dynamic_cast<TH1F *>(h_fill->Clone());
      h_errors->Clone();
      for (int i = 0; i < h_errors->GetNbinsX(); ++i) {
        if (drawRelativeError) {
          h_errors->SetBinContent(i + 1, h_fill->GetBinError(i + 1) / h_fill->GetBinContent(i + 1) * 100);
        } else {
          h_errors->SetBinContent(i + 1, h_fill->GetBinError(i + 1));
        }
      }
      std::string outputFile_error = strip_extension(outputFile) + "_unc." + outputFormat;
      geoPlot->fill(h_errors);
      geoPlot->set_output_file(outputFile_error);
      if (drawRelativeError) {
        geoPlot->set_z_title(Form("Relative spread of %s [%%]", valueLabels.at(1).c_str()));
        if (!zoomUnc.empty()) {
          geoPlot->set_z_range(zoomUncMin, zoomUncMax);
        } else {
          geoPlot->set_z_range(0, 100);
        }
      } else {
        geoPlot->set_z_title(Form("Spread of %s [%s]", valueLabels.at(1).c_str(), valueLabels.at(2).c_str()));
        if (!zoomUnc.empty()) {
          geoPlot->set_z_range(zoomUncMin, zoomUncMax);
        } else if (!zoom.empty()) {
          geoPlot->set_z_range(zoomMin, zoomMax);
        }
      }
      if (isAverageValues) {
        geoPlot->set_top_right_info(
            Form("Uncertainties of average values of %d sensor%s", N_VALID_FILES, N_VALID_FILES > 1 ? "s" : ""));
      } else {
        geoPlot->set_top_left_info(get_file_name_no_ext(inputFile));
      }
      if (valueOption != "DEP" && valueOption != "VAL") {
        geoPlot->set_low_right_info(Form("Values for %s = %.1f %s", selectorLabels.at(1).c_str(),
                                         hexValues->get_selector(), selectorLabels.at(2).c_str()));
      }
      set_info_text();
      geoPlot->draw();
    }
  } else {
    geoPlot->set_no_axis(true);
    geoPlot->set_pad_number_option(padNumberOption);
    if (overwritePadColors != -1) {
      geoPlot->force_pad_colors(overwritePadColors);
    } else {
      geoPlot->force_pad_colors(GEO_ONLY_COLOR);
    }
    geoPlot->set_output_file(outputFile);
    if (noInfo) {
      geoPlot->set_info(false);
    }
    geoPlot->highlight_pad_option(specialCellOption);
    for (auto &specialCellNumber : specialCellNumbers) {
      for (int j : specialCellNumber) {
        geoPlot->highlight_pad(j);
      }
    }
    set_info_text();
    geoPlot->draw();
  }
}
void hex_plotter::plot_flat() {
  TH1F *h_fill = hexValues->load_data_histo_1d();
  if (h_fill == nullptr) {
    ERROR_MSG("value histogram is not filled! Abort!");
    return;
  }
  flatPlot->set_is_average(isAverageValues, inputFiles);
  flatPlot->set_selector(hexValues->get_selector());
  if (additionalLabel != "") {
    flatPlot->set_top_left_info(additionalLabel);
  }

  flatPlot->set_use_logy(useLogVal);
  if (!zoom.empty()) {
    flatPlot->set_use_logy(false);
  }
  flatPlot->set_pad_types(geoPlot->get_pad_type_vec());
  flatPlot->set_pad_type_strings(geoPlot->get_pad_type_string_vec());
  flatPlot->set_special_cell_names(specialCellNames);
  flatPlot->set_special_cell_collection(specialCellCollection);
  flatPlot->set_special_cell_numbers(specialCellNumbers);
  flatPlot->set_is_raw_data(valueOption != "DEP" && valueOption != "VAL");
  flatPlot->fill_data(h_fill);
  flatPlot->draw_flat(outputFile);
  flatPlot->write_average_values(summaryFileName, yesToAll);
  flatPlot->draw_flat_category(strip_extension(outputFile) + "_cat." + outputFormat);
  flatPlot->draw_flat_category_rel(strip_extension(outputFile) + "_cat_rel." + outputFormat,
                                   geoPlot->get_pad_surface_vec());
  flatPlot->draw_flat_category_distr(strip_extension(outputFile) + "_cat_distr." + outputFormat);
  flatPlot->draw_flat_category_distr_spec(strip_extension(outputFile) + "_cat_distr_spec." + outputFormat);
  flatPlot->draw_flat_category_sensors(strip_extension(outputFile) + "_cat_sensors." + outputFormat);
}
void hex_plotter::plot_pad() {
  flatPlot->set_use_logy(useLogVal);
  if (additionalLabel != "") {
    flatPlot->set_top_left_info(additionalLabel);
  }

  if (specialCellNumbers.empty()) {
    DEBUG_MSG("Special cells collection is empty. Using the selector.");

    TH1F *h_fill = hexValues->load_data_histo_1d();
    if (h_fill == nullptr) {
      ERROR_MSG("value histogram is not filled! Abort!");
      return;
    }

    flatPlot->set_is_average(isAverageValues, inputFiles);
    flatPlot->set_selector(hexValues->get_selector());
    flatPlot->fill_data(h_fill);
    flatPlot->draw_pad(!setOutputFile.empty()
                           ? outputFile
                           : strip_extension(outputFile) + "_chan" + int_to_string(int(selector)) + "." + outputFormat,
                       isCVInput);

  } else {
    DEBUG_MSG("Special cells collection is defined.");
    // printf("specialCellNames size: %zu\n", specialCellNames.size());
    // printf("specialCellCollection size: %zu\n", specialCellCollection.size());

    // ERICA: TO FIX: if passing the name, it means that I should plot the average values and not the single pad!
    // if (specialCellNames.size() != specialCellCollection.size()) {
    if (isAverageValues || inputFiles.size() == 1) {
      std::vector<TH1F *> h_fill_cells;
      for (auto i : specialCellNumbers) {
        for (auto ipad : i) {
          DEBUG_MSG("Plotting for #%d specialCell.", ipad);
          hexValues->set_selector(ipad);
          flatPlot->set_is_average(isAverageValues, inputFiles);

          TH1F *h_fill = hexValues->load_data_histo_1d();
          if (h_fill == nullptr) {
            ERROR_MSG("value histogram is not filled! Abort!");
            return;
          }
          h_fill->SetName(Form("Pad #%d", ipad));
          flatPlot->fill_data(h_fill);
          h_fill_cells.push_back(h_fill);
        }
      }

      flatPlot->fill_data(h_fill_cells);
      flatPlot->draw_pads(!setOutputFile.empty() ? outputFile
                                                 : strip_extension(outputFile) + "_selChan." + outputFormat);
      flatPlot->draw_pads_stat(
          !setOutputFile.empty() ? outputFile : strip_extension(outputFile) + "_selChan." + outputFormat, summaryFile,
          yesToAll, summaryFileName);

    } else {
      STANDARD_MSG("The input file name is used to compose the output files name.");
      std::vector<std::string> currentInputFile;
      for (int iFile = 0; iFile < inputFiles.size(); ++iFile) {
        std::vector<TH1F *> h_fill_cells;
        currentInputFile.push_back(inputFiles.at(iFile));
        INFO_MSG("Plot for file: %s", inputFiles.at(iFile).c_str());
        for (auto i : specialCellNumbers) {
          for (auto ipad : i) {
            DEBUG_MSG("Plotting for #%d specialCell.", ipad);
            hexValues->set_selector(ipad);
            flatPlot->set_is_average(isAverageValues, currentInputFile);
            hexValues->set_input_file(inputFiles.at(iFile));
            TH1F *h_fill = hexValues->load_data_histo_1d(iFile);
            if (h_fill == nullptr) {
              ERROR_MSG("value histogram is not filled! Abort!");
              return;
            }
            h_fill->SetName(Form("Pad #%d", ipad));
            flatPlot->fill_data(h_fill);
            h_fill_cells.push_back(h_fill);
          }
        }

        std::string outputFileName =
            strip_extension(outputFile) + "_" + get_file_name_no_ext(inputFiles.at(iFile)) + "_selChan";
        flatPlot->fill_data(h_fill_cells);
        flatPlot->draw_pads(Form("%s.%s", outputFileName.c_str(), outputFormat.c_str()));
        flatPlot->draw_pads_stat(Form("%s.%s", outputFileName.c_str(), outputFormat.c_str()), summaryFile, yesToAll,
                                 Form("%s.%s", outputFileName.c_str(), "txt"), true);
        currentInputFile.clear();
        h_fill_cells.clear();
      }

      flatPlot->draw_pads_stats(inputFiles, outputFile);
    }
  }
}
void hex_plotter::plot_full() {
  TH2F *h_fill = hexValues->load_data_histo_2d();
  if (h_fill == nullptr) {
    ERROR_MSG("value histogram is not filled! Abort!");
    return;
  }
  flatPlot->set_pad_type_strings(geoPlot->get_pad_type_string_vec());
  flatPlot->fill_data(h_fill);
  flatPlot->draw_full_profiles(outputFile);
  flatPlot->draw_full_rel(strip_extension(outputFile) + "_rel." + outputFormat, showMaxValue);
  flatPlot->draw_full_lego(strip_extension(outputFile) + "_lego." + outputFormat);
  flatPlot->write_full(summaryFileName, showMaxValue, yesToAll);
}
void hex_plotter::merge() {
  std::vector<std::string> namesMaster;
  // define histograms to collect info
  TH1F *h_cat[MAX_CELL_TYPE];
  TH1F *h_cat_sensors[MAX_CELL_TYPE];
  auto *c_cat_sensors = new TCanvas("c_cat_sensors", "c_cat_sensors", 800, 500, int(aspectRatioX), int(aspectRatioY));
  auto *c_cat = new TCanvas("c_cat", "c_cat", 800, 500, int(aspectRatioX), int(aspectRatioY));
  for (unsigned int im = 0; im < mergeFiles.size(); ++im) {
    std::string inFileName = mergeFiles.at(im);
    std::string inFileShortName = get_file_name_no_ext(mergeFiles.at(im));
    if (!std::ifstream(inFileName)) {
      WARNING_MSG("Warning: file %s doesn't exist!", inFileName.c_str());
      continue;
    }
    INFO_MSG("Merging file %s", inFileName.c_str());
    auto *tree_merge = new TTree("tree_merge", "tree_merge");

    tree_merge->ReadFile(inFileName.c_str(), "name/C:val/D:err/D", '\t');
    char name[300];
    double val, err;
    tree_merge->SetBranchAddress("name", &name);
    tree_merge->SetBranchAddress("val", &val);
    tree_merge->SetBranchAddress("err", &err);
    std::vector<std::string> names;
    std::vector<double> vals;
    std::vector<double> errs;
    std::vector<int> masterPositions;
    for (unsigned int ientry = 0; ientry < tree_merge->GetEntries(); ++ientry) {
      tree_merge->GetEntry(ientry);
      names.emplace_back(name);
      vals.push_back(val);
      errs.push_back(err);
    }

    // write it out and deal with canvasses
    if (im == 0) {
      // description line 0
      for (unsigned int i = 0; i < names.size(); ++i) {
        namesMaster.push_back(names.at(i));
        masterPositions.push_back(i);
      }
      // divide the canvasses
      int nPads = namesMaster.size();
      c_cat_sensors->Divide(5, int(ceil(1.0 * nPads / 5)));
      c_cat->Divide(5, int(ceil(1.0 * nPads / 5)));
    } else {
      // make sure same order of categories in the file
      for (auto &name : names) {
        int pos = find(namesMaster.begin(), namesMaster.end(), name) - namesMaster.begin();
        DEBUG_MSG("Found %s (%s) at position %d in master!", name.c_str(), namesMaster.at(pos).c_str(), pos);
        if (pos < int(namesMaster.size())) {
          masterPositions.push_back(pos);
        } else {
          masterPositions.push_back(-1);
          WARNING_MSG("entry '%s' in %s is not in master! Skipping this entry!", name.c_str(), inFileName.c_str());
        }
      }
    }
    // fill histograms and write values out
    for (unsigned int i = 0; i < namesMaster.size(); ++i) {
      bool foundIt = false;
      double value(-9999999), error(-9999999);
      for (unsigned int j = 0; j < masterPositions.size(); ++j) {
        if (masterPositions.at(j) == int(i)) {
          foundIt = true;
          value = vals.at(j);
          error = errs.at(j);
          break;
        }
      }
      if (!foundIt) {
        WARNING_MSG("entry '%s' not found in %s! Leaving values empty!", namesMaster.at(i).c_str(), inFileName.c_str());
        continue;
      }
      std::string histname = namesMaster.at(i);
      rtrim(histname);
      std::replace(histname.begin(), histname.end(), ' ', '_');
      std::string axislabel = histname;
      std::replace(axislabel.begin(), axislabel.end(), '_', ' ');
      if (im == 0) {
        h_cat_sensors[i] = new TH1F(Form("h_sensor_%s", histname.c_str()),
                                    Form("h_sensor_%s;;%s", histname.c_str(), axislabel.c_str()), mergeFiles.size(),
                                    0 - 0.5, mergeFiles.size() - 0.5);
      }
      // if (!h_cat_sensors[i]) h_cat_sensors[i]=new
      // TH1F(Form("h_sensor_%s",histname.c_str()),Form("h_sensor_%s",histname.c_str()),mergeFiles.size(),0-0.5,mergeFiles.size()-0.5);
      h_cat_sensors[i]->GetXaxis()->SetBinLabel(im + 1, inFileShortName.c_str());
      if (error >= 0) {
        h_cat_sensors[i]->SetBinContent(im + 1, value);
        h_cat_sensors[i]->SetBinError(im + 1, error);
      } else {
        h_cat_sensors[i]->SetBinContent(im + 1, 0);
        h_cat_sensors[i]->SetBinError(im + 1, 13371337);
      }
    }
  }
  // create and fill summary histograms
  for (unsigned int i = 0; i < namesMaster.size(); ++i) {
    std::string histname = namesMaster.at(i);
    rtrim(histname);
    std::replace(histname.begin(), histname.end(), ' ', '_');
    std::string axislabel = histname;
    std::replace(axislabel.begin(), axislabel.end(), '_', ' ');
    double histMax = h_cat_sensors[i]->GetMaximum();
    double histMin = h_cat_sensors[i]->GetMinimum();
    h_cat[i] = new TH1F(Form("h_%s", histname.c_str()), Form("h_%s;%s;entries", histname.c_str(), axislabel.c_str()),
                        30, histMin - (histMax - histMin) * 0.1, histMax + (histMax - histMin) * 0.1);
    for (int jb = 0; jb < h_cat_sensors[i]->GetNbinsX(); ++jb) {
      double cont = h_cat_sensors[i]->GetBinContent(jb + 1);
      if (cont != 13371337) {
        h_cat[i]->Fill(cont);
      }
    }
    if (testInfo) {
      printf("hex_plotter::merge %s\n", unique_ID(h_cat[i]).c_str());
    }
  }
  // now plot the histograms and write out summary values
  STANDARD_MSG("Write analysis summary to %s", outputFile.c_str());
  fileout.open(outputFile.c_str());
  fileout << "# Merged analysis summary from:" << std::endl;
  for (const auto &mergeFile : mergeFiles) {
    fileout << "#" << mergeFile << std::endl;
    ;
  }
  fileout << std::endl;
  replace_val_two_col_file(outputFile, "#Categories", "Mean\tRMS");
  for (unsigned int i = 0; i < namesMaster.size(); ++i) {
    c_cat_sensors->cd(i + 1);
    // c_cat_sensors->GetPad(i+1)->SetGridx();
    h_cat_sensors[i]->Draw("e");
    c_cat->cd(i + 1);
    h_cat[i]->SetMaximum(h_cat[i]->GetMaximum() * 1.3);
    h_cat[i]->Draw("hist");

    auto *infopave = new TPaveText(0.15, 0.73, 0.88, 0.88, "NDC");
    infopave->SetFillColor(kNone);
    infopave->SetLineColor(kNone);
    infopave->SetTextSize(0.05);
    infopave->SetTextAlign(22);
    infopave->SetTextColor(kBlack);
    infopave->AddText(Form("Mean = %.2f", h_cat[i]->GetMean()));
    infopave->AddText(Form(" RMS = %.2f", h_cat[i]->GetRMS()));
    infopave->Draw("same");

    std::string lineVal = Form("%.2f\t%.2f", h_cat[i]->GetMean(), h_cat[i]->GetRMS());
    replace_val_two_col_file(outputFile, namesMaster.at(i), lineVal);

    h_cat[i]->Draw("same");
    h_cat[i]->Draw("axissame");
  }
  fileout.close();
  std::string cat_sensors_outputFile =
      OUTPUT_DIR + (OUTPUT_DIR.empty() ? "" : "/") + get_file_name_no_ext(outputFile) + "_sensors." + outputFormat;
  c_cat_sensors->Print(cat_sensors_outputFile.c_str());
  std::string cat_outputFile =
      OUTPUT_DIR + (OUTPUT_DIR.empty() ? "" : "/") + get_file_name_no_ext(outputFile) + "_distr." + outputFormat;
  c_cat->Print(cat_outputFile.c_str());
}

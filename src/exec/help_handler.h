/**
    @file help_handler.h
    @brief Handles help printout
    @author Andreas Alexander Maier
*/

#ifndef HELP_HANDLER_H
#define HELP_HANDLER_H

#include "../utils/cpp_utils.h"
#include "globals.h"

/// A class for handling the help printout
class help_handler {
 public:
  /// Print the help
  void print_help();
};

#endif

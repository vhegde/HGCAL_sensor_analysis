#include <utility>

#include <algorithm>
#include <cmath>
#include <numeric>

#include "cpp_utils.h"
#include <boost/regex.hpp>


#ifdef _WIN32
bool WINDOWS_STANDARD_COLOR_FILLED = false;
WORD SAVED_ATTRIBUTES;
#endif
//______________________________________________________________________________
//______________________________________________________________________________
//
// Shell magic
//______________________________________________________________________________
//______________________________________________________________________________
void switch_color(const std::string& color) {
// clang-format off
#ifdef _WIN32
  HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
  if (color != "RESET" && !WINDOWS_STANDARD_COLOR_FILLED) {
    CONSOLE_SCREEN_BUFFER_INFO consoleInfo;
    GetConsoleScreenBufferInfo(hConsole, &consoleInfo);
    SAVED_ATTRIBUTES = consoleInfo.wAttributes;
    WINDOWS_STANDARD_COLOR_FILLED = true;
  }
  if (color == "BLACK") { SetConsoleTextAttribute(hConsole, 80); }
  if (color == "RED") { SetConsoleTextAttribute(hConsole, 92); }
  if (color == "GREEN") { SetConsoleTextAttribute(hConsole, 90); }
  if (color == "YELLOW") { SetConsoleTextAttribute(hConsole, 94); }
  if (color == "BLUE") { SetConsoleTextAttribute(hConsole, 91); }
  if (color == "MAGENTA") { SetConsoleTextAttribute(hConsole, 93); }
  if (color == "CYAN") { SetConsoleTextAttribute(hConsole, 83); }
  if (color == "WHITE") { SetConsoleTextAttribute(hConsole, 86); }
  if (color == "BOLDBLACK") { SetConsoleTextAttribute(hConsole, 6); }
  if (color == "BOLDRED") { SetConsoleTextAttribute(hConsole, 192); }
  if (color == "BOLDGREEN") { SetConsoleTextAttribute(hConsole, 160); }
  if (color == "BOLDYELLOW") { SetConsoleTextAttribute(hConsole, 224); }
  if (color == "BOLDBLUE") { SetConsoleTextAttribute(hConsole, 176); }
  if (color == "BOLDMAGENTA") { SetConsoleTextAttribute(hConsole, 208); }
  if (color == "BOLDCYAN") { SetConsoleTextAttribute(hConsole, 48); }
  if (color == "BOLDWHITE") { SetConsoleTextAttribute(hConsole, 240); }
  if (color == "RESET") {
    SetConsoleTextAttribute(hConsole, SAVED_ATTRIBUTES);
    // SetConsoleTextAttribute(hConsole, 95); //for windows powershell
  }
#else
  if (color == "RESET") { printf(ANSI_COLOR_RESET); }
  if (color == "BLACK") { printf(ANSI_COLOR_BLACK); }
  if (color == "RED") { printf(ANSI_COLOR_RED); }
  if (color == "GREEN") { printf(ANSI_COLOR_GREEN); }
  if (color == "YELLOW") { printf(ANSI_COLOR_YELLOW); }
  if (color == "BLUE") { printf(ANSI_COLOR_BLUE); }
  if (color == "MAGENTA") { printf(ANSI_COLOR_MAGENTA); }
  if (color == "CYAN") { printf(ANSI_COLOR_CYAN); }
  if (color == "WHITE") { printf(ANSI_COLOR_WHITE); }
  if (color == "BOLDBLACK") { printf(ANSI_COLOR_BOLDBLACK); }
  if (color == "BOLDRED") { printf(ANSI_COLOR_BOLDRED); }
  if (color == "BOLDGREEN") { printf(ANSI_COLOR_BOLDGREEN); }
  if (color == "BOLDYELLOW") { printf(ANSI_COLOR_BOLDYELLOW); }
  if (color == "BOLDBLUE") { printf(ANSI_COLOR_BOLDBLUE); }
  if (color == "BOLDMAGENTA") { printf(ANSI_COLOR_BOLDMAGENTA); }
  if (color == "BOLDCYAN") { printf(ANSI_COLOR_BOLDCYAN); }
  if (color == "BOLDWHITE") { printf(ANSI_COLOR_BOLDWHITE); }
#endif
  // clang-format on
}
void print_colors() {
  // clang-format off
  switch_color("BLACK"); printf("This is BLACK in all its beauty\n"); switch_color("RESET");
  switch_color("RED"); printf("This is RED in all its beauty\n"); switch_color("RESET");
  switch_color("GREEN"); printf("This is GREEN in all its beauty\n"); switch_color("RESET");
  switch_color("YELLOW"); printf("This is YELLOW in all its beauty\n"); switch_color("RESET");
  switch_color("BLUE"); printf("This is BLUE in all its beauty\n"); switch_color("RESET");
  switch_color("MAGENTA"); printf("This is MAGENTA in all its beauty\n"); switch_color("RESET");
  switch_color("CYAN"); printf("This is CYAN in all its beauty\n"); switch_color("RESET");
  switch_color("WHITE"); printf("This is WHITE in all its beauty\n"); switch_color("RESET");
  switch_color("BOLDBLACK"); printf("This is BOLDBLACK in all its beauty\n"); switch_color("RESET");
  switch_color("BOLDRED"); printf("This is BOLDRED in all its beauty\n"); switch_color("RESET");
  switch_color("BOLDGREEN"); printf("This is BOLDGREEN in all its beauty\n"); switch_color("RESET");
  switch_color("BOLDYELLOW"); printf("This is BOLDYELLOW in all its beauty\n"); switch_color("RESET");
  switch_color("BOLDBLUE"); printf("This is BOLDBLUE in all its beauty\n"); switch_color("RESET");
  switch_color("BOLDMAGENTA"); printf("This is BOLDMAGENTA in all its beauty\n"); switch_color("RESET");
  switch_color("BOLDCYAN"); printf("This is BOLDCYAN in all its beauty\n"); switch_color("RESET");
  switch_color("BOLDWHITE"); printf("This is BOLDWHITE in all its beauty\n"); switch_color("RESET");
  // clang-format on
}
//______________________________________________________________________________
//______________________________________________________________________________
//
// CPP MAGIC
//______________________________________________________________________________
//______________________________________________________________________________

bool starts_with(const std::string& a, const std::string& b) {
  if (strncmp(a.c_str(), b.c_str(), strlen(b.c_str())) == 0) {
    return true;
  }
  return false;
}
bool ends_with(std::string a, std::string b) {
  if (b.size() > a.size()) {
    return false;
  }
  return std::equal(b.rbegin(), b.rend(), a.rbegin());
}
bool contains(const std::string& a, const std::string& b) { return a.find(b) < a.size(); }
bool match(const std::string& s, const std::string& r) {
  boost::regex e(r);
  return boost::regex_match(s, e);
}
bool file_is_empty(const std::string& fileName) {
  std::ifstream pFile(fileName.c_str(), std::ios_base::binary);
  return pFile.peek() == std::ifstream::traits_type::eof();
}
void print_progress_bar(int now, int max) {
  for (int i = 1; i <= now + 1; i++) {
    fprintf(stdout, "\r[");
    for (int j = 1; j < i; j++) {
      fprintf(stdout, "-");  // ,.-'`'-.,.-'`'-.,.-'`'-
    }
    if (i <= max) {
      fprintf(stdout, ">");
      for (int p = 1; p < max - i + 1; p++) {
        fprintf(stdout, " ");
      }
    }
    fprintf(stdout, "] ");
    fflush(stdout);
  }
}
bool is_file(const std::string& fileName) {
  std::ifstream ifile(fileName);
  return ifile.is_open();
}
bool is_file(char* fileName) { return is_file(std::string(fileName)); }
int get_n_instance(const std::string& a, const std::string& b, bool omitRepetitions) {
  int count = 0;
  size_t nPos = a.find(b, 0);  // fist occurrence
  size_t oldPos = nPos;
  bool isRep = false;
  while (nPos != std::string::npos) {
    if (omitRepetitions && !isRep) {
      count++;
    }
    nPos = a.find(b, nPos + 1);
    isRep = (nPos == oldPos + b.size());
    oldPos = nPos;
  }
  return count;
}
int get_n_columns(const std::string& fileName, const std::string& delim) {
  std::ifstream filein;
  std::string line;
  filein.open(fileName.c_str());
  while (getline(filein, line)) {
    line = trim(line);
    if (line.empty()) {
      continue;
    }
    if (starts_with(line, "#")) {
      continue;
    }
    // printf("This is the first valid line: %s\n", line.c_str() );
    break;
  }
  filein.close();
  return get_n_instance(line, delim, true) + 1;
}
std::string float_to_string(float number, unsigned int nDigitsAfterPoint) {
  std::stringstream stream;
  stream << std::fixed << std::setprecision(nDigitsAfterPoint) << number;
  return stream.str();
}
std::string double_to_string(double number, unsigned int nDigitsAfterPoint) {
  std::stringstream stream;
  stream << std::fixed << std::setprecision(nDigitsAfterPoint) << number;
  return stream.str();
}
std::string print_value(const std::string& valueName, double value, double unc, std::string unitName, int nSigDigits) {
  std::string text;
  double foo = unc / 100000000.;
  int nDigitsAfterPointe = -8;
  while (foo < 1 && nDigitsAfterPointe < 10) {
    nDigitsAfterPointe++;
    foo *= 10;
  }

  if (!unitName.empty()) {
    unitName = " " + unitName;
  }

  if (nDigitsAfterPointe <= 0) {
    text = valueName + " = " + double_to_string(value, 0) + " #pm " + double_to_string(unc, 0) + unitName;
  } else if (nDigitsAfterPointe == 1) {
    text = valueName + " = " + double_to_string(value, 1) + " #pm " + double_to_string(unc, 1) + unitName;
  } else if (nDigitsAfterPointe == 2) {
    text = valueName + " = " + double_to_string(value, 2) + " #pm " + double_to_string(unc, 2) + unitName;
  } else if (nDigitsAfterPointe == 3) {
    text = valueName + " = " + double_to_string(value, 3) + " #pm " + double_to_string(unc, 3) + unitName;
  } else if (nDigitsAfterPointe == 4) {
    text = valueName + " = " + double_to_string(value, 4) + " #pm " + double_to_string(unc, 4) + unitName;
  } else if (nDigitsAfterPointe == 5) {
    text = valueName + " = " + double_to_string(value, 5) + " #pm " + double_to_string(unc, 5) + unitName;
  } else if (nDigitsAfterPointe == 6) {
    text = valueName + " = " + double_to_string(value, 6) + " #pm " + double_to_string(unc, 6) + unitName;
  } else if (nDigitsAfterPointe == 7) {
    text = valueName + " = " + double_to_string(value, 7) + " #pm " + double_to_string(unc, 7) + unitName;
  } else if (nDigitsAfterPointe == 8) {
    text = valueName + " = " + double_to_string(value, 8) + " #pm " + double_to_string(unc, 8) + unitName;
  } else {
    text = valueName + " = " + double_to_string(value, 4) + unitName;
  }
  if (nSigDigits != -1) {
    text = valueName + " = " + double_to_string(value, nSigDigits) + " #pm " + double_to_string(unc, nSigDigits) +
           unitName;
  }
  return text;
}
int get_n_digits_in_string(std::string str) {
  std::string temp;
  int nappearance = 0;
  for (unsigned int i = 0; i < str.size(); i++) {
    if (i > 0 && (isdigit(str[i]) == 0) && (isdigit(str[i - 1]) != 0)) {
      nappearance++;
    }
    if (i == str.size() - 1 && (isdigit(str[i]) != 0)) {
      nappearance++;
    }
  }
  return nappearance;
}
int digits_from_string(std::string str, int nth) {
  std::string temp;
  int number = -1;
  int nappearance = 0;
  for (unsigned int i = 0; i < str.size(); i++) {
    if (isdigit(str[i]) != 0) {
      if (nth == nappearance) {
        for (unsigned int a = i; a < str.size(); a++) {
          temp += str[a];
        }
        break;
      }
    } else {
      if (i > 0 && (isdigit(str[i - 1]) != 0)) {
        nappearance++;
      }
    }
  }
  std::stringstream stream(temp);
  stream >> number;
  return number;
}
bool compare_strings(const std::string& a, const std::string& b) { return (a.compare(b) == -1); }
std::string int_to_string(int number, unsigned int digits) {
  std::stringstream ss;
  ss << number;
  std::string temp = ss.str();
  if (digits == 0u) {
    digits = temp.size();
  }
  while (temp.size() < digits) {
    temp += "0";
    temp += temp;
  }
  return temp;
}
bool is_in_array(int element, const int arr[], int arraySize) {
  for (int i = 0; i < arraySize; ++i) {
    if (element == arr[i]) {
      return true;
    }
  }
  return false;
}
bool is_in_vector(int num, std::vector<int> vec) {
  int pos = find(vec.begin(), vec.end(), num) - vec.begin();
  if (pos >= int(vec.size())) {
    return false;
  }
  return true;
}
bool is_same_elements(const int arr[2], int badOne, int badTwo) {
  std::vector<int> testIndices;
  testIndices.push_back(badOne);
  testIndices.push_back(badTwo);
  std::vector<int> testedIndices;
  for (int i = 0; i < 2; ++i) {
    bool found = false;
    for (int j = 0; j < 2; ++j) {
      if (is_in_vector(j, testedIndices)) {
        continue;
      }
      if (testIndices[i] == arr[j]) {
        found = true;
        testedIndices.push_back(j);
        break;
      }
    }
    if (found) {
      continue;
    }
    return false;
  }
  return true;
}
bool is_in_vector(double num, std::vector<double> vec) {
  int pos = find(vec.begin(), vec.end(), num) - vec.begin();
  if (pos >= int(vec.size())) {
    return false;
  }
  return true;
}
void string_to_array(const std::string& str, int arr[]) {
  for (int i = 0; i < get_n_digits_in_string(str); ++i) {
    int val = digits_from_string(str, i);
    // printf("digit %i: %d\n",i,val );
    arr[i] = val;
  }
}
void string_to_array_double(const std::string& str, double arr[]) {
  for (int i = 0; i < get_n_digits_in_string(str); ++i) {
    double val = digits_from_string(str, i);
    // printf("digit %i: %.2f\n",i,val );
    arr[i] = val;
  }
}
std::string& ltrim(std::string& s) {
  s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
  return s;
}
std::string& rtrim(std::string& s) {
  s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
  return s;
}
std::string& trim(std::string& s) { return ltrim(rtrim(s)); }
std::vector<std::string> split(const std::string& s, char delim) {
  std::stringstream ss(s);
  std::string item;
  std::vector<std::string> tokens;
  while (getline(ss, item, delim)) {
    tokens.push_back(item);
  }
  if (delim == ' ') {
    // concatenate the ones in quotes
    std::vector<std::string> tmpTokens = tokens;
    tokens.clear();
    int idx(0);
    bool inQuotes = false;
    for (const auto& tmpToken : tmpTokens) {
      if (inQuotes) {
        tokens.back() += delim;
        tokens.back() += tmpToken;
      } else {
        tokens.push_back(tmpToken);
      }
      if (starts_with(tmpToken, "\"")) {
        inQuotes = true;
      }
      if (ends_with(tmpToken, "\"")) {
        inQuotes = false;
        if (tokens.back().size() > 1) {
          tokens.back() = tokens.back().substr(1, tokens.back().size() - 2);
        }
      }
    }
  }
  return tokens;
}
void replace_val_two_col_file(const std::string& fileName, const std::string& searchKey, const std::string& newVal) {
  std::ifstream filein;
  std::string line;
  unsigned int curLine = 0;
  filein.open(fileName.c_str());
  bool foundit = false;
  while (getline(filein, line)) {
    curLine++;
    if (line.find(searchKey, 0) != std::string::npos) {
      // cout << "found: " << searchKey << "line: " << curLine <<"! Changing value!" << std::endl;
      foundit = true;
      break;
    }
  }
  filein.close();
  filein.open(fileName.c_str());
  if (foundit) {
    // change the line
    std::ofstream fileout;
    fileout.open((fileName + ".tmp").c_str());
    while (getline(filein, line)) {
      if (line.find(searchKey, 0) == std::string::npos) {
        fileout << line << std::endl;
      } else {
        fileout << std::left << std::setw(50) << searchKey << "\t" << newVal << std::endl;
      }
    }
    fileout.close();
    rename((fileName + ".tmp").c_str(), fileName.c_str());
    std::remove((fileName + ".tmp").c_str());
  } else {
    // cout << "Failed to find " << searchKey << " in "<< fileName<<"! Adding info! " << std::endl;
    std::ofstream fileout;
    fileout.open(fileName.c_str(), std::ofstream::app);
    fileout << std::left << std::setw(50) << searchKey << "\t" << newVal << std::endl;
    fileout.close();
  }
  filein.close();
}
std::string get_file_name(const std::string& s, const int nLayers) {
  std::string dirDelim = contains(s, "/") ? "/" : "\\";
  return get_extension(s, dirDelim, nLayers);
}
std::string get_file_name_no_ext(const std::string& s, const std::string& delim, const int nLayers) {
  std::string dirDelim = contains(s, "/") ? "/" : "\\";
  std::string fullname = get_extension(s, dirDelim, nLayers);
  return strip_extension(fullname, delim);
}
std::string get_file_dir(const std::string& s) {
  std::string dirDelim = contains(s, "/") ? "/" : "\\";
  bool isSameDir = !contains(s, dirDelim);
  if (isSameDir) {
    return "";
  }
  return strip_extension(s, dirDelim);
}
std::string get_extension(const std::string& s, const std::string& delim, const int nLayers) {
  const std::string& fullname = s;
  size_t lastindex = fullname.find_last_of(delim);
  for (size_t i = 0; i < size_t(nLayers); i++) {
    lastindex = fullname.find_last_of(delim, lastindex - 1);
  }
  std::string rawname = fullname.substr(lastindex + 1, s.length());
  return (rawname);
}
std::string strip_extension(const std::string& s, const std::string& delim) {
  const std::string& fullname = s;
  size_t lastindex = fullname.find_last_of(delim);
  std::string rawname = fullname.substr(0, lastindex);
  return (rawname);
}
std::string remove_string(const std::string& s, const std::string& removeString) {
  size_t i = s.find(removeString);
  std::string es = s;
  if (i != std::string::npos) {
    es.erase(i, removeString.length());
  }
  return es;
}
std::string remove_string_all(const std::string& s, const std::string& removeChar) {
  std::string es = s;
  while (true) {
    std::string oldString = es;
    std::string newString = remove_string(es, removeChar);
    if (oldString == newString) {
      break;
    }
    es = newString;
  }
  return es;
}
std::string replace_string(const std::string& s, const std::string& from, const std::string& to) {
  std::string es = s;
  size_t start_pos = es.find(from);
  if (start_pos == std::string::npos) {
    return es;
  }
  es.replace(start_pos, from.length(), to);
  return es;
}
std::string replace_string_all(const std::string& s, const std::string& from, const std::string& to) {
  std::string es = s;
  size_t start_pos = 0;
  while ((start_pos = es.find(from, start_pos)) != std::string::npos) {
    es.replace(start_pos, from.length(), to);
    start_pos += to.length();  // In case 'to' contains 'from', like replacing 'x' with 'yx'
  }
  return es;
}
bool yes_no_request(const std::string& prompt) {
  std::cout << prompt << " (Y/[n])  ";
  std::string answer;
  std::string input;
  std::getline(std::cin, input);
  if (!input.empty()) {
    std::istringstream stream(input);
    stream >> answer;
  }
  return (answer == "Y");
}
double get_mean(const std::vector<double>& v) {
  double mean = 0.0;
  mean = std::accumulate(v.begin(), v.end(), 0.0) / v.size();
  return mean;
}
double get_mean_err(const std::vector<double>& v) {
  double mean_err = 0.0;
  double sq_sum = std::inner_product(v.begin(), v.end(), v.begin(), 0.0);
  mean_err = std::sqrt(sq_sum / v.size() - get_mean(v) * get_mean(v));
  return mean_err;
}
double get_median(std::vector<double>& v) {
  double median = 0.0;
  int size = v.size();
  std::sort(v.begin(), v.end());
  if (size % 2 == 0) {
    median = (v[size / 2 - 1] + v[size / 2]) / 2;
  } else {
    median = v[size / 2];
  }
  return median;
}
double get_median_err(std::vector<double>& v) {
  double median_err = 0.0;
  median_err = 1.2533 * get_mean_err(v);
  return median_err;
}
std::string hours_minutes(double decimalHours) {
  std::string hours = int_to_string(int(floor(decimalHours)));
  std::string minutes = int_to_string(int((decimalHours - floor(decimalHours)) * 60));
  if (minutes.length() == 1) {
    minutes = "0" + minutes;
  }
  std::string hoursminutes = hours + ":" + minutes;
  // cout<<decimalHours<<" leads to "<<hoursminutes<<std::endl;
  return hoursminutes;
}
std::string enum_suffix(int times) {
  if (int(std::fabs(float(times))) % 10 == 1) {
    return "st";
  }
  if (int(std::fabs(float(times))) % 10 == 2) {
    return "nd";
  }
  if (int(std::fabs(float(times))) % 10 == 3) {
    return "rd";
  }
  return "th";
}
int positive_modulo(int i, int n) { return (i % n + n) % n; }
#ifndef HELP_SIGNFUNCTIONS_C
#define HELP_SIGNFUNCTIONS_C
int sign(double v) { return v > 0 ? 1 : (v < 0 ? -1 : 0); }
double positive(double v) { return sign(v) > 0 ? v : 0; }
#endif
void printf_file(const std::string& text, std::ofstream& fileout) {
  printf("%s", text.c_str());
  fileout << text;
}
std::string get_current_dir() {
#ifdef _WIN32
  const unsigned long maxDir = 260;
  char currentDir[maxDir];
  GetCurrentDirectoryA(maxDir, currentDir);
  return std::string(currentDir);
#else
  char cwd[1024];
  if (getcwd(cwd, sizeof(cwd)) != nullptr) {
    return std::string(cwd);
  }
  perror("getcwd() error");

  return "";
#endif
}
double double_from_string(const std::string& s) {
  double d;
  sscanf(s.c_str(), "%*[^0-9]%lf", &d);
  std::string firstDigit = double_to_string(d, 0);
  std::vector<std::string> parts = split(s, *firstDigit.c_str());
  if (ends_with(parts.at(0), "-")) {
    d *= -1;
  }
  return d;
}
int get_argc(const std::string& cmd) {
  int argc = get_n_instance(cmd, " ", true) + 1;
  return argc;
}
std::vector<std::string> get_argv(const std::string& cmd) {
  std::vector<std::string> argv = split(cmd, ' ');
  return argv;
}

uint64_t big_mod(uint64_t a, uint64_t b, uint64_t c) {
  // https://stackoverflow.com/questions/20971888/modular-multiplication-of-large-numbers-in-c
  if (a == 0 || b == 0) {
    return 0;
  }
  if (a == 1) {
    return b;
  }
  if (b == 1) {
    return a;
  }
  // Returns: (a * b/2) mod c
  uint64_t a2 = big_mod(a, b / 2, c);
  // Even factor
  if ((b & 1) == 0) {
    // [((a * b/2) mod c) + ((a * b/2) mod c)] mod c
    return (a2 + a2) % c;
  }
  // Odd exponent
  // [(a mod c) + ((a * b/2) mod c) + ((a * b/2) mod c)] mod c
  return ((a % c) + (a2 + a2)) % c;
}
uint64_t modulo_hash(const uint64_t i) {
  if (i == -1) {
    printf("Warning: 64-bit integer overflow. Hash faulty!\n");
    return -1;
  }
  return big_mod(i, HASH_PRIME, MAX_HASH_SIZE);
}
uint64_t modulo_merge_hash(uint64_t i, uint64_t j) {
  uint64_t combination = (modulo_hash(i) + modulo_hash(j)) % MAX_HASH_SIZE;
  return combination;
}
uint64_t modulo_hash_double(double d) {
  // lift it up in case very small
  int expCount(0);
  while (d < 1) {
    d *= 10;
    ++expCount;
  }
  // take the first few values after the digit as well
  int digitsAfterPoint = 5;
  auto finalInt = uint64_t(d * pow(10, digitsAfterPoint));
  // avoid hash invariance to scales of 10
  finalInt += expCount;
  return modulo_hash(finalInt);
}

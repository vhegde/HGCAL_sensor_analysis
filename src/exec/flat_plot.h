/**
    @file flat_plot.h
    @brief Handles plotting of flat plots
    @author Andreas Alexander Maier
*/

#ifndef FLAT_PLOT_H
#define FLAT_PLOT_H

#include <string>
#include <utility>

#include "TCanvas.h"
#include "TGaxis.h"
#include "TGraphErrors.h"
#include "TH1F.h"
#include "TLegend.h"
#include "TPad.h"

#include "../utils/cpp_utils.h"
#include "../utils/root_utils.h"
#include "globals.h"

/// A class for plotting different kinds of 1D plots
class flat_plot {
 public:
  void fill_data(TH1F *h);
  void fill_data(std::vector<TH1F *> &h_vec);
  void fill_data(TH2F *h);

  /// Produces the value vs pad number plot
  void draw_flat(const std::string &outputFile);
  /// Writes the average values of each pad category to file
  void write_average_values(const std::string &outputFile, bool yesToAll = false);
  /// Produces the distributions of values within each category
  void draw_flat_category_distr(const std::string &outputFile);
  /// Produces the distributions of values for the pads marked as special
  void draw_flat_category_distr_spec(const std::string &outputFile);
  /// Produces the average value vs category plot
  void draw_flat_category(const std::string &outputFile);
  /// Produces the average value vs category plot, scaled with pad surface
  void draw_flat_category_rel(const std::string &outputFile, std::vector<double> padSurfaceVec);
  /// Produces the average value per category vs sensor name plot
  void draw_flat_category_sensors(const std::string &outputFile);
  /// Produces the value vs selector plot, e.g. single pad IV or CV curves
  void draw_pad(const std::string &outputFile, bool isCVInput = false);
  /// Produces the value vs selector plot for different special cells
  void draw_pads(const std::string &outputFile);
  /// Produces the mean and the median values vs selector for different special cells
  void draw_pads_stat(const std::string &outputFile, bool summary = false, bool yesToAll = false,
                      const std::string &summaryFile = "summary.txt", bool saving = false);
  /// Produces the mean and the median values vs selector for different special cells
  void draw_pads_stats(const std::vector<std::string> &inputFileList, const std::string &outputFile);
  /// Writes out some info on full sensor
  void write_full(const std::string &outputFile, bool showMaxValue = true, bool yesToAll = false);
  /// Produces a 2D plot of pad number vs voltages vs value fraction of respective pad maximum
  void draw_full_rel(const std::string &outputFile, bool showMaxValue = true);
  ///  Produces a 2D plot of pad number vs voltages vs value in lego plot option
  void draw_full_lego(const std::string &outputFile);
  /// Produces pad number vs value overlayed for all voltages
  void draw_full_profiles(const std::string &outputFile);

  void set_aspect_ratio(double x, double y) {
    aspectRatioX = int(x);
    aspectRatioY = int(y);
  };
  void set_is_average(bool p, std::vector<std::string> v) {
    isAverageValues = p;
    inputFiles = std::move(v);
  };
  void set_is_ratio(bool p, std::vector<std::string> v) {
    isRatioValues = p;
    inputFiles = std::move(v);
  };
  /// Set value is derived, e.g. a depletion voltage
  void set_is_raw_data(bool p) { isRawData = p; };
  void set_pad_type_strings(std::vector<std::string> v) { padTypeStrings = std::move(v); };
  void set_pad_types(std::vector<int> v) { padTypes = std::move(v); };
  void set_range(double min = 0, double max = 0) {
    zoomMin = min;
    zoomMax = max;
  };
  void set_selector(double p) { selector = p; };
  void set_selector_labels(std::vector<std::string> v) { selectorLabels = std::move(v); };
  void set_special_cell_collection(std::vector<std::string> v) { specialCellCollection = std::move(v); };
  void set_special_cell_names(std::vector<std::string> v) { specialCellNames = std::move(v); };
  void set_special_cell_numbers(std::vector<std::vector<int>> v) { specialCellNumbers = std::move(v); };
  /// Print out hashed info on result
  void set_test_info(bool p) { testInfo = p; };
  void set_use_logy(bool p = true) { useLogVal = p; };
  void set_only_special(bool p = true) { onlySpecial = p; };
  void set_value_labels(std::vector<std::string> v) { valueLabels = std::move(v); };
  void set_verbose(int p) { verbose = p; };
  /// Set info text appearing in the top right corner of the plot
  void set_top_left_info(std::string s) { topLeftLabel = std::move(s); };

  flat_plot();

 private:
  const bool SINGLE_CELLS_FOR_AVERAGE = true;

  bool initializedFlat;
  bool isAverageValues;
  bool isRatioValues;
  bool isRawData;
  bool onlySpecial;
  bool SuccessfullyLoaded;
  bool testInfo;
  bool useLogVal;
  double selector;
  double totalMax;
  double totalMin;
  double zoomMax;
  double zoomMin;
  int aspectRatioX;
  int aspectRatioY;
  int numberNAN;
  int numberOF;
  int standardpos;
  int verbose;
  std::ofstream fileout;
  std::vector<int> invlegendorder;
  std::vector<int> legendorder;
  std::vector<int> padTypes;
  std::vector<int> special_types;
  std::vector<int> types;
  std::vector<std::string> inputFiles;
  std::vector<std::string> padTypeStrings;
  std::vector<std::string> selectorLabels;
  std::vector<std::string> specialCellCollection;
  std::vector<std::string> specialCellNames;
  std::vector<std::string> valueLabels;
  std::vector<std::vector<int>> cellWithType;
  std::vector<std::vector<int>> specialCellNumbers;
  TGraphErrors *gr_categories_sensors[MAX_CELL_TYPE];
  TH1F *h_cat_sensors[MAX_CELL_TYPE][MAX_SENSOR_NUMBER];
  TH1F *h_categories[MAX_CELL_TYPE + 1];
  TH1F *h_categories_meanError[MAX_CELL_TYPE + 1];
  TH1F *h_fill;
  std::vector<TH1F *> h_fill_vec;
  std::vector<TGraphErrors *> gr_means;
  std::vector<TGraphErrors *> gr_medians;
  TH1F *h_markers[MAX_CELL_TYPE];
  TH1F *h_markers_proj[MAX_CELL_TYPE];
  TH1F *h_markers_proj_zoom[MAX_CELL_TYPE];
  TH2F *h_fill_2D;
  TLegend *full_leg;
  TLegend *leg;
  TLegend *special_leg;
  TPaveText *padVoltageInfo;
  std::string topLeftLabel;

  bool has_negative_values();
  void create_summary_file(const std::string &outputFile, bool yesToAll = false);
  void draw_warnings();
  void init_flat();
  std::map<double, std::vector<double>> profile_pads();
  TGraphErrors *profile_mean(const std::map<double, std::vector<double>> map_profile,
                             std::map<float, float> bin_widths);
  TGraphErrors *profile_median(std::map<double, std::vector<double>> map_profile, std::map<float, float> bin_widths);
};

#endif
